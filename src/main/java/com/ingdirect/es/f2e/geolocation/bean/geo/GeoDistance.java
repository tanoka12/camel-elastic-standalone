package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;

/**
 * Created by EH80OB on 25/04/2017.
 */
public class GeoDistance implements Serializable, Routing {
	
	private final String id;
	
	private final GeoPoint location;
	
	private final String otherFields;
	
	public GeoDistance(String id, GeoPoint location, String otherFields) {
		this.id = id;
		this.location = location;
		this.otherFields = otherFields;
	}

    public String getId() {
        return id;
    }

    public String getOtherFields() {
        return otherFields;
    }

    @Override
	public GeoPoint getLocation() {
		return location;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GeoDistance that = (GeoDistance) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (location != null ? !location.equals(that.location) : that.location != null) return false;
        return otherFields != null ? otherFields.equals(that.otherFields) : that.otherFields == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (location != null ? location.hashCode() : 0);
        result = 31 * result + (otherFields != null ? otherFields.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GeoDistance{");
        sb.append("id='").append(id).append('\'');
        sb.append(", location=").append(location);
        sb.append(", otherFields='").append(otherFields).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
