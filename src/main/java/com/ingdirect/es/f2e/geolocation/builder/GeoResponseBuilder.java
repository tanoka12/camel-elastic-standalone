package com.ingdirect.es.f2e.geolocation.builder;

import java.util.Map;

import com.ingdirect.es.f2e.geolocation.bean.geo.GeoResponse;

public class GeoResponseBuilder {
	
	private final String lon;
	
	private final String lat;
	
	private final String number;
	
	private final String viaName;
	
	private final String viaType;
	
	private final String town;
	
	private final String province;
	
	private final String postalCode;
	
	private final String state;
	
	private String source;
	
	public GeoResponseBuilder(Map<String, Object> resultMap) {
		
		this.number = (String) resultMap.get( "number" );
		this.viaName = (String) resultMap.get( "via_name" );
		this.viaType = (String) resultMap.get( "via_type" );
		this.town = (String) resultMap.get( "town" );
		this.province = (String) resultMap.get( "province" );
		this.postalCode = (String) resultMap.get( "postalCode" );
		this.state = (String) resultMap.get( "state" );
		
		@SuppressWarnings("unchecked")
		Map<String, Object> location = (Map<String, Object>) resultMap.get( "location" );
		
		this.lat = (String) location.get( "lat" );
		this.lon = (String) location.get( "lon" );
		
	}
	
	public GeoResponseBuilder source(String source) {
		
		this.source = source;
		return this;
		
	}
	
	public GeoResponse build() {
		GeoResponse geoResponse = new GeoResponse( lon, lat, number, viaName, viaType, town, province, postalCode,
				state );
		geoResponse.setSource( source );
		return geoResponse;
	}
	
}
