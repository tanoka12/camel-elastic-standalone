package com.ingdirect.es.f2e.geolocation.bean.geo;

public class ErrorESResponse {
	
	private final String id;
	
	private final String detail;
	
	public ErrorESResponse(String id, String detail) {
		
		this.id = id;
		this.detail = detail;
	}
	
	public String getId() {
		return id;
	}
	
	public String getDetail() {
		return detail;
	}
	
	@Override
	public String toString() {
		return "ErrorESResponse [id=" + id + ", detail=" + detail + "]\n";
	}
	
}
