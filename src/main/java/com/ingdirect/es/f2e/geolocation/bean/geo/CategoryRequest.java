package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by EH80OB on 17/04/2017.
 */
public class CategoryRequest implements Serializable {

    private final List<String> categories;

    public CategoryRequest() {
        this.categories = new ArrayList<>();
    }

    public CategoryRequest(String categories) {
        this.categories = new ArrayList<>();
        Stream.of( categories.split( ";", -1 ) ).forEach(category -> this.categories.add(category));
    }

    public void addCategory(String newCategory){
        categories.add(newCategory);
    }

    public List<String> getCategories() {
        return categories;
    }
}
