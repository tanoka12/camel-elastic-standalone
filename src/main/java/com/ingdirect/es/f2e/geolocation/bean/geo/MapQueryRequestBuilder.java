package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by EH80OB on 22/08/2017.
 */
public class MapQueryRequestBuilder {

    private String buffer;
    private String profile;
    private int poiLimit;
    private GeoPoint location;
    private List<String> categories;

    public MapQueryRequestBuilder(GeoPoint location) {
        this.location = location;
    }

    public MapQueryRequestBuilder withBuffer(String buffer){
        this.buffer = buffer;
        return this;
    }

    public MapQueryRequestBuilder withProfile(String profile){
        this.profile = profile;
        return this;
    }

    public MapQueryRequestBuilder withLimit(int poiLimit){
        this.poiLimit = poiLimit;
        return this;
    }

    public MapQueryRequestBuilder withCategories(List<String> categories){
        this.categories = categories;
        return this;
    }

    public MapQueryRequest build() {
        final MapQueryRequest mapQueryRequest = new MapQueryRequest(buffer, profile, poiLimit, "", createCategoriesInfo(categories), 1, 1);
        mapQueryRequest.setLocation(location);
        return mapQueryRequest;
    }

    private List<CategoryInfo> createCategoriesInfo(List<String> categories) {

        return categories.stream().map(category -> {
            CategoryInfo cat = new CategoryInfo(category, "", "", 1, 1);
            return cat;
        }).collect(Collectors.toList());
    }
}
