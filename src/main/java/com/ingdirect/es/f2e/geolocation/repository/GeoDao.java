package com.ingdirect.es.f2e.geolocation.repository;

import java.util.List;

import com.ingdirect.es.f2e.geolocation.bean.geo.GeoPoint;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoRequest;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoResponse;
import com.ingdirect.es.f2e.geolocation.bean.geo.QueryParam;

public interface GeoDao extends ElasticSearchDao {

    List<GeoResponse> getLocations(GeoRequest location, String index, String mapping, QueryParam queryParam) throws Exception;

    List<GeoResponse> getLocationsBool(GeoRequest location, String index, String mapping, boolean exactQuery) throws Exception;

    GeoPoint findLocationById(String index, String mapping, String id) throws Exception;
}
