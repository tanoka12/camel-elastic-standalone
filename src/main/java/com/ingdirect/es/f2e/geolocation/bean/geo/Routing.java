package com.ingdirect.es.f2e.geolocation.bean.geo;

/**
 * Created by EH80OB on 25/04/2017.
 */
public interface Routing {

    GeoPoint getLocation();
}
