package com.ingdirect.es.f2e.geolocation.client;

public interface GeoElasticSearchClient {

    String getRecentIndexByPrefix(String preffixIndex) throws Exception;

    boolean createIndex(String index) throws Exception;

    boolean createIndex(String index, String indexSettings) throws Exception;

    boolean existsIndex(String index) throws Exception;

    boolean refreshIndex(String index)throws Exception;

    boolean deleteIndex(String index) throws Exception;
}
