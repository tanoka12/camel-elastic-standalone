package com.ingdirect.es.f2e.geolocation.bean.geo.exception;

@SuppressWarnings("serial")
public class GeoProcessException extends Exception {
	
	private final Long errorCode;
	private final String msg;
	
	public GeoProcessException(String msg, Long code) {
		super( msg );
		this.errorCode = code;
		this.msg = msg;
		
	}
	
	public Long getErrorCode() {
		return errorCode;
	}
	
	public String getMsg() {
		return msg;
	}
}
