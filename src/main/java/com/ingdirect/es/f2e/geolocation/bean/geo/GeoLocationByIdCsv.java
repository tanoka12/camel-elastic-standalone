package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GeoLocationByIdCsv {
	
	private static final String LONGITUD_HEADER_VALUE = "LONGITUD";
	
	private static final String LATITUD_HEADER_VALUE = "LATITUD";
	
	private final String id;
	
	private final List<String> dynamicColumn;
	
	private final boolean header;
	
	private String longitude;
	
	private String latitude;
	
	public GeoLocationByIdCsv(String id, boolean header) {
		super();
		this.id = id;
		this.header = header;
		this.dynamicColumn = new ArrayList<String>();
		
		if (header) {
			this.longitude = LONGITUD_HEADER_VALUE;
			this.latitude = LATITUD_HEADER_VALUE;
		}
	}
	
	public String getId() {
		return id;
	}
	
	public List<String> getDynamicColumn() {
		return dynamicColumn;
	}
	
	public void addDynamicColumn(String column) {
		dynamicColumn.add( column );
	}
	
	public void addDynamicColumns(List<String> columns) {
		dynamicColumn.addAll( columns );
		
	}
	
	public boolean isHeader() {
		return header;
	}
	
	@Override
	public String toString() {
		return "GeoLocationByIdCsv [id=" + id + ", dynamicColumn=" + dynamicColumn + ", header=" + header + "]";
	}
	
	public String toFileRegister() {
		
		List<String> listValues = new ArrayList<String>();
		
		listValues.add( id );
		listValues.add( longitude );
		listValues.add( latitude );
		listValues.addAll( dynamicColumn );
		
		String register = listValues.stream().map( item -> {
			return ( item == null ? "" : item );
		} ).collect( Collectors.joining( "\t" ) );
		
		return register;
		
	}
	
	public String getLongitude() {
		return longitude;
	}
	
	public String getLatitude() {
		return latitude;
	}
	
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
}
