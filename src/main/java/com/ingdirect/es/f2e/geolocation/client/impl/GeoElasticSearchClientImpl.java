package com.ingdirect.es.f2e.geolocation.client.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingdirect.es.f2e.geolocation.client.GeoElasticSearchClient;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.ResponseException;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.Strings;

import java.io.IOException;
import java.util.*;
import java.util.stream.StreamSupport;

public class GeoElasticSearchClientImpl extends RestHighLevelClient  implements GeoElasticSearchClient {
    public static final int HTTP_OK = 200;

    public static final String PUT_METHOD = "PUT";
    public static final String GET_METHOD = "GET";
    public static final String DELETE_METHOD = "DELETE";
    public static final String POST_METHOD = "POST";
    public static final String REFRESH = "/_refresh";
    public static final String SETTINGS = "/_settings";

    protected RestClient lowLevelClient;

    private final ObjectMapper mapper = new ObjectMapper();

    public GeoElasticSearchClientImpl(RestClient restClient) {
        super(restClient);
        this.lowLevelClient = restClient;
    }

    public RestClient getLowLevelClient() {
        return lowLevelClient;
    }

    @Override
    public String getRecentIndexByPrefix(String preffixIndex) throws Exception {

        final Response response = getLowLevelClient().performRequest(GET_METHOD, "/" + preffixIndex + SETTINGS,
                new HashMap<>());

        final String recentIndexByCreationTime = getRecentIndexByCreationTime(response);

        return recentIndexByCreationTime;
    }

    @Override
    public boolean createIndex(String index) throws Exception {

        Response response = getLowLevelClient().performRequest(PUT_METHOD, "/" + index, Collections.emptyMap());

        return getStatusCode(response) == HTTP_OK;
    }

    @Override
    public boolean createIndex(String index, String indexSettings) throws Exception {

        StringEntity entity = null;
        if (!Strings.isNullOrEmpty(indexSettings)) {
            entity = new StringEntity(indexSettings, ContentType.APPLICATION_JSON);
        }

        Response response = getLowLevelClient().performRequest(PUT_METHOD, "/" + index, Collections.emptyMap(), entity);

        return getStatusCode(response) == HTTP_OK;
    }

    @Override
    public boolean existsIndex(String index) throws Exception{
        try {
            Response response = getLowLevelClient().performRequest(GET_METHOD, "/" + index);
            return getStatusCode(response) == HTTP_OK;
        } catch (ResponseException e) {
            return false;
        }

    }

    @Override
    public boolean refreshIndex(String index)throws Exception{

        Response response = getLowLevelClient().performRequest(POST_METHOD, "/" + index + REFRESH);
        return getStatusCode(response) == HTTP_OK;
    }

    private int getStatusCode(Response response){
        return response.getStatusLine().getStatusCode();
    }

    @Override
    public boolean deleteIndex(String index) throws Exception{
        Response response = getLowLevelClient().performRequest(DELETE_METHOD, "/" + index);
        return getStatusCode(response) == HTTP_OK;
    }


    private String getRecentIndexByCreationTime(Response response) throws IOException {

        JsonNode root = mapper.readTree(EntityUtils.toString(response.getEntity()));

        final Iterator<JsonNode> iteratorRoot = root.iterator();

        final Optional<JsonNode> max = StreamSupport
                .stream(((Iterable<JsonNode>) () -> iteratorRoot).spliterator(), false)
                .max(Comparator.comparing(
                        jsonNode -> jsonNode.path("settings").path("index").path("creation_date").asLong()));

        if (max.isPresent()) {
            return max.get().path("settings").path("index").path("provided_name").asText();

        } else {
            return "";
        }
    }
}
