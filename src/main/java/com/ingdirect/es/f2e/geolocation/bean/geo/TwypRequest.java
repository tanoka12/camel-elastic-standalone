package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("serial")
public class TwypRequest implements Serializable, ESRequest {
	
	private final String id;
	
	private final String postalCode;
	
	private final GeoPoint location;
	
	private final String town;
	
	private final String province;
	
	private final String viaName;
	
	private final String state;
	
	private final String merchantName;
	
	public TwypRequest(String id, String postalCode, GeoPoint location, String town, String province, String viaName,
			String state, String merchantName) {
		super();
		this.id = id;
		this.postalCode = postalCode;
		this.location = location;
		this.town = town;
		this.province = province;
		this.viaName = viaName;
		this.state = state;
		this.merchantName = merchantName;
	}
	
	@JsonProperty("via_name")
	public String getViaName() {
		return viaName;
	}
	
	public GeoPoint getLocation() {
		return location;
	}
	
	public String getTown() {
		return town;
	}
	
	public String getProvince() {
		return province;
	}
	
	public String getPostalCode() {
		return postalCode;
	}
	
	public String getState() {
		return state;
	}
	
	@JsonProperty("subcategory")
	public String getMerchantName() {
		return merchantName;
	}
	
	@JsonIgnore
	public String getId() {
		return id;
	}
	
	@Override
	public String getIndexId() {
		return getId();
	}
	
}
