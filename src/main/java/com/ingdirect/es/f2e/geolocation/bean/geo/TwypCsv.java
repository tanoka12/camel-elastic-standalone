package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.commons.lang3.StringUtils;

@CsvRecord(separator = "\t")
public class TwypCsv {
	
	@DataField(pos = 1, required = true)
	private String id;
	
	@DataField(pos = 2, required = false)
	private String viaName;
	
	@DataField(pos = 3, required = true)
	private String postalCode;
	
	@DataField(pos = 4, required = false)
	private String town;
	
	@DataField(pos = 5, required = false)
	private String merchantName;
	
	@DataField(pos = 6, required = true)
	private String latitude;
	
	@DataField(pos = 7, required = true)
	private String longitude;
	
	@Override
	public String toString() {
		return "TwypCsv [id=" + id + ", viaName=" + viaName + ", postalCode=" + postalCode + ", town=" + town
				+ ", merchantName=" + merchantName + ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getViaName() {
		return viaName;
	}
	
	public void setViaName(String viaName) {
		this.viaName = viaName;
	}
	
	public String getPostalCode() {
		return postalCode;
	}
	
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	public String getTown() {
		return town;
	}
	
	public void setTown(String town) {
		this.town = town;
	}
	
	public String getMerchantName() {
		return merchantName;
	}
	
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	
	public String getLatitude() {
		return latitude;
	}
	
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
	public String getLongitude() {
		return longitude;
	}
	
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public boolean isValid() {
		return !( StringUtils.isEmpty( id )
				|| StringUtils.isEmpty( postalCode )
				|| StringUtils.isEmpty( viaName )
				|| StringUtils.isEmpty( latitude )
				|| StringUtils.isEmpty( longitude ) );
	}
	
	public String toFileRegister() {
		
		List<String> listValues = new ArrayList<String>();
		
		listValues.add( id );
		listValues.add( viaName );
		listValues.add( postalCode );
		listValues.add( town );
		listValues.add( merchantName );
		listValues.add( latitude );
		listValues.add( longitude );
		
		String register = listValues.stream().map( item -> {
			return ( item == null ? "" : item );
		} ).collect( Collectors.joining( "\t" ) );
		
		return register + "\n";
		
	}
	
}
