package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;

/**
 * Created by EH80OB on 16/05/2017.
 */
public class QueryParam implements Serializable{

    private final boolean exactQuery;

    private final boolean matchQuery;


    public QueryParam(boolean exactQuery, boolean matchQuery) {
        this.exactQuery = exactQuery;
        this.matchQuery = matchQuery;
    }

    public boolean isExactQuery() {
        return exactQuery;
    }

    public boolean isMatchQuery() {
        return matchQuery;
    }
}
