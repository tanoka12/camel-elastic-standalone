package com.ingdirect.es.f2e.geolocation.bean.geo;

public class NumberFieldsPerCustomer {
	
	private Integer numberFieldsPerCustomer = 13;
	
	public Integer getNumberFieldsPerCustomer() {
		return numberFieldsPerCustomer;
	}
	
	public void setNumberFieldsPerCustomer(Integer numberFieldsPerCustomer) {
		this.numberFieldsPerCustomer = numberFieldsPerCustomer;
	}
	
	@Override
	public String toString() {
		return "NumberFieldsPerCustomer [numberFieldsPerCustomer=" + numberFieldsPerCustomer + "]";
	}
	
}
