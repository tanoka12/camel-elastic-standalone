package com.ingdirect.es.f2e.geolocation.repository;

import com.ingdirect.es.f2e.geolocation.bean.geo.ESRequest;
import com.ingdirect.es.f2e.geolocation.bean.geo.ErrorESResponse;
import org.elasticsearch.action.get.GetResponse;

import java.util.List;

public interface ElasticSearchDao {

    boolean createIndex(String index) throws Exception;

    boolean existIndex(String index) throws Exception;

    boolean createIndex(String index, String indexJsonSource) throws Exception;

    String getRecentIndexByPrefix(String preffixIndex) throws Exception;

    boolean deleteIndex(String index) throws Exception;

    void refreshIndex(String index) throws Exception;

    List<ErrorESResponse> insertLocations(String index, String mapping, List<? extends ESRequest> locations) throws Exception;

    GetResponse findObjectById(String index, String mapping, String id) throws Exception;

    <T> List<T> getAllDocuments(String index, Class<T> clazz) throws Exception;
}
