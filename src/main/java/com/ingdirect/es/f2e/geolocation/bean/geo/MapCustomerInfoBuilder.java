package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.util.List;

public class MapCustomerInfoBuilder {

    private String id;
    private String otherFields;
    private boolean header;

    public MapCustomerInfoBuilder() {
    }

    public MapCustomerInfoBuilder(List<String> fields) {

        this.header = isHeader(fields);

        if (fields.size() == 1) {
            this.id = fields.get(0);
            this.otherFields = "";
        } else {
            this.id = fields.get(0);
            this.otherFields = fields.get(1);
        }
    }

    private boolean isHeader(List<String> listFields) {
        return "CUSTOMER_ID".equals(listFields.get(0));
    }

    public MapCustomerInfoBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public MapCustomerInfoBuilder withOtherFields(String otherFields) {
        this.otherFields = otherFields;
        return this;
    }

    public MapCustomerInfoBuilder isHeader(boolean header) {
        this.header = header;
        return this;
    }

    public MapCustomerInfo build() {
        return new MapCustomerInfo(id, otherFields, header);
    }
}