package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OsrmResponse implements Serializable {
	
	private String code;
	
	private List<OsrmRouteResponse> routes;
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public List<OsrmRouteResponse> getRoutes() {
		return routes;
	}
	
	public void setRoutes(List<OsrmRouteResponse> routes) {
		this.routes = routes;
	}
	
	@Override
	public String toString() {
		return "OsrmResponse [code=" + code + ", routes=" + routes + "]";
	}

	public String getDistanceFromResponse() {
		String distance = "";
		if (isNotEmptyRoutes()) {
			distance = routes.get( 0 ).getDistance();
		}
		return distance;
	}

	private boolean isNotEmptyRoutes() {
		return routes != null && routes.size() >= 1;
	}

	public CoordinateResponse[] getCoordinatesFromResponse() {
		CoordinateResponse[] coordinates = null;
		if (isNotEmptyRoutes() && routes.get( 0 ).getGeometry() != null) {
			coordinates = routes.get(0).getGeometry().getCoordinates();
		}
		return coordinates;
	}
	
}
