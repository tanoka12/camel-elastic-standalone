package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;

public class RoutingResponse implements Serializable {
	
	private final String id;
	
	private final String longitude;
	
	private final String latitude;
	
	private final String othersFields;
	
	public RoutingResponse(String id, String longitude, String latitude, String othersFields) {
		super();
		this.id = id;
		this.longitude = longitude;
		this.latitude = latitude;
		this.othersFields = othersFields;
	}
	
	public String getId() {
		return id;
	}
	
	public String getLongitude() {
		return longitude;
	}
	
	public String getLatitude() {
		return latitude;
	}
	
	public String getOthersFields() {
		return othersFields;
	}
	
}
