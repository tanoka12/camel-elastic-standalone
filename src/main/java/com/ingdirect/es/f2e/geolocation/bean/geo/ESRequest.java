package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface ESRequest {
	
	@JsonIgnore
	default public String getIndexId() {
		return UUID.randomUUID().toString();
	}
	
}
