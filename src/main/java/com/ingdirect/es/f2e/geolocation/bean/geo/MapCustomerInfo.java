package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by EH80OB on 22/05/2017.
 */
public class MapCustomerInfo implements Serializable, Routing{

    private final String id;

    private final String otherFields;

    private final boolean header;

    private GeoPoint location;

    private List<PoiMapDetail> nearestPoi;

    public MapCustomerInfo(String id, String otherFields, boolean header) {
        this.id = id;
        this.otherFields = otherFields;
        this.header = header;

    }

    public boolean isHeader() {
        return header;
    }

    public String getOtherFields() {
        return otherFields;
    }

    public String getId() {
        return id;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public List<PoiMapDetail> getNearestPoi() {
        return nearestPoi;
    }

    public void setNearestPoi(List<PoiMapDetail> nearestPoi) {
        this.nearestPoi = nearestPoi;
    }
}
