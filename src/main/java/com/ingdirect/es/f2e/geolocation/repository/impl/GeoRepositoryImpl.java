package com.ingdirect.es.f2e.geolocation.repository.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoPoint;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoRequest;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoResponse;
import com.ingdirect.es.f2e.geolocation.bean.geo.QueryParam;
import com.ingdirect.es.f2e.geolocation.builder.GeoQueryBuilder;
import com.ingdirect.es.f2e.geolocation.builder.GeoResponseBuilder;
import com.ingdirect.es.f2e.geolocation.client.impl.GeoElasticSearchClientImpl;
import com.ingdirect.es.f2e.geolocation.repository.GeoDao;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GeoRepositoryImpl extends ElasticSearchRestDaoImpl implements GeoDao {

    //private static final F2ELogger logger = F2ELogger.getLogger( GeoRepositoryImpl.class );

    private GeoQueryBuilder queryBuilder = new GeoQueryBuilder();

    private static final String ELASTIC_LOCATION_FIELD = "location";

    private static final String ELASTIC_LONGITUDE_FIELD = "lon";

    private static final String ELASTIC_LATITUDE_FIELD = "lat";

    public GeoRepositoryImpl(GeoElasticSearchClientImpl client, ObjectMapper mapper) {
        super(client, mapper);
    }

    @Override
    public List<GeoResponse> getLocations(GeoRequest location, String index, String mapping, QueryParam param) throws Exception {

        List<GeoResponse> locations = new ArrayList<>();

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(500).query(buildQuery(location, param));

        SearchRequest searchRequest = new SearchRequest(index).types(mapping).source(searchSourceBuilder);

        //logger.info( null, "::: Query ES  in index : " + index + " " + searchQuery );

        SearchResponse response = client.search(searchRequest);

        //logger.info( null, "::: Response ES :" + response );

        if (response.getHits().getTotalHits() > 0) {
            locations = extractResponse(response);
        }

        return locations;
    }

    private QueryBuilder buildQuery(GeoRequest location, QueryParam param) {
        if (param.isMatchQuery()) {
            return queryBuilder.querySearchByLocation(location, param.isExactQuery());
        } else {
            return queryBuilder.boolQuerySearchByLocation(location, param.isExactQuery());
        }
    }

    @Override
    public List<GeoResponse> getLocationsBool(GeoRequest location, String index, String mapping, boolean exactQuery) throws Exception {
        List<GeoResponse> locations = new ArrayList<>();


        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(500)
                .query(queryBuilder.boolQuerySearchByLocation(location, exactQuery));

        SearchRequest searchRequest = new SearchRequest(index).types(mapping).source(searchSourceBuilder);

        //logger.info( null, "::: Query ES  in index : " + index + " " + searchQuery );

        SearchResponse response = client.search(searchRequest);


        //logger.info( null, "::: Response ES :" + response );

        if (response.getHits().getTotalHits() > 0) {
            locations = extractResponse(response);
        }

        return locations;
    }

    private List<GeoResponse> extractResponse(SearchResponse response) {
        List<GeoResponse> locations = Arrays.stream(response.getHits().getHits())
                .map(item -> new GeoResponseBuilder(item.getSource()).source(item.getIndex()).build())
                .collect(Collectors.toList());
        return locations;
    }

    @Override
    public GeoPoint findLocationById(String index, String mapping, String id) throws Exception {

        GeoPoint geoPoint = null;
        GetResponse response = findObjectById(index, mapping, id);

        if (response.isExists()) {

            Map<String, Object> location = (Map<String, Object>) response.getSourceAsMap()
                    .get(ELASTIC_LOCATION_FIELD);

            String longitude = (String) location.get(ELASTIC_LONGITUDE_FIELD);
            String latitude = (String) location.get(ELASTIC_LATITUDE_FIELD);

            geoPoint = new GeoPoint(longitude, latitude);
        }

        return geoPoint;
    }
}
