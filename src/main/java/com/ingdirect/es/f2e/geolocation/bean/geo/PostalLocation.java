package com.ingdirect.es.f2e.geolocation.bean.geo;

public class PostalLocation {
	
	private final String town;
	
	private final String province;
	
	private final String community;
	
	private final String postalCode;
	
	public PostalLocation(String town, String province, String community, String postalCode) {
		this.town = town;
		this.province = province;
		this.community = community;
		this.postalCode = postalCode;
	}
	
	public String getTown() {
		return town;
	}
	
	public String getProvince() {
		return province;
	}
	
	public String getCommunity() {
		return community;
	}
	
	@Override
	public String toString() {
		return "PostalLocation [town=" + town + ", province=" + province + ", community=" + community + ", postalCode="
				+ postalCode + "]";
	}
	
	public String getPostalCode() {
		return postalCode;
	}
}
