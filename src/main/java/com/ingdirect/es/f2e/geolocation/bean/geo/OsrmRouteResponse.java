package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;

public class OsrmRouteResponse implements Serializable {
	
	private String duration;
	
	private String distance;
	
	private GeometryResponse geometry;
	
	public String getDuration() {
		return duration;
	}
	
	public void setDuration(String duration) {
		this.duration = duration;
	}
	
	public String getDistance() {
		return distance;
	}
	
	public GeometryResponse getGeometry() {
		return geometry;
	}
	
	public void setGeometry(GeometryResponse geometry) {
		this.geometry = geometry;
	}
	
	public void setDistance(String distance) {
		this.distance = distance;
	}
	
	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer( "OsrmRouteResponse{" );
		sb.append( "duration='" ).append( duration ).append( '\'' );
		sb.append( ", distance='" ).append( distance ).append( '\'' );
		sb.append( ", geometry=" ).append( geometry );
		sb.append( '}' );
		return sb.toString();
	}
}
