package com.ingdirect.es.f2e.geolocation.bean.geo;

public class GeoCsvBuilder {
	
	private String lat;
	private String lon;
	private String province;
	private String number;
	private String postalcode;
	private String street;
	private String town;
	private String state;
	
	public GeoCsvBuilder() {
	}
	
	public GeoCsvBuilder latitude(String lat) {
		this.lat = lat;
		return this;
	}
	
	public GeoCsvBuilder longitude(String lon) {
		this.lon = lon;
		return this;
	}
	
	public GeoCsvBuilder province(String province) {
		this.province = province;
		return this;
	}
	
	public GeoCsvBuilder number(String number) {
		this.number = number;
		return this;
	}
	
	public GeoCsvBuilder postalcode(String postalcode) {
		this.postalcode = postalcode;
		return this;
	}
	
	public GeoCsvBuilder street(String street) {
		this.street = street;
		return this;
	}
	
	public GeoCsvBuilder state(String state) {
		this.state = state;
		return this;
	}
	
	public GeoCsvBuilder town(String town) {
		this.town = town;
		return this;
	}
	
	public GeoCsv build() {
		
		GeoPointCsv point = new GeoPointCsv();
		point.setLat( this.lat );
		point.setLon( this.lon );
		
		GeoCsv geo = new GeoCsv();
		
		geo.setTown( this.town );
		geo.setNumber( this.number );
		geo.setPostalCode( this.postalcode );
		geo.setProvince( this.province );
		geo.setStreet( this.street );
		geo.setLocation( point );
		geo.setDistrict( state );
		
		return geo;
	}
}
