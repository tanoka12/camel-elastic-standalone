package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.util.ArrayList;
import java.util.List;

public class GeoResponse {
	
	protected static final String OPENADDRESS = "OPENADDRESS";
	protected static final String CARTOCIUDAD = "CARTOCIUDAD";
	
	private final String lon;
	
	private final String lat;
	
	private final String number;
	
	private final String viaName;
	
	private final String viaType;
	
	private final String town;
	
	private final String province;
	
	private final String postalCode;
	
	private final String state;
	
	private String source;
	
	private int success;
	
	public GeoResponse(String lon, String lat, String number, String viaName, String viaType, String town,
			String province, String postalCode, String state) {
		super();
		this.lon = lon;
		this.lat = lat;
		this.number = number;
		this.viaName = viaName;
		this.viaType = viaType;
		this.town = town;
		this.province = province;
		this.postalCode = postalCode;
		this.state = state;
	}
	
	public String getLon() {
		return lon;
	}
	
	public String getLat() {
		return lat;
	}
	
	public String getNumber() {
		return number;
	}
	
	public String getViaName() {
		return viaName;
	}
	
	public String getViaType() {
		return viaType;
	}
	
	public String getTown() {
		return town;
	}
	
	public String getProvince() {
		return province;
	}
	
	public String getPostalCode() {
		return postalCode;
	}
	
	public String getState() {
		return state;
	}
	
	public String getSource() {
		return source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	
	public int getSuccess() {
		return success;
	}
	
	public void setSuccess(int success) {
		this.success = success;
	}
	
	public List<String> getValues() {
		
		List<String> values = new ArrayList<String>();
		
		values.add( prettySource( source ) );
		values.add( Integer.toString( success ) );
		values.add( viaType );
		values.add( viaName );
		values.add( number );
		values.add( postalCode );
		return values;
		
	}
	
	private String prettySource(String source) {
		if (source != null && source.contains( CARTOCIUDAD.toLowerCase() )) {
			return CARTOCIUDAD;
		} else if (source != null && source.contains( OPENADDRESS.toLowerCase() )) {
			return OPENADDRESS;
		}
		return "";
	}
	
	@Override
	public String toString() {
		return "GeoResponse [lon=" + lon + ", lat=" + lat + ", number=" + number + ", viaName=" + viaName
				+ ", viaType=" + viaType + ", town=" + town + ", province=" + province + ", postalCode=" + postalCode
				+ ", state=" + state + ", source=" + source + ", success=" + success + "]\n";
	}
}
