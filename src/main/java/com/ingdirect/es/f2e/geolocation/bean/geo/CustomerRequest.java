package com.ingdirect.es.f2e.geolocation.bean.geo;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerRequest implements ESRequest {
	
	private String id;
	
	private String name;
	
	private String surName1;
	
	private String surName2;
	
	private String viaType;
	
	private String viaName;
	
	private String number;
	
	private String floor;
	
	private String additionalInfo;
	
	private String city;
	
	private String province;
	
	private String country;
	
	private String postalCode;
	
	private final GeoPoint location;
	
	private final int success;
	
	private final String source;
	
	@JsonCreator
	public CustomerRequest(@JsonProperty("id") String id,
						   @JsonProperty("name") String name,
						   @JsonProperty("surName1") String surName1,
						   @JsonProperty("surName2") String surName2,
						   @JsonProperty("viaType") String viaType,
						   @JsonProperty("viaName") String viaName,
						   @JsonProperty("number") String number,
						   @JsonProperty("floor") String floor,
						   @JsonProperty("additionalInfo") String additionalInfo,
						   @JsonProperty("city") String city,
						   @JsonProperty("province") String province,
						   @JsonProperty("country") String country,
						   @JsonProperty("postalCode") String postalCode,
						   @JsonProperty("success") int success,
						   @JsonProperty("source") String source,
						   @JsonProperty("location") GeoPoint location) {

		super();
		this.id = id;
		this.name = name;
		this.surName1 = surName1;
		this.surName2 = surName2;
		this.viaType = viaType;
		this.viaName = viaName;
		this.number = number;
		this.floor = floor;
		this.additionalInfo = additionalInfo;
		this.city = city;
		this.province = province;
		this.country = country;
		this.postalCode = postalCode;
		this.location = location;
		this.success = success;
		this.source = source;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurName1() {
		return surName1;
	}
	
	public void setSurName1(String surName1) {
		this.surName1 = surName1;
	}
	
	public String getSurName2() {
		return surName2;
	}
	
	public void setSurName2(String surName2) {
		this.surName2 = surName2;
	}
	
	public String getViaType() {
		return viaType;
	}
	
	public void setViaType(String viaType) {
		this.viaType = viaType;
	}
	
	public String getViaName() {
		return viaName;
	}
	
	public void setViaName(String viaName) {
		this.viaName = viaName;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String getFloor() {
		return floor;
	}
	
	public void setFloor(String floor) {
		this.floor = floor;
	}
	
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getProvince() {
		return province;
	}
	
	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getPostalCode() {
		return postalCode;
	}
	
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	public GeoPoint getLocation() {
		return location;
	}
	
	@Override
	@JsonIgnore
	public String getIndexId() {
		return getId();
	}
	
	public int getSuccess() {
		return success;
	}
	
	public String getSource() {
		return source;
	}
	
}
