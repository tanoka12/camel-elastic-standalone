package com.ingdirect.es.f2e.geolocation.repository.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingdirect.es.f2e.geolocation.bean.geo.CategoryRequest;
import com.ingdirect.es.f2e.geolocation.bean.geo.CategoryResponse;
import com.ingdirect.es.f2e.geolocation.bean.geo.MapQueryRequest;
import com.ingdirect.es.f2e.geolocation.bean.geo.PoiSubCategoryResponse;
import com.ingdirect.es.f2e.geolocation.builder.PoiSubCategoryResponseBuilder;
import com.ingdirect.es.f2e.geolocation.client.impl.GeoElasticSearchClientImpl;
import com.ingdirect.es.f2e.geolocation.ctes.PoiConstant;
import com.ingdirect.es.f2e.geolocation.repository.SearchCategoryDao;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.GeoDistanceQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by EH80OB on 17/04/2017.
 */

public class SearchCategoryDaoImpl extends ElasticSearchRestDaoImpl implements SearchCategoryDao {

    //private static final F2ELogger logger = F2ELogger.getLogger( SearchCategoryDaoImpl.class );
    private static final String TARGET_MAPPER = "geolocation";

    public SearchCategoryDaoImpl(GeoElasticSearchClientImpl client, ObjectMapper mapper) {
        super(client, mapper);
    }

    @Override
    public List<PoiSubCategoryResponse> getPoiByCategoryAndBuffer(MapQueryRequest queryRequest) throws Exception {

        QueryBuilder queryCategoryBuffer = createCategoryBufferQuery(queryRequest);

        return executeQuery(queryCategoryBuffer);

    }

    @Override
    public List<PoiSubCategoryResponse> getPoiByCategory(CategoryRequest request) throws Exception {

        QueryBuilder queryCategory = createCategoryQuery(request);
        return executeQuery(queryCategory);
    }

    private List<PoiSubCategoryResponse> executeQuery(QueryBuilder queryBuilder) throws Exception {

        List<PoiSubCategoryResponse> locations = new ArrayList<>();

        String[] poiIndex = getPoisIndex();

        final Scroll scroll = new Scroll(TimeValue.timeValueMinutes(1L));

        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(1000).query(queryBuilder);

        SearchRequest searchRequest = new SearchRequest(poiIndex).types(TARGET_MAPPER).scroll(scroll).source(searchSourceBuilder);

        //logger.info(null, "::: Query : " + searchSourceBuilder);

        SearchResponse searchResponse = client.search(searchRequest);

        String scrollId = searchResponse.getScrollId();

        SearchHit[] searchHits = searchResponse.getHits().getHits();


        while (searchHits != null && searchHits.length > 0) {

            final List<PoiSubCategoryResponse> partialLocations = extractResponse(searchResponse);

            locations.addAll(partialLocations);

            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId).scroll(scroll);

            searchResponse = client.searchScroll(scrollRequest);

            scrollId = searchResponse.getScrollId();

            searchHits = searchResponse.getHits().getHits();
        }

        return locations;
    }

    @Override
    public List<CategoryResponse> getAllCategories() throws Exception {

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(0).query(QueryBuilders.matchAllQuery())
                .aggregation(AggregationBuilders.terms("distinct_subcategory").field("subcategory").size(2000));

        SearchRequest searchRequest = new SearchRequest("geo-poi-*").types(TARGET_MAPPER).source(searchSourceBuilder);

        //logger.info(null, "::: Query : " + searchQueryBuilder);

        SearchResponse response = client.search(searchRequest);

        Terms aggregationResult = response.getAggregations().get("distinct_subcategory");

        return aggregationResult.getBuckets().stream()
                .map(bucket -> new CategoryResponse((String) bucket.getKey()))
                .collect(Collectors.toList());

    }

    private List<PoiSubCategoryResponse> extractResponse(SearchResponse response) {
        final List<PoiSubCategoryResponse> poiResponses = Arrays.stream(response.getHits().getHits())
                .map(result -> new PoiSubCategoryResponseBuilder(result.getIndex(), result.getSource())
                        .setId(result.getId()).build())
                .collect(Collectors.toList());
        return poiResponses;
    }

    private QueryBuilder createCategoryQuery(CategoryRequest request) {

        final BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        request.getCategories().stream()
                .forEach(cat -> boolQueryBuilder.should(QueryBuilders.matchQuery("subcategory", cat)));
        return boolQueryBuilder;

    }

    public QueryBuilder createCategoryBufferQuery(MapQueryRequest queryRequest) {

        final QueryBuilder categoryQueryBuilder = createCategoryQuery(queryRequest.getCategoryRequest());

        final BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

        GeoDistanceQueryBuilder qb = QueryBuilders
                .geoDistanceQuery("location")
                .point(Double.parseDouble(queryRequest.getLocation().getLat()),
                        Double.parseDouble(queryRequest.getLocation().getLon()))
                .distance(Double.parseDouble(queryRequest.getBuffer()), DistanceUnit.METERS);


        boolQueryBuilder.must(categoryQueryBuilder).filter(qb);

        return boolQueryBuilder;

    }

    public String[] getPoisIndex() throws Exception {
        String atmIndex = getRecentIndexByPrefix(PoiConstant.INDEX_POI_ATM + "*");
        String twypIndex = getRecentIndexByPrefix(PoiConstant.INDEX_POI_TWYP + "*");
        String osmIndex = getRecentIndexByPrefix(PoiConstant.INDEX_POI_OSM + "*");

        return new String[]{atmIndex, twypIndex, osmIndex};
    }
}
