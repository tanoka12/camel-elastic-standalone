package com.ingdirect.es.f2e.geolocation.bean.geo;

public class AtmRequestBuilder2 {
    private GeoPoint location;
    private String id;
    private String red4b;
    private String red6000;
    private String servired;
    private String entityCode;
    private String entity;
    private String branchCode;
    private String branch;
    private String atmCode;
    private String ordinal;
    private String province;
    private String state;
    private String postalCode;
    private String town;
    private String viaType;
    private String viaName;
    private String number;
    private String rdo;
    private int success;
    private String source;

    public AtmRequestBuilder2 setLocation(GeoPoint location) {
        this.location = location;
        return this;
    }

    public AtmRequestBuilder2 setId(String id) {
        this.id = id;
        return this;
    }

    public AtmRequestBuilder2 setRed4b(String red4b) {
        this.red4b = red4b;
        return this;
    }

    public AtmRequestBuilder2 setRed6000(String red6000) {
        this.red6000 = red6000;
        return this;
    }

    public AtmRequestBuilder2 setServired(String servired) {
        this.servired = servired;
        return this;
    }

    public AtmRequestBuilder2 setEntityCode(String entityCode) {
        this.entityCode = entityCode;
        return this;
    }

    public AtmRequestBuilder2 setEntity(String entity) {
        this.entity = entity;
        return this;
    }

    public AtmRequestBuilder2 setBranchCode(String branchCode) {
        this.branchCode = branchCode;
        return this;
    }

    public AtmRequestBuilder2 setBranch(String branch) {
        this.branch = branch;
        return this;
    }

    public AtmRequestBuilder2 setAtmCode(String atmCode) {
        this.atmCode = atmCode;
        return this;
    }

    public AtmRequestBuilder2 setOrdinal(String ordinal) {
        this.ordinal = ordinal;
        return this;
    }

    public AtmRequestBuilder2 setProvince(String province) {
        this.province = province;
        return this;
    }

    public AtmRequestBuilder2 setState(String state) {
        this.state = state;
        return this;
    }

    public AtmRequestBuilder2 setPostalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public AtmRequestBuilder2 setTown(String town) {
        this.town = town;
        return this;
    }

    public AtmRequestBuilder2 setViaType(String viaType) {
        this.viaType = viaType;
        return this;
    }

    public AtmRequestBuilder2 setViaName(String viaName) {
        this.viaName = viaName;
        return this;
    }

    public AtmRequestBuilder2 setNumber(String number) {
        this.number = number;
        return this;
    }

    public AtmRequestBuilder2 setRdo(String rdo) {
        this.rdo = rdo;
        return this;
    }

    public AtmRequestBuilder2 setSuccess(int success) {
        this.success = success;
        return this;
    }

    public AtmRequestBuilder2 setSource(String source) {
        this.source = source;
        return this;
    }

    public AtmRequest createAtmRequest() {
        return new AtmRequest(location, id, red4b, red6000, servired, entityCode, entity, branchCode, branch, atmCode, ordinal, province, state, postalCode, town, viaType, viaName, number, rdo, success, source);
    }
}