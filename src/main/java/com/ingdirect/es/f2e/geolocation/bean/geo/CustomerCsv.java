package com.ingdirect.es.f2e.geolocation.bean.geo;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(separator = "\t")
public class CustomerCsv implements GeoLocation {
	
	@DataField(pos = 1, required = true)
	private String id;
	
	@DataField(pos = 2, required = false)
	private String name;
	
	@DataField(pos = 3, required = true)
	private String surName1;
	
	@DataField(pos = 4, required = false)
	private String surName2;
	
	@DataField(pos = 5, required = false)
	private String viaType;
	
	@DataField(pos = 6, required = true)
	private String viaName;
	
	@DataField(pos = 7, required = true)
	private String number;
	
	@DataField(pos = 8, required = true)
	private String floor;
	
	@DataField(pos = 9, required = false)
	private String additionalInfo;
	
	@DataField(pos = 10, required = true)
	private String town;
	
	@DataField(pos = 11, required = true)
	private String province;
	
	@DataField(pos = 12, required = true)
	private String country;
	
	@DataField(pos = 13, required = true)
	private String postalCode;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurName1() {
		return surName1;
	}
	
	public void setSurName1(String surName1) {
		this.surName1 = surName1;
	}
	
	public String getSurName2() {
		return surName2;
	}
	
	public void setSurName2(String surName2) {
		this.surName2 = surName2;
	}
	
	@Override
	public String getViaType() {
		return viaType;
	}
	
	public void setViaType(String viaType) {
		this.viaType = viaType;
	}
	
	@Override
	public String getViaName() {
		return viaName;
	}
	
	public void setViaName(String viaName) {
		this.viaName = viaName;
	}
	
	@Override
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String getFloor() {
		return floor;
	}
	
	public void setFloor(String floor) {
		this.floor = floor;
	}
	
	public String getAdditionalInfo() {
		return additionalInfo;
	}
	
	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	
	@Override
	public String getTown() {
		return town;
	}
	
	public void setTown(String town) {
		this.town = town;
	}
	
	public String getProvince() {
		return province;
	}
	
	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	@Override
	public String getPostalCode() {
		return postalCode;
	}
	
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	@Override
	public String toString() {
		return "CustomerCsv [id=" + id + ", name=" + name + ", surName1=" + surName1 + ", surName2=" + surName2
				+ ", viaType=" + viaType + ", viaName=" + viaName + ", number=" + number + ", floor=" + floor
				+ ", additionalInfo=" + additionalInfo + ", town=" + town + ", province=" + province + ", country="
				+ country + ", postalCode=" + postalCode + "]";
	}
	
}
