package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("serial")
public class GeoRequest implements Serializable, ESRequest {
	
	private final GeoPoint location;
	
	private final String number;
	
	private final String town;
	
	private final String province;
	
	private final String postalCode;
	
	private final String viaName;
	
	private final String viaType;
	
	private final String state;
	
	public GeoRequest(GeoPoint location, String number, String town, String province, String postalCode,
			String viaName, String viaType, String state) {
		super();
		this.location = location;
		this.number = number;
		this.town = town;
		this.province = province;
		this.postalCode = postalCode;
		this.viaName = viaName;
		this.viaType = viaType;
		this.state = state;
	}
	
	public GeoPoint getLocation() {
		return location;
	}
	
	public String getNumber() {
		return number;
	}
	
	public String getTown() {
		return town;
	}
	
	public String getProvince() {
		return province;
	}

	public String getPostalCode() {
		return postalCode;
	}
	
	@JsonProperty("via_name")
	public String getViaName() {
		return viaName;
	}
	
	public String getState() {
		return state;
	}
	
	@JsonProperty("via_type")
	public String getViaType() {
		return viaType;
	}
	
	@Override
	public String toString() {
		return "GeoRequest [location=" + location + ", number=" + number + ", town=" + town + ", province=" + province
				+ ", postalCode=" + postalCode + ", viaName=" + viaName + ", viaType=" + viaType + ", state=" + state
				+ "]";
	}
	
}
