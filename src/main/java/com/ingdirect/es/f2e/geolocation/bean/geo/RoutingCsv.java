package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("serial")
public class RoutingCsv implements Serializable, ESRequest, Routing {
	
	private final String id;
	private final GeoPoint location;
	private final String otherFields;
	private final boolean header;
	private final boolean error;
	
	private String distance;
	
	public RoutingCsv(String id, GeoPoint location, String otherFields, boolean header, boolean error) {
		super();
		this.id = id;
		this.location = location;
		this.otherFields = otherFields;
		this.header = header;
		this.error = error;
	}
	
	public String getId() {
		return id;
	}
	
	public String getOtherFields() {
		return otherFields;
	}
	
	public boolean isHeader() {
		return header;
	}
	
	public boolean isError() {
		return error;
	}
	
	public String getDistance() {
		return distance;
	}
	
	public void setDistance(String distance) {
		this.distance = distance;
	}
	
	public String toFileRegister() {
		
		List<String> listValues = new ArrayList<String>();
		
		listValues.add( id );
		listValues.add( location.getLon() );
		listValues.add( location.getLat() );
		listValues.add( otherFields );
		
		String register = listValues.stream().map( item -> {
			return ( item == null ? "" : item );
		} ).collect( Collectors.joining( "," ) );
		
		return register;
		
	}
	
	@Override
	public String toString() {
		return "RoutingCsv [id=" + id + ", location=" + location + ", otherFields=" + otherFields + ", header="
				+ header + ", error=" + error + ", distance=" + distance + "]";
	}
	
	public GeoPoint getLocation() {
		return location;
	}
	
}
