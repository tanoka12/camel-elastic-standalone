package com.ingdirect.es.f2e.geolocation.bean.geo;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(separator = "\t")
public class AtmCsv implements GeoLocation {
	
	@DataField(pos = 1, required = false)
	private String id;
	
	@DataField(pos = 2, required = false)
	private String red4B;
	
	@DataField(pos = 3, required = false)
	private String red6000;
	
	@DataField(pos = 4, required = false)
	private String servired;
	
	@DataField(pos = 5, required = false)
	private String entityCode;
	
	@DataField(pos = 6, required = false)
	private String entity;
	
	@DataField(pos = 7, required = false)
	private String branchCode;
	
	@DataField(pos = 8, required = false)
	private String branch;
	
	@DataField(pos = 9, required = false)
	private String atmCode;
	
	@DataField(pos = 10, required = false)
	private String ordinal;
	
	@DataField(pos = 11, required = false)
	private String province;
	
	@DataField(pos = 12, required = false)
	private String state;
	
	@DataField(pos = 13, required = true)
	private String postalCode;
	
	@DataField(pos = 14, required = false)
	private String town;
	
	@DataField(pos = 15, required = false)
	private String viaType;
	
	@DataField(pos = 16, required = true)
	private String viaName;
	
	@DataField(pos = 17, required = true)
	private String number;
	
	@DataField(pos = 18, required = false)
	private String rdo;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getRed4B() {
		return red4B;
	}
	
	public void setRed4B(String red4b) {
		red4B = red4b;
	}
	
	public String getRed6000() {
		return red6000;
	}
	
	public void setRed6000(String red6000) {
		this.red6000 = red6000;
	}
	
	public String getServired() {
		return servired;
	}
	
	public void setServired(String servired) {
		this.servired = servired;
	}
	
	public String getEntityCode() {
		return entityCode;
	}
	
	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}
	
	public String getEntity() {
		return entity;
	}
	
	public void setEntity(String entity) {
		this.entity = entity;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	
	public String getBranch() {
		return branch;
	}
	
	public void setBranch(String branch) {
		this.branch = branch;
	}
	
	public String getAtmCode() {
		return atmCode;
	}
	
	public void setAtmCode(String atmCode) {
		this.atmCode = atmCode;
	}
	
	public String getOrdinal() {
		return ordinal;
	}
	
	public void setOrdinal(String ordinal) {
		this.ordinal = ordinal;
	}
	
	public String getProvince() {
		return province;
	}
	
	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	@Override
	public String getPostalCode() {
		return postalCode;
	}
	
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	public String getTown() {
		return town;
	}
	
	public void setTown(String town) {
		this.town = town;
	}
	
	@Override
	public String getViaType() {
		return viaType;
	}
	
	public void setViaType(String viaType) {
		this.viaType = viaType;
	}
	
	@Override
	public String getViaName() {
		return viaName;
	}
	
	public void setViaName(String viaName) {
		this.viaName = viaName;
	}
	
	@Override
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String getRdo() {
		return rdo;
	}
	
	public void setRdo(String rdo) {
		this.rdo = rdo;
	}
	
	@Override
	public String toString() {
		return "AtmCsv [id=" + id + ", red4B=" + red4B + ", red6000=" + red6000 + ", servired=" + servired
				+ ", entityCode=" + entityCode + ", entity=" + entity + ", branchCode=" + branchCode + ", branch="
				+ branch + ", atmCode=" + atmCode + ", ordinal=" + ordinal + ", province=" + province + ", state="
				+ state + ", postalCode=" + postalCode + ", town=" + town + ", viaType=" + viaType + ", viaName="
				+ viaName + ", number=" + number + ", rdo=" + rdo + "]";
	}
	
}
