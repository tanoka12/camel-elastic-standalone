package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by EH80OB on 01/06/2017.
 */
public class CategoryInfo implements Serializable{

    private final String name;
    private final String iconId;
    private final String path;
    private final int offsetx;
    private final int offsety;


    public CategoryInfo(String name, String iconId, String path, int offsetx, int offsety) {
        this.name = name;
        this.iconId = iconId;
        this.path = path;
        this.offsetx = offsetx;
        this.offsety = offsety;
    }

    public String getName() {
        return name;
    }

    public String getIconId() {
        return iconId;
    }

    public String getPath() {
        return path;
    }

    public int getOffsetx() {
        return offsetx;
    }

    public int getOffsety() {
        return offsety;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        CategoryInfo that = (CategoryInfo) o;

        return new EqualsBuilder()
            .append(name, that.name)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(name)
            .toHashCode();
    }
}
