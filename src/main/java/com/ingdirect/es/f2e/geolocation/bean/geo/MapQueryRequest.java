package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by EH80OB on 22/05/2017.
 */
public class MapQueryRequest implements Serializable {
	
	private final String buffer;
	
	private final String profile;
	
	private final int poiLimit;
	
	private final String processType;
	
	private GeoPoint location;
	
	private final List<CategoryInfo> categoriesInfo;
	
	private final Integer width;
	
	private final Integer height;
	
	public MapQueryRequest(String buffer, String profile, int poiLimit, String processType,
			List<CategoryInfo> categoriesInfo, Integer width, Integer height) {
		this.buffer = buffer;
		this.profile = profile;
		this.poiLimit = poiLimit;
		this.processType = processType;
		this.categoriesInfo = categoriesInfo;
		this.width = width;
		this.height = height;
	}
	
	public int getPoiLimit() {
		return poiLimit;
	}
	
	public String getBuffer() {
		return buffer;
	}
	
	public CategoryRequest getCategoryRequest() {
		CategoryRequest categoryRequest = new CategoryRequest();
		categoriesInfo.stream().forEach( categoryInfo -> categoryRequest.addCategory( categoryInfo.getName() ) );
		return categoryRequest;
		
	}
	
	public String getProfile() {
		return profile;
	}
	
	public GeoPoint getLocation() {
		return location;
	}
	
	public void setLocation(GeoPoint location) {
		this.location = location;
	}
	
	public CategoryInfo findCategoryByName(String categoryName) {
		return categoriesInfo.stream().filter( categoryInfo -> categoryName.equals( categoryInfo.getName() ) )
				.findFirst().orElse( null );
	}
	
	public String getProcessType() {
		return processType;
	}
	
	public Integer getWidth() {
		return width;
	}
	
	public Integer getHeight() {
		return height;
	}
	
}
