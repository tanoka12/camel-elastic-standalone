package com.ingdirect.es.f2e.geolocation.bean.geo;

/**
 * Created by EH80OB on 18/04/2017.
 */
public class CategoryResponse {

    public final String name;

    public CategoryResponse(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
