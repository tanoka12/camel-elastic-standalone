package com.ingdirect.es.f2e.geolocation.builder;

import com.ingdirect.es.f2e.geolocation.bean.geo.GeoPoint;
import com.ingdirect.es.f2e.geolocation.bean.geo.TwypCsv;
import com.ingdirect.es.f2e.geolocation.bean.geo.TwypRequest;

public class TwypRequestBuilder {
	
	private final String id;
	
	private String postalCode;
	
	private final GeoPoint location;
	
	private String town;
	
	private String province;
	
	private final String viaName;
	
	private String state;
	
	private final String merchantName;
	
	public TwypRequestBuilder(TwypCsv twip) {
		
		location = new GeoPoint( twip.getLongitude(), twip.getLatitude() );
		
		this.postalCode = twip.getPostalCode();
		
		this.id = twip.getId();
		
		this.town = twip.getTown();
		
		this.viaName = twip.getViaName();
		
		this.merchantName = twip.getMerchantName();
		
	}
	
	public TwypRequestBuilder town(String town) {
		this.town = town;
		return this;
	}
	
	public TwypRequestBuilder state(String state) {
		this.state = state;
		return this;
	}
	
	public TwypRequestBuilder province(String province) {
		this.province = province;
		return this;
	}
	
	public TwypRequest build() {
		
		TwypRequest twypRequest = new TwypRequest( id, postalCode, location, town, province, viaName, state,
				merchantName );
		
		return twypRequest;
	}
	
	public TwypRequestBuilder postalCode(String postalCode) {
		this.postalCode = postalCode;
		return this;
	}
	
}
