package com.ingdirect.es.f2e.geolocation.bean.geo;

/**
 * Created by EH80OB on 25/04/2017.
 */
public class IndexHeaderLocation {

    private final int id;

    private final int longitude;

    private final int latitude;

    private final int othersField;


    public IndexHeaderLocation(int id, int longitude, int latitude, int othersField) {
        this.id = id;
        this.longitude = longitude;
        this.latitude = latitude;
        this.othersField = othersField;
    }


    public int getId() {
        return id;
    }

    public int getLongitude() {
        return longitude;
    }

    public int getLatitude() {
        return latitude;
    }

    public int getOthersField() {
        return othersField;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("IndexHeaderLocation{");
        sb.append("id=").append(id);
        sb.append(", longitude=").append(longitude);
        sb.append(", latitude=").append(latitude);
        sb.append(", othersField=").append(othersField);
        sb.append('}');
        return sb.toString();
    }
}
