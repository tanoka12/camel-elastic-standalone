package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@SuppressWarnings("serial")
public class GeoPoint implements Serializable {
	
	private final String lon;
	
	private final String lat;

	@JsonCreator
	public GeoPoint(@JsonProperty("lon") String lon,  @JsonProperty("lat") String lat) {
		super();
		this.lon = lon;
		this.lat = lat;
	}


	public String getLon() {
		return lon;
	}
	
	public String getLat() {
		return lat;
	}
	
	@Override
	public String toString() {
		return "GeoPoint [lon=" + lon + ", lat=" + lat + "]";
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        GeoPoint geoPoint = (GeoPoint) o;

        return new EqualsBuilder
				()
            .append(lon, geoPoint.lon)
            .append(lat, geoPoint.lat)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(lon)
            .append(lat)
            .toHashCode();
    }
}
