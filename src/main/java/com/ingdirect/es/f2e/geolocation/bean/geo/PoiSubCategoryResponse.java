package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;

public class PoiSubCategoryResponse implements Serializable {

	private final String id;

	private final String longitude;

	private final String latitude;

	private final String relevantInfo;

	private final String subcategory;

	private final String index;

	public PoiSubCategoryResponse(String id, String longitude, String latitude, String relevantInfo, String subcategory, String index) {
		super();
		this.id = id;
		this.longitude = longitude;
		this.latitude = latitude;
		this.relevantInfo = relevantInfo;
		this.subcategory = subcategory;
		this.index = index;
	}
	
	public String getId() {
		return id;
	}

	public String getIndex() {
		return index;
	}

	public String getLongitude() {
		return longitude;
	}

	public String getSubcategory() {return subcategory;	}

	public String getLatitude() {
		return latitude;
	}
	
	public String getRelevantInfo() {
		return relevantInfo;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("PoiSubCategoryResponse{");
		sb.append("id='").append(id).append('\'');
		sb.append(", longitude='").append(longitude).append('\'');
		sb.append(", latitude='").append(latitude).append('\'');
		sb.append(", relevantInfo='").append(relevantInfo).append('\'');
		sb.append(", subcategory='").append(subcategory).append('\'');
		sb.append(", index='").append(index).append('\'');
		sb.append('}');
		return sb.toString();
	}

	public String toFileRegister(){

		final StringBuffer sb = new StringBuffer(id);
		sb.append(longitude);
		sb.append(latitude);
		sb.append(subcategory);
		return sb.toString();
	}
}
