package com.ingdirect.es.f2e.geolocation.repository.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingdirect.es.f2e.geolocation.bean.geo.ErrorESResponse;
import com.ingdirect.es.f2e.geolocation.bean.geo.Routing;
import com.ingdirect.es.f2e.geolocation.bean.geo.RoutingCsv;
import com.ingdirect.es.f2e.geolocation.builder.RoutingCsvBuilder;
import com.ingdirect.es.f2e.geolocation.client.impl.GeoElasticSearchClientImpl;
import com.ingdirect.es.f2e.geolocation.repository.RoutingDao;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.GeoDistanceQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

public class RoutingDaoImpl extends ElasticSearchRestDaoImpl implements RoutingDao {

    //private static final F2ELogger logger = F2ELogger.getLogger( GeoRepositoryImpl.class );

    private static final String TARGET_MAPPER = "routing_target";

    private static final String HEADER_MAPPER = "header";

    public RoutingDaoImpl(GeoElasticSearchClientImpl client, ObjectMapper mapper) {
        super(client, mapper);
    }

    @Override
    public String getRoutingHeader(String uuidIndex) throws Exception {

        final GetResponse response = findObjectById(uuidIndex, HEADER_MAPPER, "1");

        return (String) response.getSourceAsMap().get("header");
    }

    @Override
    public List<RoutingCsv> getNearestLocations(String uuidIndex, Routing routingLocation, String buffer) throws Exception {

        List<RoutingCsv> locations = new ArrayList<>();

        GeoDistanceQueryBuilder qb = QueryBuilders
                .geoDistanceQuery("location")
                .point(Double.parseDouble(routingLocation.getLocation().getLat()),
                        Double.parseDouble(routingLocation.getLocation().getLon()))
                .distance(Double.parseDouble(buffer), DistanceUnit.METERS);


        //logger.info( null, "::: Query ES  in index : " + uuidIndex + " " + searchQuery );

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(500).query(qb);

        SearchRequest searchRequest = new SearchRequest(uuidIndex).types(TARGET_MAPPER).source(searchSourceBuilder);


        SearchResponse response = client.search(searchRequest);

        //logger.info( null, "::: Response ES :" + response );

        if (response.getHits().getTotalHits() > 0) {
            locations = extractResponse(response);
        }

        return locations;

    }

    private List<RoutingCsv> extractResponse(SearchResponse response) {
        List<RoutingCsv> locations = Arrays.stream(response.getHits().getHits())
                .map(item -> new RoutingCsvBuilder(item.getSource()).build()).collect(Collectors.toList());
        return locations;
    }

    @Override
    public ErrorESResponse insertRoutingHeader(String uuidIndex, RoutingCsv header) throws Exception {

        IndexRequest request = new IndexRequest(uuidIndex, HEADER_MAPPER, "1").source(jsonBuilder()
                        .startObject()
                        .field("header", buildHeaderField(header))
                        .endObject());

        IndexResponse response = client.index(request);
        return new ErrorESResponse(response.getId(), response.getResult().toString());
    }

    private String buildHeaderField(RoutingCsv header) {
        List<String> listFields = Arrays.asList(header.getId(), header.getLocation().getLon(), header.getLocation()
                        .getLat(),
                header.getOtherFields());
        String join = String.join(",", listFields);
        return join;
    }

    @Override
    public List<ErrorESResponse> insertRoutingTargets(String uuidIndex, List<RoutingCsv> targets) throws Exception {
        final List<ErrorESResponse> response = insertLocations(uuidIndex, TARGET_MAPPER, targets);
        refreshIndex(uuidIndex);
        return response;

    }

}
