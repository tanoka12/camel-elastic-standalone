package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;

/**
 * Created by EH80OB on 25/04/2017.
 */
public class DistanceLocation implements Serializable{

    private final GeoDistance source;

    private final GeoDistance target;

    private final boolean header;

    private String distance;


    public DistanceLocation(GeoDistance source, GeoDistance target, boolean header) {
        this.source = source;
        this.target = target;
        this.header = header;
    }

    public GeoDistance getSource() {
        return source;
    }

    public GeoDistance getTarget() {
        return target;
    }

    public boolean isHeader() {
        return header;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DistanceLocation{");
        sb.append("source=").append(source);
        sb.append(", target=").append(target);
        sb.append(", header=").append(header);
        sb.append(", distance='").append(distance).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
