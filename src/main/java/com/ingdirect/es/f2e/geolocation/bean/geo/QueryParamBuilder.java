package com.ingdirect.es.f2e.geolocation.bean.geo;

public class QueryParamBuilder {
    private boolean exactQuery = true;
    private boolean matchQuery = true;

    public QueryParamBuilder() {
    }

    public QueryParamBuilder withExactQuery(boolean exactQuery) {
        this.exactQuery = exactQuery;
        return this;
    }

    public QueryParamBuilder withMatchQuery(boolean matchQuery) {
        this.matchQuery = matchQuery;
        return this;
    }

    public QueryParam build() {
        return new QueryParam(exactQuery, matchQuery);
    }
}