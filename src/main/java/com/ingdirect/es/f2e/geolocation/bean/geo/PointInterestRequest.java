package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class PointInterestRequest implements Serializable, ESRequest {
	
	private final String id;
	
	private final String uid;
	
	private final String version;
	
	private final String changeset;
	
	private final String timestamp;
	
	private final String category;
	
	private final String subcategory;
	
	private final String name;
	
	private final GeoPoint location;
	
	private final String other;
	
	public PointInterestRequest(String id, String uid, String version, String changeset, String timestamp,
			String category, String subcategory, String name, GeoPoint location, String other) {
		super();
		this.id = id;
		this.uid = uid;
		this.version = version;
		this.changeset = changeset;
		this.timestamp = timestamp;
		this.category = category;
		this.subcategory = subcategory;
		this.name = name;
		this.location = location;
		this.other = other;
	}
	
	public String getId() {
		return id;
	}
	
	public String getUid() {
		return uid;
	}
	
	public String getVersion() {
		return version;
	}
	
	public String getChangeset() {
		return changeset;
	}
	
	public String getTimestamp() {
		return timestamp;
	}
	
	public String getCategory() {
		return category;
	}
	
	public String getSubcategory() {
		return subcategory;
	}
	
	public String getName() {
		return name;
	}
	
	public GeoPoint getLocation() {
		return location;
	}
	
	public String getOther() {
		return other;
	}
	
	//	@Override
	//	@JsonIgnore
	//	public String getIndexId() {
	//		return getId();
	//	}
	
	@Override
	public String toString() {
		return "PointInterestRequest [id=" + id + ", uid=" + uid + ", version=" + version + ", changeset=" + changeset
				+ ", timestamp=" + timestamp + ", category=" + category + ", subcategory=" + subcategory + ", name="
				+ name + ", location=" + location + ", other=" + other + "]";
	}
	
}
