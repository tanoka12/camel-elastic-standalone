package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;
import java.util.List;

public class RoutingProcessResponse implements Serializable {
	
	private RoutingCsv source;
	
	private List<RoutingCsv> targets;
	
	public RoutingProcessResponse(RoutingCsv source) {
		super();
		this.source = source;
	}
	
	public RoutingCsv getSource() {
		return source;
	}
	
	public void setSource(RoutingCsv source) {
		this.source = source;
	}
	
	public List<RoutingCsv> getTargets() {
		return targets;
	}
	
	public void setTargets(List<RoutingCsv> targets) {
		this.targets = targets;
	}
	
}
