package com.ingdirect.es.f2e.geolocation.builder;

import java.util.ArrayList;
import java.util.List;

import com.ingdirect.es.f2e.geolocation.bean.geo.GeoLocationCsv;

public class GeoLocationCsvBuilder {
	
	private String id;
	private String viaType;
	private String viaName;
	private String number;
	private String latitude;
	private String longitude;
	private String postalCode;
	private List<String> dynamicColumn = new ArrayList<String>();
	private List<String> resultColumn = new ArrayList<String>();
	private boolean header;
	
	public GeoLocationCsvBuilder id(String id) {
		this.id = id;
		return this;
	}
	
	public GeoLocationCsvBuilder viaType(String viaType) {
		this.viaType = viaType;
		return this;
	}
	
	public GeoLocationCsvBuilder latitude(String latitude) {
		this.latitude = latitude;
		return this;
	}
	
	public GeoLocationCsvBuilder longitude(String longitude) {
		this.longitude = longitude;
		return this;
	}
	
	public GeoLocationCsvBuilder viaName(String viaName) {
		this.viaName = viaName;
		return this;
	}
	
	public GeoLocationCsvBuilder number(String number) {
		this.number = number;
		return this;
	}
	
	public GeoLocationCsvBuilder postalCode(String postalCode) {
		this.postalCode = postalCode;
		return this;
	}
	
	public GeoLocationCsvBuilder dynamicColumn(List<String> dynamicColumn) {
		this.dynamicColumn = dynamicColumn;
		return this;
	}
	
	public GeoLocationCsvBuilder resultColumn(List<String> resultColumn) {
		this.resultColumn = resultColumn;
		return this;
	}
	
	public GeoLocationCsvBuilder header(boolean header) {
		this.header = header;
		return this;
	}
	
	public GeoLocationCsv build() {
		GeoLocationCsv customer = new GeoLocationCsv( id, viaType, viaName, number, postalCode, header );
		customer.setResultColumn( resultColumn );
		if (!header) {
			customer.setLatitude( latitude );
			customer.setLongitude( longitude );
		}
		customer.addDynamicColumns( dynamicColumn );
		return customer;
	}
}