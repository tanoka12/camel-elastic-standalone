package com.ingdirect.es.f2e.geolocation.bean.geo;

import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.Link;

@SuppressWarnings("serial")
@Link
public class GeoPointCsv {
	
	@DataField(pos = 1, required = true)
	private String lon;
	@DataField(pos = 2, required = true)
	private String lat;
	
	public String getLon() {
		return lon;
	}
	
	public void setLon(String lon) {
		this.lon = lon;
	}
	
	public String getLat() {
		return lat;
	}
	
	public void setLat(String lat) {
		this.lat = lat;
	}
	
	@Override
	public String toString() {
		return "GeoPointCsv [lon=" + lon + ", lat=" + lat + "]";
	}
	
}
