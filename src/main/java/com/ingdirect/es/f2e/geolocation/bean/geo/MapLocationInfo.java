package com.ingdirect.es.f2e.geolocation.bean.geo;

public class MapLocationInfo implements Comparable<MapLocationInfo> {
	
	private final int position;
	
	private final double distance;
	
	private final String category;
	
	private final String imageId;
	
	private final String fileDirectory;
	
	private final int offsetX;
	
	private final int offsetY;
	
	public MapLocationInfo(int position, double distance, String category, String imageId, String fileDirectory,
			int offsetX, int offsetY) {
		super();
		this.position = position;
		this.distance = distance;
		this.category = category;
		this.imageId = imageId;
		this.fileDirectory = fileDirectory;
		this.offsetX = offsetX;
		this.offsetY = offsetY;
	}
	
	@Override
	public int compareTo(MapLocationInfo o) {
		
		if (distance < o.distance) {
			return -1;
		}
		
		if (distance > o.distance) {
			return 1;
		}
		
		return 0;
	}
	
	public int getPosition() {
		return position;
	}
	
	public double getDistance() {
		return distance;
	}
	
	public String getCategory() {
		return category;
	}
	
	public String getImageId() {
		return imageId;
	}
	
	public String getFileDirectory() {
		return fileDirectory;
	}
	
	public int getOffsetX() {
		return offsetX;
	}
	
	public int getOffsetY() {
		return offsetY;
	}
	
}