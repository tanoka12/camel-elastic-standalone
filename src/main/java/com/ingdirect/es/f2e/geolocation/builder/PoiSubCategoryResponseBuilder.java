package com.ingdirect.es.f2e.geolocation.builder;

import java.util.Map;

import com.ingdirect.es.f2e.geolocation.bean.geo.PoiSubCategoryResponse;
import com.ingdirect.es.f2e.geolocation.ctes.PoiConstant;

public class PoiSubCategoryResponseBuilder {
	
	private String id;
	private String longitude;
	private String latitude;
	private String relevantInfo;
	private String subcategory;
	private String index;
	
	public PoiSubCategoryResponseBuilder() {
		
	}
	
	public PoiSubCategoryResponseBuilder(String index, Map<String, Object> source) {
		
		fillIndex( index );
		fillFromSource( source );
	}
	
	private void fillFromSource(Map<String, Object> source) {
		this.subcategory = source.get( "subcategory" ).toString();
		Map<String, String> location = (Map<String, String>) source.get( "location" );
		this.longitude = location.get( "lon" );
		this.latitude = location.get( "lat" );
		
		if (PoiConstant.POI_OSM.equals( index )) {
			this.relevantInfo = extractRelevantInfoFromOsm( source );
		} else if (PoiConstant.POI_TWYP.equals( index )) {
			this.relevantInfo = extractRelevantInfoFromTwyp( source );
		} else if (PoiConstant.POI_ATM.equals( index )) {
			this.relevantInfo = extractRelevantInfoFromAtm( source );
		}
	}
	
	private String extractRelevantInfoFromTwyp(Map<String, Object> source) {
		StringBuilder sb = new StringBuilder();
		sb.append( source.get( "via_name" ) ).append( " " ).append( source.get( "postalCode" ) );
		return sb.toString();
	}
	
	private String extractRelevantInfoFromOsm(Map<String, Object> source) {
		return (String) source.get( "name" );
	}
	
	private String extractRelevantInfoFromAtm(Map<String, Object> source) {
		StringBuilder sb = new StringBuilder();
		sb.append( source.get( "viaType" ) ).append( " " )
				.append( source.get( "viaName" ) ).append( " " )
				.append( source.get( "number" ) ).append( " " )
				.append( source.get( "postalCode" ) );
		return sb.toString();
	}
	
	private void fillIndex(String index) {
		if (index.contains( PoiConstant.INDEX_POI_ATM )) {
			this.index = PoiConstant.POI_ATM;
		} else if (index.contains( PoiConstant.INDEX_POI_OSM )) {
			this.index = PoiConstant.POI_OSM;
		} else if (index.contains( PoiConstant.INDEX_POI_TWYP )) {
			this.index = PoiConstant.POI_TWYP;
		}
	}
	
	public PoiSubCategoryResponseBuilder setId(String id) {
		this.id = id;
		return this;
	}
	
	public PoiSubCategoryResponseBuilder setLongitude(String longitude) {
		this.longitude = longitude;
		return this;
	}
	
	public PoiSubCategoryResponseBuilder setLatitude(String latitude) {
		this.latitude = latitude;
		return this;
	}
	
	public PoiSubCategoryResponseBuilder setRelevantInfo(String relevantInfo) {
		this.relevantInfo = relevantInfo;
		return this;
	}
	
	public PoiSubCategoryResponseBuilder setSubcategory(String subcategory) {
		this.subcategory = subcategory;
		return this;
	}
	
	public PoiSubCategoryResponseBuilder setIndex(String index) {
		this.index = index;
		return this;
	}
	
	public PoiSubCategoryResponse build() {
		return new PoiSubCategoryResponse( id, longitude, latitude, relevantInfo, subcategory, index );
	}
}