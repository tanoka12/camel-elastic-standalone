package com.ingdirect.es.f2e.geolocation.bean.geo;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.Link;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("serial")
@CsvRecord(separator = ",")
public class GeoCsv {
	
	@Link
	private GeoPointCsv location;
	
	@DataField(pos = 3, required = false)
	private String number;
	
	@DataField(pos = 4, required = true)
	private String street;
	
	@DataField(pos = 5, required = false)
	private String unit;
	
	@DataField(pos = 6, required = false)
	private String town;
	
	@DataField(pos = 7, required = false)
	private String district;
	
	@DataField(pos = 8, required = false)
	private String province;
	
	@DataField(pos = 9, required = true)
	private String postalCode;
	
	@DataField(pos = 10, required = false)
	private String id;
	
	@DataField(pos = 11, required = false)
	private String hash;
	
	public String getNumber() {
		return number;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	@JsonProperty("via_name")
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	@JsonIgnore
	public String getUnit() {
		return unit;
	}
	
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public String getTown() {
		return town;
	}
	
	public void setTown(String town) {
		this.town = town;
	}
	
	@JsonProperty("state")
	public String getDistrict() {
		return district;
	}
	
	public void setDistrict(String district) {
		this.district = district;
	}
	
	public String getProvince() {
		return province;
	}
	
	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getPostalCode() {
		return postalCode;
	}
	
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	@JsonIgnore
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	@JsonIgnore
	public String getHash() {
		return hash;
	}
	
	public void setHash(String hash) {
		this.hash = hash;
	}
	
	public GeoPointCsv getLocation() {
		return location;
	}
	
	public void setLocation(GeoPointCsv location) {
		this.location = location;
	}
	
}
