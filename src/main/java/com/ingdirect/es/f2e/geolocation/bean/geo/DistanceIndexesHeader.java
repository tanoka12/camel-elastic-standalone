package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;

/**
 * Created by EH80OB on 25/04/2017.
 */
public class DistanceIndexesHeader implements Serializable{

    private IndexHeaderLocation source;

    private IndexHeaderLocation target;


    public DistanceIndexesHeader(IndexHeaderLocation source, IndexHeaderLocation target) {
        this.source = source;
        this.target = target;
    }

    public DistanceIndexesHeader() {
        this.source = new IndexHeaderLocation(0,1,2,3);
        this.target =  new IndexHeaderLocation(4,5,6,7);
    }

    public int getIdSource(){
        return source.getId();
    }

    public int getIdTarget(){
        return target.getId();
    }

    public void setSource(IndexHeaderLocation source) {
        this.source = source;
    }

    public void setTarget(IndexHeaderLocation target) {
        this.target = target;
    }

    public int getLongitudeSource(){
        return source.getLongitude();
    }

    public int getLongitudeTarget(){
        return target.getLongitude();
    }

    public int getLatitudeSource(){
        return source.getLatitude();
    }

    public int getLatitudeTarget(){
        return target.getLatitude();
    }

    public int getOtherFieldsSource(){
        return source.getOthersField();
    }

    public int getOtherFieldsTarget(){
        return target.getOthersField();
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DistanceIndexesHeader{");
        sb.append("source=").append(source);
        sb.append(", target=").append(target);
        sb.append('}');
        return sb.toString();
    }

}
