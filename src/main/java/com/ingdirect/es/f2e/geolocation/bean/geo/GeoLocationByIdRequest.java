package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;

@SuppressWarnings("serial")
public class GeoLocationByIdRequest implements Serializable, ESRequest {
	
	private final String id;
	
	private final GeoPoint location;
	
	public GeoLocationByIdRequest(String id, GeoPoint location) {
		super();
		this.id = id;
		this.location = location;
	}
	
	public String getId() {
		return id;
	}
	
	public GeoPoint getLocation() {
		return location;
	}
	
	@Override
	public String getIndexId() {
		return getId();
	}
	
	@Override
	public String toString() {
		return "GeoLocationByIdRequest [id=" + id + "]";
	}
}
