package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GeoLocationCsv implements GeoLocation {
	
	private static final String LATITUD_HEADER_VALUE = "LATITUD";
	
	private static final String LONGITUD_HEADER_VALUE = "LONGITUD";
	
	private final String id;
	
	private final String viaType;
	
	private final String viaName;
	
	private final String number;
	
	private final String postalCode;
	
	private final List<String> dynamicColumn;
	
	private List<String> resultColumn;
	
	private final boolean header;
	
	private String longitude;
	
	private String latitude;
	
	public GeoLocationCsv(String id, String viaType, String viaName, String number, String postalCode, boolean header) {
		super();
		this.id = id;
		this.viaType = viaType;
		this.viaName = viaName;
		this.number = number;
		this.postalCode = postalCode;
		this.header = header;
		this.dynamicColumn = new ArrayList<String>();
		this.resultColumn = new ArrayList<String>();
		
		if (header) {
			this.longitude = LONGITUD_HEADER_VALUE;
			this.latitude = LATITUD_HEADER_VALUE;
		}
		
	}
	
	public String getId() {
		return id;
	}
	
	@Override
	public String getViaType() {
		return viaType;
	}
	
	@Override
	public String getViaName() {
		return viaName;
	}
	
	@Override
	public String getNumber() {
		return number;
	}
	
	@Override
	public String getPostalCode() {
		return postalCode;
	}
	
	public List<String> getDynamicColumn() {
		return dynamicColumn;
	}
	
	public void addDynamicColumn(String column) {
		dynamicColumn.add( column );
	}
	
	public void addDynamicColumns(List<String> columns) {
		dynamicColumn.addAll( columns );
		
	}
	
	public boolean isHeader() {
		return header;
	}
	
	@Override
	public String toString() {
		return "CustomerCsv [id=" + id + ", viaType=" + viaType + ", viaName=" + viaName + ", number=" + number
				+ ", postalCode=" + postalCode + ", dynamicColumn=" + dynamicColumn + ", header=" + header + "]";
	}
	
	public String toFileRegister() {
		
		List<String> listValues = new ArrayList<String>();
		
		listValues.add( id );
		listValues.add( longitude );
		listValues.add( latitude );
		listValues.add( viaType );
		listValues.add( viaName );
		listValues.add( number );
		listValues.add( postalCode );
		listValues.addAll( dynamicColumn );
		listValues.addAll( resultColumn );
		
		String register = listValues.stream().map( item -> {
			return ( item == null ? "" : item );
		} ).collect( Collectors.joining( "," ) );
		
		return register;
		
	}
	
	public List<String> getResultColumn() {
		return resultColumn;
	}
	
	public void addResultColumn(String resultColumn) {
		this.resultColumn.add( resultColumn );
	}
	
	public void setResultColumn(List<String> resultColumn) {
		this.resultColumn = resultColumn;
	}
	
	public String getLongitude() {
		return longitude;
	}
	
	public String getLatitude() {
		return latitude;
	}
	
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
	@Override
	public String getTown() {
		return null;
	}
}
