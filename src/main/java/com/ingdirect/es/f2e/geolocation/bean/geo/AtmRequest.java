package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("serial")
public class AtmRequest implements Serializable, ESRequest {
	
	private final GeoPoint location;
	
	private final String id;
	
	private final String red4B;
	
	private final String red6000;
	
	private final String servired;
	
	private final String entityCode;
	
	private final String entity;
	
	private final String branchCode;
	
	private final String branch;
	
	private final String atmCode;
	
	private final String ordinal;
	
	private final String province;
	
	private final String state;
	
	private final String postalCode;
	
	private final String town;
	
	private final String viaType;
	
	private final String viaName;
	
	private final String number;
	
	private final String rdo;
	
	private final int success;
	
	private final String source;
	
	public AtmRequest(GeoPoint location, String id, String red4b, String red6000, String servired, String entityCode,
			String entity, String branchCode, String branch, String atmCode, String ordinal, String province,
			String state, String postalCode, String town, String viaType, String viaName, String number, String rdo,
			int success, String source) {
		
		super();
		this.location = location;
		this.id = id;
		this.red4B = red4b;
		this.red6000 = red6000;
		this.servired = servired;
		this.entityCode = entityCode;
		this.entity = entity;
		this.branchCode = branchCode;
		this.branch = branch;
		this.atmCode = atmCode;
		this.ordinal = ordinal;
		this.province = province;
		this.state = state;
		this.postalCode = postalCode;
		this.town = town;
		this.viaType = viaType;
		this.viaName = viaName;
		this.number = number;
		this.rdo = rdo;
		this.success = success;
		this.source = source;
	}
	
	public GeoPoint getLocation() {
		return location;
	}
	
	public String getId() {
		return id;
	}
	
	public String getRed4B() {
		return red4B;
	}
	
	public String getRed6000() {
		return red6000;
	}
	
	public String getServired() {
		return servired;
	}
	
	public String getEntityCode() {
		return entityCode;
	}
	
	@JsonProperty("subcategory")
	public String getEntity() {
		return entity;
	}
	
	public String getBranchCode() {
		return branchCode;
	}
	
	public String getBranch() {
		return branch;
	}
	
	public String getAtmCode() {
		return atmCode;
	}
	
	public String getOrdinal() {
		return ordinal;
	}
	
	public String getProvince() {
		return province;
	}
	
	public String getState() {
		return state;
	}
	
	public String getPostalCode() {
		return postalCode;
	}
	
	public String getTown() {
		return town;
	}
	
	public String getViaType() {
		return viaType;
	}
	
	public String getViaName() {
		return viaName;
	}
	
	public String getNumber() {
		return number;
	}
	
	public String getRdo() {
		return rdo;
	}
	
	public int getSuccess() {
		return success;
	}
	
	public String getSource() {
		return source;
	}
	
	@Override
	@JsonIgnore
	public String getIndexId() {
		return getId();
	}
	
	@Override
	public String toString() {
		return "AtmRequest [location=" + location + ", id=" + id + ", red4B=" + red4B + ", red6000=" + red6000
				+ ", servired=" + servired + ", entityCode=" + entityCode + ", entity=" + entity + ", branchCode="
				+ branchCode + ", branch=" + branch + ", atmCode=" + atmCode + ", ordinal=" + ordinal + ", province="
				+ province + ", state=" + state + ", postalCode=" + postalCode + ", town=" + town + ", viaType="
				+ viaType + ", viaName=" + viaName + ", number=" + number + ", rdo=" + rdo + ", success=" + success
				+ ", source=" + source + "]";
	}
	
}
