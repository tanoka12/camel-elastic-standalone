package com.ingdirect.es.f2e.geolocation.builder;

import org.apache.commons.lang3.StringUtils;

import com.ingdirect.es.f2e.geolocation.bean.geo.GeoLocationCsv;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoCsv;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoJson;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoLocation;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoPoint;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoRequest;

public class GeoRequestBuilder {
	
	private String lon;
	
	private String lat;
	
	private String number;
	
	private String town;
	
	private String province;
	
	private String postalCode;
	
	private String viaName;
	
	private String viaType;
	
	private String state;
	
	public GeoRequestBuilder() {
		
	}
	
	public GeoRequestBuilder(GeoLocation location) {
		this.viaName = location.getViaName();
		this.viaType = location.getViaType();
		this.number = location.getNumber();
		this.postalCode = location.getPostalCode();
	}
	
	public GeoRequestBuilder(GeoCsv geoCsv) {
		
		if (geoCsv.getLocation() != null) {
			this.lon = geoCsv.getLocation().getLon();
			this.lat = geoCsv.getLocation().getLat();
		}
		
		this.number = geoCsv.getNumber();
		this.postalCode = geoCsv.getPostalCode();
		
		fillVia( geoCsv.getStreet() );
		
	}

	public GeoRequestBuilder(GeoLocationCsv customer) {

		this.number = customer.getNumber();
		this.postalCode = customer.getPostalCode();
		this.viaName = customer.getViaName();
		this.viaType = customer.getViaType();

	}

	public GeoRequestBuilder(GeoJson geoJson) {

		if (geoJson.getGeo() != null) {
			this.lon = geoJson.getGeo().getLongitude();
			this.lat = geoJson.getGeo().getLatitude();
		}

		this.number = geoJson.getNumber();
		this.postalCode = geoJson.getCodPostal();
		this.viaName = geoJson.getViaName();
		this.viaType = geoJson.getViaType();

	}
	
	private void fillVia(String via) {
		
		if (StringUtils.isNotBlank( via ) && via.length() >= 3) {
			this.viaType = via.substring( 0, 2 );
			this.viaName = via.substring( 3 );
		} else {
			this.viaName = via;
			this.viaType = "";
		}
		
	}
	

	
	public GeoRequest build() {
		
		GeoPoint location = new GeoPoint( lon, lat );
		
		GeoRequest geoRequest = new GeoRequest( location, number, town, province, postalCode, viaName, viaType, state );
		return geoRequest;
	}
	
	public GeoRequestBuilder latitude(String lat) {
		this.lat = lat;
		return this;
	}
	
	public GeoRequestBuilder longitude(String lon) {
		this.lon = lon;
		return this;
	}
	
	public GeoRequestBuilder province(String province) {
		this.province = province;
		return this;
	}
	
	public GeoRequestBuilder number(String number) {
		this.number = number;
		return this;
	}
	
	public GeoRequestBuilder postalcode(String postalcode) {
		this.postalCode = postalcode;
		return this;
	}
	
	public GeoRequestBuilder viaName(String viaName) {
		this.viaName = viaName;
		return this;
	}
	
	public GeoRequestBuilder viaType(String viaType) {
		this.viaType = viaType;
		return this;
	}
	
	public GeoRequestBuilder state(String state) {
		this.state = state;
		return this;
	}
	
	public GeoRequestBuilder town(String town) {
		this.town = town;
		return this;
	}
	
}
