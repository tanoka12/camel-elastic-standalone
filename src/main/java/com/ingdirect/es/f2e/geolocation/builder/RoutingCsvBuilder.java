package com.ingdirect.es.f2e.geolocation.builder;

import java.util.Map;

import com.ingdirect.es.f2e.geolocation.bean.geo.GeoPoint;
import com.ingdirect.es.f2e.geolocation.bean.geo.RoutingCsv;

public class RoutingCsvBuilder {
	
	private String id;
	private String latitude;
	private String longitude;
	private String otherFields;
	private boolean header;
	private boolean error;
	private String distance;
	
	public RoutingCsvBuilder() {
		this.header = false;
		this.error = false;
	}
	
	public RoutingCsvBuilder(Map<String, Object> resultMap) {
		
		this.id = (String) resultMap.get( "id" );
		this.otherFields = (String) resultMap.get( "otherFields" );
		this.header = (Boolean) resultMap.get( "header" );
		this.error = (Boolean) resultMap.get( "error" );
		
		@SuppressWarnings("unchecked")
		Map<String, Object> location = (Map<String, Object>) resultMap.get( "location" );
		
		this.latitude = (String) location.get( "lat" );
		this.longitude = (String) location.get( "lon" );
		
	}
	
	public RoutingCsvBuilder id(String id) {
		this.id = id;
		return this;
	}
	
	public RoutingCsvBuilder latitude(String latitude) {
		this.latitude = latitude;
		return this;
	}
	
	public RoutingCsvBuilder longitude(String longitude) {
		this.longitude = longitude;
		return this;
	}
	
	public RoutingCsvBuilder otherFields(String otherFields) {
		this.otherFields = otherFields;
		return this;
	}
	
	public RoutingCsvBuilder header(boolean header) {
		this.header = header;
		return this;
	}
	
	public RoutingCsvBuilder error(boolean error) {
		this.error = error;
		return this;
	}
	
	public RoutingCsvBuilder distance(String distance) {
		this.distance = distance;
		return this;
	}
	
	public RoutingCsv build() {
		GeoPoint location = new GeoPoint( longitude, latitude );
		RoutingCsv routingCsv = new RoutingCsv( id, location, otherFields, header, error );
		routingCsv.setDistance( distance );
		return routingCsv;
	}
	
}
