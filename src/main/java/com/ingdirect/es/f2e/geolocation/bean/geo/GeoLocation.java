package com.ingdirect.es.f2e.geolocation.bean.geo;

public interface GeoLocation {
	
	String getViaName();
	
	String getNumber();
	
	String getPostalCode();
	
	String getViaType();
	
	String getTown();
	
}
