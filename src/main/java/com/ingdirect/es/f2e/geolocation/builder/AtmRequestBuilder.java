package com.ingdirect.es.f2e.geolocation.builder;

import com.ingdirect.es.f2e.geolocation.bean.geo.AtmCsv;
import com.ingdirect.es.f2e.geolocation.bean.geo.AtmRequest;
import com.ingdirect.es.f2e.geolocation.bean.geo.AtmRequestBuilder2;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoPoint;

public class AtmRequestBuilder {
	
	private String lon;
	
	private String lat;
	
	private String id;
	
	private String red4B;
	
	private String red6000;
	
	private String servired;
	
	private String entityCode;
	
	private String entity;
	
	private String branchCode;
	
	private String branch;
	
	private String atmCode;
	
	private String ordinal;
	
	private String province;
	
	private String state;
	
	private String postalCode;
	
	private String town;
	
	private String viaType;
	
	private String viaName;
	
	private String number;
	
	private String rdo;
	
	private int success;
	
	private String source;
	
	public AtmRequestBuilder() {
		
	}
	
	public AtmRequestBuilder(AtmCsv atmCsv) {
		
		this.id = atmCsv.getId();
		this.red4B = atmCsv.getRed4B();
		this.red6000 = atmCsv.getRed6000();
		this.servired = atmCsv.getServired();
		this.entityCode = atmCsv.getEntityCode();
		this.entity = atmCsv.getEntity();
		this.branchCode = atmCsv.getBranchCode();
		this.branch = atmCsv.getBranch();
		this.atmCode = atmCsv.getAtmCode();
		this.ordinal = atmCsv.getOrdinal();
		this.province = atmCsv.getProvince();
		this.state = atmCsv.getState();
		this.postalCode = atmCsv.getPostalCode();
		this.town = atmCsv.getTown();
		this.viaType = atmCsv.getViaType();
		this.viaName = atmCsv.getViaName();
		this.number = atmCsv.getNumber();
		this.rdo = atmCsv.getRdo();
	}
	
	public AtmRequestBuilder latitude(String lat) {
		this.lat = lat;
		return this;
	}
	
	public AtmRequestBuilder longitude(String lon) {
		this.lon = lon;
		return this;
	}
	
	public AtmRequestBuilder success(int success) {
		this.success = success;
		return this;
	}
	
	public AtmRequestBuilder source(String source) {
		this.source = source;
		return this;
	}
	
	public AtmRequest build() {
		
		GeoPoint location = new GeoPoint( lon, lat );
		
		AtmRequest atmRequest = new AtmRequestBuilder2().setLocation(location).setId(id).setRed4b(red4B).setRed6000(red6000).setServired(servired).setEntityCode(entityCode).setEntity(entity).setBranchCode(branchCode).setBranch(branch).setAtmCode(atmCode).setOrdinal(ordinal).setProvince(province).setState(state).setPostalCode(postalCode).setTown(town).setViaType(viaType).setViaName(viaName).setNumber(number).setRdo(rdo).setSuccess(success).setSource(source).createAtmRequest();
		
		return atmRequest;
	}
}
