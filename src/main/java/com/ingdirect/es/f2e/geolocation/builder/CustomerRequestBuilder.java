package com.ingdirect.es.f2e.geolocation.builder;

import com.ingdirect.es.f2e.geolocation.bean.geo.CustomerCsv;
import com.ingdirect.es.f2e.geolocation.bean.geo.CustomerRequest;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoPoint;

public class CustomerRequestBuilder {
	
	private String id;
	
	private String name;
	
	private String surName1;
	
	private String surName2;
	
	private String viaType;
	
	private String viaName;
	
	private String number;
	
	private String floor;
	
	private String additionalInfo;
	
	private String town;
	
	private String province;
	
	private String country;
	
	private String postalCode;
	
	private String lon;
	
	private String lat;
	
	private int success;
	
	private String source;
	
	public CustomerRequestBuilder() {
		
	}
	
	public CustomerRequestBuilder(CustomerCsv customer) {
		
		this.id = customer.getId();
		
		this.name = customer.getName();
		
		this.surName1 = customer.getSurName1();
		
		this.surName2 = customer.getSurName2();
		
		this.viaType = customer.getViaType();
		
		this.viaName = customer.getViaName();
		
		this.number = customer.getNumber();
		
		this.floor = customer.getFloor();
		
		this.additionalInfo = customer.getAdditionalInfo();
		
		this.town = customer.getTown();
		
		this.province = customer.getProvince();
		
		this.country = customer.getCountry();
		
		this.postalCode = customer.getPostalCode();
		
	}
	
	public CustomerRequestBuilder id(String id) {
		this.id = id;
		return this;
	}
	
	public CustomerRequestBuilder name(String name) {
		this.name = name;
		return this;
	}
	
	public CustomerRequestBuilder surName1(String surName1) {
		this.surName1 = surName1;
		return this;
	}
	
	public CustomerRequestBuilder surName2(String surName2) {
		this.surName2 = surName2;
		return this;
	}
	
	public CustomerRequestBuilder viaType(String viaType) {
		this.viaType = viaType;
		return this;
	}
	
	public CustomerRequestBuilder viaName(String viaName) {
		this.viaName = viaName;
		return this;
	}
	
	public CustomerRequestBuilder number(String number) {
		this.number = number;
		return this;
	}
	
	public CustomerRequestBuilder town(String town) {
		this.town = town;
		return this;
	}
	
	public CustomerRequestBuilder province(String province) {
		this.province = province;
		return this;
	}
	
	public CustomerRequestBuilder postalCode(String postalCode) {
		this.postalCode = postalCode;
		return this;
	}
	
	public CustomerRequestBuilder latitude(String latitude) {
		this.lat = latitude;
		return this;
	}
	
	public CustomerRequestBuilder longitude(String longitude) {
		this.lon = longitude;
		return this;
	}
	
	public CustomerRequestBuilder success(int success) {
		this.success = success;
		return this;
	}
	
	public CustomerRequestBuilder source(String source) {
		this.source = source;
		return this;
	}
	
	public CustomerRequest build() {
		
		GeoPoint location = new GeoPoint( lon, lat );
		
		CustomerRequest customerRequest = new CustomerRequest( id, name, surName1, surName2, viaType, viaName, number,
				floor, additionalInfo, town, province, country, postalCode, success, source, location );
		return customerRequest;
	}
}
