package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;
import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Eh80OB on 13/06/2017.
 */

public class GeometryResponse implements Serializable {
	
	private CoordinateResponse[] coordinates;
	
	public CoordinateResponse[] getCoordinates() {
		return coordinates;
	}
	
	@JsonProperty("coordinates")
	@JsonFormat(shape = JsonFormat.Shape.ARRAY)
	public void setCoordinates(CoordinateResponse[] coordinates) {
		this.coordinates = coordinates;
	}
	
	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer( "GeometryResponse{" );
		sb.append( "coordinates=" ).append( coordinates == null ? "null" : Arrays.asList( coordinates ).toString() );
		sb.append( '}' );
		return sb.toString();
	}
}
