package com.ingdirect.es.f2e.geolocation.builder;

import com.ingdirect.es.f2e.geolocation.bean.geo.GeoPoint;
import com.ingdirect.es.f2e.geolocation.bean.geo.PointInterestCsv;
import com.ingdirect.es.f2e.geolocation.bean.geo.PointInterestRequest;

public class PointInterestRequestBuilder {
	
	private final String id;
	
	private final String uid;
	
	private final String version;
	
	private final String changeset;
	
	private final String timestamp;
	
	private final String category;
	
	private final String subcategory;
	
	private final String name;
	
	private final GeoPoint location;
	
	private final String other;
	
	public PointInterestRequestBuilder(PointInterestCsv pointCsv) {
		
		location = new GeoPoint( pointCsv.getLon(), pointCsv.getLat() );
		
		this.id = pointCsv.getId();
		
		this.uid = pointCsv.getUid();
		
		this.version = pointCsv.getVersion();
		
		this.changeset = pointCsv.getChangeset();
		
		this.timestamp = pointCsv.getTimestamp();
		
		this.category = pointCsv.getCategory();
		
		this.subcategory = pointCsv.getSubcategory();
		
		this.name = pointCsv.getName();
		
		this.other = pointCsv.getOther();
		
	}
	
	public PointInterestRequest build() {
		PointInterestRequest pointInterestRequest = new PointInterestRequest( id, uid, version, changeset, timestamp,
				category, subcategory, name, location, other );
		return pointInterestRequest;
	}
}
