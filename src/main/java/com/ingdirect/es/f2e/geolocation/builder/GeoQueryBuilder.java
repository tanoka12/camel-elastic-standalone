package com.ingdirect.es.f2e.geolocation.builder;

import java.util.StringJoiner;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;

import com.ingdirect.es.f2e.geolocation.bean.geo.GeoRequest;

public class GeoQueryBuilder {
	
	private static final String FUZZINESS = "2";
	private static final String EXACT_FUZZINESS = "0";
	private static final String ALL_FIELD = "geo_all";
	
	public GeoQueryBuilder() {
		
	}
	
	public MatchQueryBuilder querySearchByLocation(GeoRequest location, boolean exactQuery) {
		
		MatchQueryBuilder builder = QueryBuilders.matchQuery( ALL_FIELD, buildQueryString( location ) )
				.operator( Operator.AND )
				.fuzziness( ( exactQuery ? EXACT_FUZZINESS : FUZZINESS ) );
		
		return builder;
		
	}
	
	public BoolQueryBuilder boolQuerySearchByLocation(GeoRequest location, boolean exactQuery) {

		String fuzzy = (exactQuery ? EXACT_FUZZINESS : FUZZINESS );
		
		BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
		if (StringUtils.isNotEmpty( location.getNumber() )) {
			boolQuery.must( QueryBuilders.matchQuery( "number", location.getNumber() ).fuzziness(fuzzy) );
		}
		
		if (StringUtils.isNotEmpty( location.getPostalCode() )) {
			boolQuery.must( QueryBuilders.matchQuery( "postalCode", location.getPostalCode() ).fuzziness(fuzzy) );
		}
		
		if (StringUtils.isNotEmpty( location.getViaName() )) {
			boolQuery.must( QueryBuilders.matchQuery( "via_name", location.getViaName() ).fuzziness(fuzzy).minimumShouldMatch("75%") );
		}
		
		if (StringUtils.isNotEmpty( location.getViaType() )) {
			boolQuery.must( QueryBuilders.matchQuery( "via_type", location.getViaType() ).fuzziness(fuzzy) );
		}
		
		return boolQuery;
	}
	
	private String buildQueryString(GeoRequest location) {
		StringJoiner queryString = new StringJoiner( ", " );
		
		if (StringUtils.isNotEmpty( location.getViaType() )) {
			queryString.add( location.getViaType() );
		}
		
		if (StringUtils.isNotEmpty( location.getViaName() )) {
			queryString.add( location.getViaName() );
		}
		
		if (StringUtils.isNotEmpty( location.getNumber() )) {
			queryString.add( location.getNumber() );
		}
		
		if (StringUtils.isNotEmpty( location.getPostalCode() )) {
			queryString.add( location.getPostalCode() );
		}
		
		if (StringUtils.isNotEmpty( location.getTown() )) {
			queryString.add( location.getTown() );
		}
		
		return queryString.toString();
	}
	
}
