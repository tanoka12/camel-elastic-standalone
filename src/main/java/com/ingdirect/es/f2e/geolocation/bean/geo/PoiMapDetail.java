package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by EH80OB on 22/05/2017.
 */
public class PoiMapDetail implements Serializable, Routing {

    private final GeoPoint location;

    private  String distance;

    private final CategoryInfo category;

    private final String relevantInfo;

    private CoordinateResponse[] coordinates;


    public PoiMapDetail(GeoPoint location, String distance, CategoryInfo category, String relevantInfo) {
        this.location = location;
        this.distance = distance;
        this.category = category;
        this.relevantInfo = relevantInfo;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public String getDistance() {
        return distance;
    }

    public CategoryInfo getCategory() {
        return category;
    }
    public String getRelevantInfo() {
        return relevantInfo;
    }
    public void setDistance(String distance) {
        this.distance = distance;
    }

    public CoordinateResponse[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(CoordinateResponse[] coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PoiMapDetail{");
        sb.append("location=").append(location);
        sb.append(", distance='").append(distance).append('\'');
        sb.append(", category='").append(category).append('\'');
        sb.append(", coordinates='").append(coordinates).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PoiMapDetail that = (PoiMapDetail) o;

        return new EqualsBuilder()
            .append(location, that.location)
            .append(distance, that.distance)
            .append(category, that.category)
            .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(location)
            .append(distance)
            .append(category)
            .toHashCode();
    }
}
