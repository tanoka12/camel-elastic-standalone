package com.ingdirect.es.f2e.geolocation.repository;

import java.util.List;

import com.ingdirect.es.f2e.geolocation.bean.geo.ErrorESResponse;
import com.ingdirect.es.f2e.geolocation.bean.geo.Routing;
import com.ingdirect.es.f2e.geolocation.bean.geo.RoutingCsv;

public interface RoutingDao extends ElasticSearchDao {

    ErrorESResponse insertRoutingHeader(String uuidIndex, RoutingCsv header) throws Exception;

    List<ErrorESResponse> insertRoutingTargets(String uuidIndex, List<RoutingCsv> targets) throws Exception;

    String getRoutingHeader(String uuidIndex) throws Exception;

    List<RoutingCsv> getNearestLocations(String uuidIndex, Routing routingLocation, String buffer) throws Exception;

}
