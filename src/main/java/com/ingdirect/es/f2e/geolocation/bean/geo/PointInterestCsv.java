package com.ingdirect.es.f2e.geolocation.bean.geo;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(separator = ",")
public class PointInterestCsv {
	
	@DataField(pos = 1, required = false)
	private String id;
	
	@DataField(pos = 2, required = false)
	private String uid;
	
	@DataField(pos = 3, required = false)
	private String version;
	
	@DataField(pos = 4, required = false)
	private String changeset;
	
	@DataField(pos = 5, required = false)
	private String timestamp;
	
	@DataField(pos = 6, required = true)
	private String category;
	
	@DataField(pos = 7, required = true)
	private String subcategory;
	
	@DataField(pos = 8, required = false)
	private String name;
	
	@DataField(pos = 9, required = true)
	private String lon;
	
	@DataField(pos = 10, required = true)
	private String lat;
	
	@DataField(pos = 11, required = false)
	private String other;
	
	public String getId() {
		return id;
	}
	
	public String getUid() {
		return uid;
	}
	
	public String getVersion() {
		return version;
	}
	
	public String getChangeset() {
		return changeset;
	}
	
	public String getTimestamp() {
		return timestamp;
	}
	
	public String getCategory() {
		return category;
	}
	
	public String getSubcategory() {
		return subcategory;
	}
	
	public String getLon() {
		return lon;
	}
	
	public String getLat() {
		return lat;
	}
	
	public String getOther() {
		return other;
	}
	
	@Override
	public String toString() {
		return "PointInterestCsv [id=" + id + ", uid=" + uid + ", version=" + version + ", changeset=" + changeset
				+ ", timestamp=" + timestamp + ", category=" + category + ", subcategory=" + subcategory + ", name="
				+ name + ", lon=" + lon + ", lat=" + lat + ", other=" + other + "]";
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}
	
	public void setChangeset(String changeset) {
		this.changeset = changeset;
	}
	
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	public void setSubcategory(String subcategory) {
		this.subcategory = subcategory;
	}
	
	public void setLon(String lon) {
		this.lon = lon;
	}
	
	public void setLat(String lat) {
		this.lat = lat;
	}
	
	public void setOther(String other) {
		this.other = other;
	}
	
}
