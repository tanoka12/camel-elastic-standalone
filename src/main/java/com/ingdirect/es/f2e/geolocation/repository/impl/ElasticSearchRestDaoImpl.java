package com.ingdirect.es.f2e.geolocation.repository.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingdirect.es.f2e.geolocation.bean.geo.ESRequest;
import com.ingdirect.es.f2e.geolocation.bean.geo.ErrorESResponse;
import com.ingdirect.es.f2e.geolocation.client.impl.GeoElasticSearchClientImpl;
import com.ingdirect.es.f2e.geolocation.repository.ElasticSearchDao;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ElasticSearchRestDaoImpl implements ElasticSearchDao {

    protected final GeoElasticSearchClientImpl client;

    private final ObjectMapper mapper;

    public ElasticSearchRestDaoImpl(GeoElasticSearchClientImpl client, ObjectMapper mapper) {
        this.client = client;
        this.mapper = mapper;
    }

    @Override
    public boolean createIndex(String index) throws Exception {
        return client.createIndex(index);
    }

    @Override
    public boolean createIndex(String index, String indexJsonSource) throws Exception {
        return client.createIndex(index, indexJsonSource);
    }

    @Override
    public boolean existIndex(String index) throws Exception {
        return client.existsIndex(index);
    }

    @Override
    public String getRecentIndexByPrefix(String preffixIndex) throws Exception {
        return client.getRecentIndexByPrefix(preffixIndex);
    }

    @Override
    public boolean deleteIndex(String index) throws Exception {
        return client.deleteIndex(index);
    }

    @Override
    public void refreshIndex(String index) throws Exception {
        client.refreshIndex(index);
    }

    @Override
    public GetResponse findObjectById(String index, String mapping, String id) throws Exception {

        GetRequest getRequest = new GetRequest(index, mapping, id);

        return client.get(getRequest);
    }

    @Override
    public List<ErrorESResponse> insertLocations(String index, String mapping, List<? extends ESRequest> locations) throws Exception{
        List<ErrorESResponse> errors = new ArrayList<>();

        if (!locations.isEmpty()) {
            BulkRequest bulkRequest = new BulkRequest();

            locations.forEach(location -> {
                try {
                    bulkRequest.add(new IndexRequest(index, mapping, location.getIndexId())
                            .source( mapper.writeValueAsString( location ), XContentType.JSON ));
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            });
            BulkResponse bulkResponse = client.bulk(bulkRequest);
            if (bulkResponse.hasFailures()) {
                errors = buildErrorsResponse( bulkResponse.getItems() );
            }

        }
        return errors;

    }

    protected List<ErrorESResponse> buildErrorsResponse(BulkItemResponse[] items) {
        List<ErrorESResponse> errors = Arrays.stream( items ).filter( item -> item.isFailed() )
                .map( item -> new ErrorESResponse( item.getId(), item.getFailureMessage() ) )
                .collect( Collectors.toList() );
        return errors;
    }


    @Override
    public <T> List<T> getAllDocuments(String index, Class<T> clazz) throws Exception {

        List<T> result = new ArrayList<>();

        final Scroll scroll = new Scroll(TimeValue.timeValueMinutes(1L));

        final SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder().size(1000);

        SearchRequest searchRequest = new SearchRequest(index).scroll(scroll).source(searchSourceBuilder);

        SearchResponse searchResponse = client.search(searchRequest);

        String scrollId = searchResponse.getScrollId();

        SearchHit[] searchHits = searchResponse.getHits().getHits();

        while (searchHits != null && searchHits.length > 0) {

            final List<T> partialResult = extractResponse(searchResponse, clazz);

            result.addAll(partialResult);

            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId).scroll(scroll);

            searchResponse = client.searchScroll(scrollRequest);

            scrollId = searchResponse.getScrollId();

            searchHits = searchResponse.getHits().getHits();
        }

        return result;
    }

    private <T> List<T> extractResponse(SearchResponse response, Class<T> clazz) {
        return Arrays
                .stream(response.getHits().getHits()).map(searchHitFields -> {
                    try {
                        return mapper.readValue(searchHitFields.getSourceAsString(), clazz);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }).collect(Collectors.toList());
    }

}
