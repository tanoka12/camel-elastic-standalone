package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by Eh80OB on 13/06/2017.
 */
public class CoordinateResponse implements Serializable {
	
	private Double longitude;
	
	private Double latitude;
	
	public Double getLongitude() {
		return longitude;
	}
	
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public Double getLatitude() {
		return latitude;
	}
	
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer( "CoordinateResponse{" );
		sb.append( "longitude=" ).append( longitude );
		sb.append( ", latitude=" ).append( latitude );
		sb.append( '}' );
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		
		if (o == null || getClass() != o.getClass())
			return false;
		
		CoordinateResponse that = (CoordinateResponse) o;
		
		return new EqualsBuilder()
				.append( longitude, that.longitude )
				.append( latitude, that.latitude )
				.isEquals();
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder( 17, 37 )
				.append( longitude )
				.append( latitude )
				.toHashCode();
	}
}
