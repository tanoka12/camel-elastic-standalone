package com.ingdirect.es.f2e.geolocation.bean.geo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({"postalCode", "via_type", "via_name", "number", "location"})
public class GeoJson {

    private String type;

    private String pk;

    private String tramo;

    private String vial;

    private String cp;


    private String codPostal;


    private String number;


    private String viaType;


    private String viaName;


    private GeoPointJson geo;

    @JsonProperty("properties")
    public void nestedProperty(Map<String, String> prop) {

        pk = prop.get("ID_POR_PK");

        tramo = prop.get("ID_TRAMO");

        vial = prop.get("ID_VIAL");

        cp = prop.get("ID_CP");

        codPostal = prop.get("COD_POSTAL");

        number = prop.get("NUM_POR");

        viaType = prop.get("TIP_VIA_IN");

        viaName = prop.get("NOM_VIA");
    }

    @JsonIgnore
    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    @JsonIgnore
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonIgnore
    public String getTramo() {
        return tramo;
    }

    public void setTramo(String tramo) {
        this.tramo = tramo;
    }

    @JsonIgnore
    public String getVial() {
        return vial;
    }

    public void setVial(String vial) {
        this.vial = vial;
    }

    @JsonIgnore
    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    @JsonProperty("postalCode")
    public String getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String num) {
        this.number = num;
    }

    @JsonProperty("via_type")
    public String getViaType() {
        return viaType;
    }

    public void setViaType(String viaType) {
        this.viaType = viaType;
    }

    @JsonProperty(value = "via_name")
    public String getViaName() {
        return viaName;
    }

    public void setViaName(String viaName) {
        this.viaName = viaName;
    }

    @JsonProperty("location")
    public GeoPointJson getGeo() {
        return geo;
    }

    @JsonProperty(value = "geometry", required = true)
    public void setGeo(GeoPointJson geo) {
        this.geo = geo;
    }

    public boolean validGeo() {
        return !(StringUtils.isEmpty(number)
                || StringUtils.isEmpty(codPostal)
                || StringUtils.isEmpty(viaName)
                || StringUtils.isEmpty(viaType)
                || geo == null);
    }

    @Override
    public String toString() {
        return "GeoJson [type=" + type + ", pk=" + pk + ", tramo=" + tramo + ", vial=" + vial + ", cp=" + cp
                + ", codPostal=" + codPostal + ", number=" + number + ", viaType=" + viaType + ", viaName=" + viaName
                + ", geo=" + geo + "]";
    }

}
