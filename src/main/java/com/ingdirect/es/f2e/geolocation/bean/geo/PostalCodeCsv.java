package com.ingdirect.es.f2e.geolocation.bean.geo;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(separator = ";", autospanLine = true)
public class PostalCodeCsv {
	
	@DataField(pos = 1, required = true)
	private String postalCode;
	
	@DataField(pos = 2, required = true)
	private String town;
	
	@DataField(pos = 3, required = true)
	private String province;
	
	@DataField(pos = 4, required = true)
	private String state;
	
	@DataField(pos = 5, required = false)
	private String rest;
	
	public String getPostalCode() {
		return postalCode;
	}
	
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	public String getTown() {
		return town;
	}
	
	public void setTown(String town) {
		this.town = town;
	}
	
	public String getProvince() {
		return province;
	}
	
	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	@Override
	public String toString() {
		return "PostalCodeCsv [postalCode=" + postalCode + ", town=" + town + ", province=" + province + ", state="
				+ state + "]";
	}
	
	public String getRest() {
		return rest;
	}
	
	public void setRest(String rest) {
		this.rest = rest;
	}
	
}
