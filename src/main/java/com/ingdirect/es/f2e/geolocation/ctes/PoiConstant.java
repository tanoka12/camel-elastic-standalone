package com.ingdirect.es.f2e.geolocation.ctes;

/**
 * Created by EH80OB on 17/04/2017.
 */
public class PoiConstant {

    public static final String INDEX_POI_ATM = "geo-poi-atm-";

    public static final String INDEX_POI_TWYP = "geo-poi-twyp-";

    public static final String INDEX_POI_OSM = "geo-poi-osm-";

    public static final String POI_ATM = "ATM";

    public static final String POI_TWYP = "TWYP";

    public static final String POI_OSM = "OSM";
}
