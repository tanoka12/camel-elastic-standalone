package com.ingdirect.es.f2e.geolocation.bean.geo;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CartoCiudad {
	
	@JsonProperty("features")
	private List<GeoJson> features = new ArrayList<GeoJson>();
	
	/**
	 * 
	 * @return The features
	 */
	@JsonProperty("features")
	public List<GeoJson> getFeatures() {
		return features;
	}
	
	/**
	 * 
	 * @param features
	 *        The features
	 */
	@JsonProperty("features")
	public void setFeatures(List<GeoJson> features) {
		this.features = features;
	}
}
