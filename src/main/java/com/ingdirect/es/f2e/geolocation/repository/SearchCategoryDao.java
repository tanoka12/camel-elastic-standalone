package com.ingdirect.es.f2e.geolocation.repository;

import com.ingdirect.es.f2e.geolocation.bean.geo.CategoryRequest;
import com.ingdirect.es.f2e.geolocation.bean.geo.CategoryResponse;
import com.ingdirect.es.f2e.geolocation.bean.geo.MapQueryRequest;
import com.ingdirect.es.f2e.geolocation.bean.geo.PoiSubCategoryResponse;

import java.util.List;

/**
 * Created by EH80OB on 18/04/2017.
 */
public interface SearchCategoryDao {

    List<PoiSubCategoryResponse> getPoiByCategory(CategoryRequest request) throws Exception;

    List<CategoryResponse> getAllCategories() throws Exception;

    List<PoiSubCategoryResponse> getPoiByCategoryAndBuffer(MapQueryRequest queryRequest) throws Exception;
}
