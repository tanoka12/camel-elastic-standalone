package org.apache.camel.example.spring.javaconfig;

import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.processor.aggregate.AbstractListAggregationStrategy;


public class OpenStreetMapAggregationStrategy extends AbstractListAggregationStrategy<GeoCsv> {

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        if (oldExchange == null) {
            oldExchange = new DefaultExchange(newExchange);
        }



        boolean splitCompleteProperty = (boolean) newExchange.getProperty(Exchange.SPLIT_COMPLETE);

        if (splitCompleteProperty) {
            oldExchange.setProperty(Exchange.AGGREGATION_COMPLETE_CURRENT_GROUP, true);
        }

        return super.aggregate(oldExchange, newExchange);
    }

    @Override
    public GeoCsv getValue(Exchange exchange) {
        GeoCsv value = (GeoCsv) exchange.getIn().getBody();
        return value;
    }

}
