package org.apache.camel.example.spring.javaconfig;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.zipfile.ZipFileDataFormat;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.stream.Stream;

@Component
public class CSVRoute extends RouteBuilder {



    @Override
    public void configure() throws Exception {


        from("file:target/reportsZip?recursive=true&move=../processed/copy-of-${file:name}").process(new Processor() {
            @Override
            public void process(Exchange exchange) throws Exception {
                String relativePath = exchange.getIn().getHeader("CamelFileParent", String.class);
                String parentPath =  Stream.of(relativePath.split("\\/")).reduce((a, b) -> b)
                        .orElse("false");
                exchange.getIn().setHeader("elastic_index", parentPath);
            }
        }).log("Processing ${header.elastic_index}").end();
    }

}
