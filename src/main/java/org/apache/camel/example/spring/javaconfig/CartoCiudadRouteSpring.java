package org.apache.camel.example.spring.javaconfig;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.file.FileComponent;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.spi.DataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CartoCiudadRouteSpring extends RouteBuilder {

    public static final String SETTINGS_ROUTE_ID = "cartoCiudadRote";

    public static final String SETTINGS_ERROR_ROUTE_ID = "errorsCartoCiudadRote";
    public static final String SETTINGS_ROUTE_ID_KK = "kkRoute";

    private final String directory;
    private final String intermediateDirectory;
    private final String finalDirectory;
    private final String charsetFile;

    private final int splitGroupNumber;
    private final String params;

    @Autowired
    private CartoCiudadProcessorSpring processor;

    public CartoCiudadRouteSpring() {
        charsetFile = "UTF-8";
        directory = "files_to_publish";
        intermediateDirectory = "intermedio";
        finalDirectory = "finish";

        splitGroupNumber = 1;

        //params = "?" + "noop=true&fileName=sss";
        params = "?move=processed/${file:name.noext}-$simple{date:now:yyyyMMdd-HHmmss}.${file:ext}";
    }

    @Override
    public void configure() throws Exception {

        DataFormat bindy = new BindyCsvDataFormat( GeoCsv.class );

        from( "file://" + directory + params ).routeId( SETTINGS_ROUTE_ID ).errorHandler(deadLetterChannel("direct:errors"))
                .split( body().tokenize( "\n", splitGroupNumber, false ) )
                .streaming().unmarshal( bindy ).process( processor ).log("VAMOSSSSSS ${property.CamelSplitComplete}")
                .to( "direct:second" ).end();

        from("direct:second").routeId( SETTINGS_ROUTE_ID_KK ).log(LoggingLevel.INFO, "Vamos cousin ${property.CamelSplitComplete}").
                aggregate(constant(false), new OpenStreetMapAggregationStrategy()).completionSize(2).log("Lista ${body}")
                .to( "stream:out" ).end();

        from("direct:errors").routeId(SETTINGS_ERROR_ROUTE_ID).process(new Processor() {
            @Override
            public void process(Exchange exchange) throws Exception {
                System.out.println(":::::: ERROR ::::::" + exchange.getIn().getBody());
            }
        }).setHeader(Exchange.FILE_NAME, constant("report.txt")).to("file:target/reports");
    }

}
