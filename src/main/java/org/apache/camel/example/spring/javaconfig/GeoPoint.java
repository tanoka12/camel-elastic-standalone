package org.apache.camel.example.spring.javaconfig;

import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.Link;

@SuppressWarnings("serial")
@Link
public class GeoPoint {
	
	@DataField(pos = 1, required = true)
	private String lon;
	@DataField(pos = 2, required = true)
	private String lat;
	
	public String getLon() {
		return lon;
	}
	
	public void setLon(String lon) {
		this.lon = lon;
	}
	
	public String getLat() {
		return lat;
	}
	
	public void setLat(String lat) {
		this.lat = lat;
	}
	
}
