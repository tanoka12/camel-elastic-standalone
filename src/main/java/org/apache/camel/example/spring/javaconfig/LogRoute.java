package org.apache.camel.example.spring.javaconfig;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.spi.DataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LogRoute extends RouteBuilder {



    @Override
    public void configure() throws Exception {

 /*       DataFormat bindy = new BindyCsvDataFormat( GeoCsv.class );

        errorHandler(deadLetterChannel("direct:errors"));

        from( "file://" + directory + "?noop=true&fileName=open-street-data.csv" ).routeId( SETTINGS_ROUTE_ID )
                .split( body().tokenize( "\n", splitGroupNumber, false ) )
                .streaming().unmarshal( bindy ).process( processor ).convertBodyTo( String.class )
                .to( "stream:out" ).end();

        from("direct:errors").routeId(SETTINGS_ERROR_ROUTE_ID).process(new Processor() {
            @Override
            public void process(Exchange exchange) throws Exception {
                System.out.println(":::::: ERROR ::::::" + exchange.getIn().getBody());
            }
        }).setHeader(Exchange.FILE_NAME, constant("report.txt")).to("file:target/reports");
 */
        from("direct:start").log(LoggingLevel.DEBUG, "Processing ${id}").to("bean:foo");
    }

}
