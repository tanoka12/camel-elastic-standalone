package org.apache.camel.example.spring.javaconfig;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.zipfile.ZipFileDataFormat;
import org.springframework.stereotype.Component;

import java.util.Iterator;

@Component
public class ZipRoute extends RouteBuilder {



    @Override
    public void configure() throws Exception {

        ZipFileDataFormat zipFile = new ZipFileDataFormat();
        zipFile.setUsingIterator(true);
        from("file://target/zipfile/?consumer.delay=1000&move=../processed/copy-of-${file:name}").setHeader("kk",simple("$simple{date:now:yyyyMMdd-HHmmss}"))
                .unmarshal(zipFile)
                .split(body(Iterator.class))
                .streaming()
                .process(new Processor() {
                    @Override
                    public void process(Exchange exchange) throws Exception {
                        System.out.println("::: ZIP");
                    }
                }).log("MENASJE: ${header.CamelFileName}").setHeader("koko",constant("KAKKKA"))
                .to("file:target/reportsZip/?fileName=openaddress-${header.kk}/${header.CamelFileName}");

           }

}
