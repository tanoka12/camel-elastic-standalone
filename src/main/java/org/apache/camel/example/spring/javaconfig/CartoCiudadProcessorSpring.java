package org.apache.camel.example.spring.javaconfig;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;


@Component
public class CartoCiudadProcessorSpring implements Processor {
	

	
	@Override
	public void process(Exchange exchange) throws Exception {
		
		//logger.debug( null, "::: CartoCiudadProcessor Procesor:::: " + exchange.getIn().getBody() );
		System.out.println("::: CartoCiudadProcessor Procesor:::: " + exchange.getIn().getBody() );
		
	}
	
}
