package org.apache.camel.example.spring.javaconfig;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.spi.DataFormat;


public class CartoCiudadRoute extends RouteBuilder {
	
	public static final String SETTINGS_ROUTE_ID = "cartoCiudadRote";
	
	private final String directory;
	private final String intermediateDirectory;
	private final String finalDirectory;
	private final String charsetFile;
	
	private final int splitGroupNumber;
	
	public CartoCiudadRoute() {
		
		charsetFile = "UTF-8";
		directory = "files_to_publish";
		intermediateDirectory = "intermedio";
		finalDirectory = "finish";
		
		splitGroupNumber = 3;
		
	}
	
	@Override
	public void configure() throws Exception {
		
		DataFormat bindy = new BindyCsvDataFormat( GeoCsv.class );
		
		from( "file://" + directory + "?noop=true&fileName=open-street-data.csv" ).routeId( SETTINGS_ROUTE_ID )
				.split( body().tokenize( "\n", splitGroupNumber, false ) )
				.streaming().unmarshal( bindy ).process( new CartoCiudadProcessor() ).convertBodyTo( String.class )
				.to( "stream:out" ).end();
		
	}
}
