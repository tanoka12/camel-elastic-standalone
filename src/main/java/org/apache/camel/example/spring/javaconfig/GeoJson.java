package org.apache.camel.example.spring.javaconfig;

import com.fasterxml.jackson.annotation.JsonProperty;


public class GeoJson {


    private String lon;

    private String lat;

    public String getLon() {
        return lon;
    }


    @JsonProperty(required = true)
    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    @Override
    public String toString() {
        return "GeoJson{" +
                "lon='" + lon + '\'' +
                ", lat='" + lat + '\'' +
                '}';
    }
}
