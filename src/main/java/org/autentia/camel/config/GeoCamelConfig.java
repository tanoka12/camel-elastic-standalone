package org.autentia.camel.config;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultShutdownStrategy;
import org.apache.camel.spring.javaconfig.CamelConfiguration;
import org.apache.camel.spring.javaconfig.Main;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.List;


/**
 * A simple example router from a file system to an ActiveMQ queue and then to a file system
 */
@Configuration
@ComponentScan
public class GeoCamelConfig extends CamelConfiguration {


    @Bean
    public DefaultShutdownStrategy shutdown(){

        final DefaultShutdownStrategy defaultShutdownStrategy = new DefaultShutdownStrategy();
        defaultShutdownStrategy.setTimeout(10);
        return defaultShutdownStrategy;
    }

    /**
     * Allow this route to be run as an application
     */
    public static void main(String[] args) throws Exception {
        Main main = new Main();
        main.setConfigClass(org.autentia.camel.config.GeoCamelConfig.class);
        final List<CamelContext> camelContexts = main.getCamelContexts();
        main.run();
    }


}