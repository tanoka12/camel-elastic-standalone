package com.ingdirect.es.f2e.geolocation.repository.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingdirect.es.f2e.geolocation.bean.geo.*;
import com.ingdirect.es.f2e.geolocation.builder.GeoQueryBuilder;
import com.ingdirect.es.f2e.geolocation.builder.GeoRequestBuilder;
import com.ingdirect.es.f2e.geolocation.client.impl.GeoElasticSearchClientImpl;
import com.ingdirect.es.f2e.geolocation.it.repository.impl.BaseRestElasticSearchTestIT;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.test.ESIntegTestCase;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;

@ESIntegTestCase.ClusterScope(scope = ESIntegTestCase.Scope.SUITE, numDataNodes = 1, numClientNodes = 0, transportClientRatio = 0, supportsDedicatedMasters = false)
public class GeoRepositoryImplTest extends BaseRestElasticSearchTestIT {


    private GeoElasticSearchClientImpl geoClient;

    private String indexJsonSource;

    private final String index = "dummy_index";

    private final String find_index = "find_index";

    private final String index_geo = "geo_index";

    private final String mapping_geo = "geolocation";

    private final GeoQueryBuilder builder = new GeoQueryBuilder();

    private List<GeoRequest> locations;

    private final ObjectMapper mapper = new ObjectMapper();

    private GeoRepositoryImpl repository;

    @Override
    public void aditionalInit() throws Exception {

        geoClient = new GeoElasticSearchClientImpl(client);

        repository = new GeoRepositoryImpl(geoClient, mapper);

        GeoRequest location1 = new GeoRequestBuilder().latitude("40.363556").longitude("-3.7684460")
                .province("Madrid")
                .number("53").postalcode("28039").viaName("Valsesangil").viaType("Calle").build();

        GeoRequest location2 = new GeoRequestBuilder().latitude("20.363556").longitude("-45.7684460")
                .province("Cuenca")
                .number("3").postalcode("28054").viaName("Pinar de Sanjose").viaType("Calle").build();

        locations = Arrays.asList(location1, location2);


        //indexJsonSource = F2EApplicationProperties.getProperty( "environment.openaddress.elasticIndex" );

        indexJsonSource = "{\"settings\":{\"index\":{\"number_of_shards\":\"2\",\"number_of_replicas\":\"1\"},\"analysis\":{\"char_filter\":{\"no_commas\":{\"type\":\"mapping\",\"mappings\":[\", => \"]}},\"analyzer\":{\"no_accents\":{\"tokenizer\":\"whitespace\",\"char_filter\":[\"no_commas\"],\"filter\":[\"standard\",\"asciifolding\",\"lowercase\",\"synonym\"]}},\"filter\":{\"synonym\":{\"type\":\"synonym\",\"synonyms\":[\"ODONELL, O'DONELL, O DONELL, O DONNELL\",\"NUESTRA, NTRA, NTRA,\",\"SENORA, SE�ORA, SRA, SRA.\",\"INGENIERO, ING\",\"AVDA, AVENIDA, Av., AVDA., AVD., AVD, AVINGUDA, ETORBIDEA, AV, AVGDA\",\"URB, URB., URBANIZACION, URBANIZA, URBANITZ\",\"c/, CALLE, CL, RUA, c-, C, CARRER, KALEA, RU, CARRE\",\"PZA., PZA, PLAZA, PLACA, PL, PZ, PRAZA, PZ, ENPARANTZA\",\"DR, DOCTOR, DR.\",\"FCO, FCO., FRANCISCO\",\"RODRIGUEZ, RO, RODRIG, RGUEZ\",\"FERNANDEZ, FDEZ\",\"ARQUITECTO, ARQTO, ARQUITECTE\",\"SANTA, STA, STA.\",\"ARCHIDUQUE, ARX, ARXIDUC\",\"MADRE, MARE\",\"DIOS, DEU\",\"PDA, PUJADA\",\"VIRGEN, VIRXEN, MARE DE DEU, MADRE DE DIOS\",\"CAPITAN, CAP\",\"JUAN, JOAN, XOAN, XOAM\",\"GRAL, GENERAL\",\"PASEO, PASSEIG, PG, PS, IBILTOKIA\",\"CAMINO, CMNO, BIDEA, CAMI, CAMIN, CM\",\"GRAN VIA, G.V., GV\",\"HERMANO, GERMA\",\"SAN, S, S., SANT\",\"PASAJE, PSAJE, PJ, PASSATGE\",\"CARRETERA, CTRA, CARRETER, CR\",\"RAMBLA, RBLA, RB\",\"FRAY, FREI\",\"TRAVESIA, TRVA\",\"POLIGONO, POLIG\",\"GLORIETA, GTA\"],\"ignore_case\":true}}}},\"mappings\":{\"geolocation\":{\"_all\":{\"enabled\":false},\"properties\":{\"postalCode\":{\"type\":\"text\",\"copy_to\":\"geo_all\"},\"via_type\":{\"type\":\"text\",\"analyzer\":\"no_accents\",\"copy_to\":\"geo_all\"},\"via_name\":{\"type\":\"text\",\"analyzer\":\"no_accents\",\"copy_to\":\"geo_all\"},\"number\":{\"type\":\"text\",\"copy_to\":\"geo_all\"},\"town\":{\"type\":\"text\",\"analyzer\":\"no_accents\",\"copy_to\":\"geo_all\"},\"province\":{\"type\":\"text\"},\"state\":{\"type\":\"text\"},\"country\":{\"type\":\"text\"},\"location\":{\"type\":\"geo_point\"},\"geo_all\":{\"type\":\"text\",\"analyzer\":\"no_accents\"}}}}}";


        prepareCreate(index_geo).setSource(indexJsonSource, XContentType.JSON).get();

        prepareCreate(find_index).setSource(indexJsonSource, XContentType.JSON).get();

        insertData(find_index, mapping_geo, locations);


    }

    @Test
    public void shouldFindOneLocation() throws Exception {

        GeoRequest findLocation = new GeoRequestBuilder().latitude("40.363556").longitude("-3.7684460")
                .province("Cuenca")
                .number("3").postalcode("28098").viaName("Pinar de Sanjose").viaType("Calle").build();

        QueryParam queryParam = new QueryParamBuilder().withExactQuery(false).withMatchQuery(true).build();

        List<GeoResponse> locations = repository.getLocations(findLocation, find_index, mapping_geo, queryParam);

        assertThat(locations, hasSize(1));
    }


    @Test
    public void shouldFindTwoLocation() throws Exception {

        GeoRequest location3 = new GeoRequestBuilder().latitude("20.363556").longitude("-45.7684460")
                .province("Cuenca")
                .number("3").postalcode("28058").viaName("Pinar de Sanjosa").viaType("Calle").build();

        insertData(find_index, mapping_geo, Arrays.asList(location3));

        GeoRequest findLocation = new GeoRequestBuilder().latitude("40.363556").longitude("-3.7684460")
                .province("Madrid")
                .number("3").postalcode("28098").viaName("Pinar de Sanjose").viaType("Calle").build();

        QueryParam queryParam = new QueryParamBuilder().withExactQuery(false).withMatchQuery(true).build();

        List<GeoResponse> locations = repository.getLocations(findLocation, find_index, mapping_geo, queryParam);

        assertThat(locations, hasSize(2));
    }


    @Test
    public void shouldFindLocationByTown() throws Exception {

        GeoRequest location3 = new GeoRequestBuilder().latitude("20.363556").longitude("-45.7684460")
                .province("Cuenca").town("Tarancon")
                .number("3").postalcode("02012").viaName("Preciados").viaType("Calle").build();

        insertData(find_index, mapping_geo, Arrays.asList(location3));

        GeoRequest findLocation = new GeoRequestBuilder().latitude("").longitude("")
                .province("Cuanca").town("Tarancon")
                .number("3").postalcode("").viaName("Preciados").viaType("Calle").build();

        QueryParam queryParam = new QueryParamBuilder().withExactQuery(false).withMatchQuery(true).build();

        List<GeoResponse> locations = repository.getLocations(findLocation, find_index, mapping_geo, queryParam);

        assertThat(locations, hasSize(1));
    }


    @Test
    public void shouldFindLocationWithOutMatchQuery() throws Exception {

        GeoRequest location3 = new GeoRequestBuilder().latitude("20.363556").longitude("-45.7684460")
                .province("Alicante")
                .number("3").postalcode("03013").viaName("Villajoiosa").viaType("Calle").build();

        insertData(find_index, mapping_geo, Arrays.asList(location3));

        GeoRequest findLocation = new GeoRequestBuilder().latitude("").longitude("")
                .province("Alicante")
                .number("").postalcode("03013").viaName("Andres Villajoiosa").viaType("Calle").build();

        QueryParam queryParam = new QueryParamBuilder().withExactQuery(true).withMatchQuery(false).build();

        List<GeoResponse> locations = repository.getLocations(findLocation, find_index, mapping_geo, queryParam);

        assertThat(locations, hasSize(1));
    }


    @Test
    public void shouldNotFindLocations() throws Exception {

        GeoRequest findLocation = new GeoRequestBuilder().latitude("40.363556").longitude("-3.7684460")
                .province("Madrid")
                .number("3").postalcode("03013").viaName("Pio XII").viaType("Avenida").build();

        QueryParam queryParam = new QueryParamBuilder().withExactQuery(true).withMatchQuery(true).build();

        List<GeoResponse> locations = repository.getLocations(findLocation, find_index, mapping_geo, queryParam);

        assertThat(locations, hasSize(0));
    }

    @Test
    public void shouldInsertListGeoCsvInES() throws Exception {

        List<ErrorESResponse> errors = repository.insertLocations( index_geo, mapping_geo, locations );

        assertThat( errors, hasSize( 0 ) );

    }

    @Test
    public void findLocationByIdShouldReturnGeoPointCorrectly() throws Exception{

        String id = "1234";

        String longitude = "-45.7685";
        String latitude = "20.36888";
        GeoPoint expectedGeoPoint = new GeoPoint( longitude, latitude );

        GeoLocationByIdRequest request = new GeoLocationByIdRequest( id, expectedGeoPoint );

        List<GeoLocationByIdRequest> requests = Arrays.asList( request );
        repository.insertLocations( index_geo, mapping_geo, requests );

        refresh( index_geo );

        GeoPoint geoPoint = repository.findLocationById( index_geo, mapping_geo, id );
        GeoPoint notFoundGeoPoint = repository.findLocationById( index_geo, mapping_geo, "5678" );

        assertEquals( expectedGeoPoint, geoPoint );
        assertNull( notFoundGeoPoint );
    }

}