package com.ingdirect.es.f2e.geolocation.client.impl;

import com.ingdirect.es.f2e.geolocation.it.repository.impl.BaseRestElasticSearchTestIT;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.test.ESIntegTestCase;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;


@ESIntegTestCase.ClusterScope(scope = ESIntegTestCase.Scope.SUITE, numDataNodes = 1, numClientNodes = 0, transportClientRatio = 0, supportsDedicatedMasters = false)
public class GeoElasticSearchClientImplTest extends BaseRestElasticSearchTestIT {


    private GeoElasticSearchClientImpl geoElasticSearchClient;

    protected final String atm_index = "geo-poi-atm-20170408-100819";

    protected final String new_index = "geo-poi-new-index-20170403-162635";

    protected final String delete_index = "delete_index";

    protected String indexLocationSource;

    protected String indexAtmSource;

    @Override
    public void aditionalInit() throws Exception {

        geoElasticSearchClient = new GeoElasticSearchClientImpl(client);

        indexAtmSource = "{\"settings\":{\"index\":{\"number_of_shards\":\"2\",\"number_of_replicas\":\"1\"}},\"mappings\":{\"geolocation\":{\"properties\":{\"location\":{\"type\":\"geo_point\"},\"id\":{\"type\":\"string\"},\"red4B\":{\"type\":\"boolean\"},\"red6000\":{\"type\":\"boolean\"},\"servired\":{\"type\":\"boolean\"},\"entityCode\":{\"type\":\"string\"},\"subcategory\":{\"type\":\"keyword\"},\"branchCode\":{\"type\":\"string\"},\"branch\":{\"type\":\"string\"},\"atmCode\":{\"type\":\"string\"},\"ordinal\":{\"type\":\"string\"},\"province\":{\"type\":\"string\"},\"state\":{\"type\":\"string\"},\"postalCode\":{\"type\":\"string\"},\"town\":{\"type\":\"string\"},\"viaType\":{\"type\":\"string\"},\"viaName\":{\"type\":\"string\"},\"number\":{\"type\":\"string\"},\"rdo\":{\"type\":\"string\"},\"success\":{\"type\":\"integer\"},\"source\":{\"type\":\"keyword\"}}}}}";

        indexLocationSource = "{\"settings\":{\"index\":{\"number_of_shards\":\"2\",\"number_of_replicas\":\"1\"},\"analysis\":{\"char_filter\":{\"no_commas\":{\"type\":\"mapping\",\"mappings\":[\", => \"]}},\"analyzer\":{\"no_accents\":{\"tokenizer\":\"whitespace\",\"char_filter\":[\"no_commas\"],\"filter\":[\"standard\",\"asciifolding\",\"lowercase\",\"synonym\"]}},\"filter\":{\"synonym\":{\"type\":\"synonym\",\"synonyms\":[\"ODONELL, O'DONELL, O DONELL, O DONNELL\",\"NUESTRA, NTRA, NTRA,\",\"SENORA, SE�ORA, SRA, SRA.\",\"INGENIERO, ING\",\"AVDA, AVENIDA, Av., AVDA., AVD., AVD, AVINGUDA, ETORBIDEA, AV, AVGDA\",\"URB, URB., URBANIZACION, URBANIZA, URBANITZ\",\"c/, CALLE, CL, RUA, c-, C, CARRER, KALEA, RU, CARRE\",\"PZA., PZA, PLAZA, PLACA, PL, PZ, PRAZA, PZ, ENPARANTZA\",\"DR, DOCTOR, DR.\",\"FCO, FCO., FRANCISCO\",\"RODRIGUEZ, RO, RODRIG, RGUEZ\",\"FERNANDEZ, FDEZ\",\"ARQUITECTO, ARQTO, ARQUITECTE\",\"SANTA, STA, STA.\",\"ARCHIDUQUE, ARX, ARXIDUC\",\"MADRE, MARE\",\"DIOS, DEU\",\"PDA, PUJADA\",\"VIRGEN, VIRXEN, MARE DE DEU, MADRE DE DIOS\",\"CAPITAN, CAP\",\"JUAN, JOAN, XOAN, XOAM\",\"GRAL, GENERAL\",\"PASEO, PASSEIG, PG, PS, IBILTOKIA\",\"CAMINO, CMNO, BIDEA, CAMI, CAMIN, CM\",\"GRAN VIA, G.V., GV\",\"HERMANO, GERMA\",\"SAN, S, S., SANT\",\"PASAJE, PSAJE, PJ, PASSATGE\",\"CARRETERA, CTRA, CARRETER, CR\",\"RAMBLA, RBLA, RB\",\"FRAY, FREI\",\"TRAVESIA, TRVA\",\"POLIGONO, POLIG\",\"GLORIETA, GTA\"],\"ignore_case\":true}}}},\"mappings\":{\"geolocation\":{\"_all\":{\"enabled\":false},\"properties\":{\"postalCode\":{\"type\":\"text\",\"copy_to\":\"geo_all\"},\"via_type\":{\"type\":\"text\",\"analyzer\":\"no_accents\",\"copy_to\":\"geo_all\"},\"via_name\":{\"type\":\"text\",\"analyzer\":\"no_accents\",\"copy_to\":\"geo_all\"},\"number\":{\"type\":\"text\",\"copy_to\":\"geo_all\"},\"town\":{\"type\":\"text\",\"analyzer\":\"no_accents\",\"copy_to\":\"geo_all\"},\"province\":{\"type\":\"text\"},\"state\":{\"type\":\"text\"},\"country\":{\"type\":\"text\"},\"location\":{\"type\":\"geo_point\"},\"geo_all\":{\"type\":\"text\",\"analyzer\":\"no_accents\"}}}}}";

        prepareCreate( atm_index ).setSource(indexAtmSource, XContentType.JSON ).get();

        prepareCreate( delete_index ).setSource(indexAtmSource, XContentType.JSON ).get();


    }

    @Test
    public void shouldCreateIndex() throws Exception {

        final boolean createdIndex = geoElasticSearchClient.createIndex("index");

        assertThat(createdIndex, equalTo(true));
    }

    @Test
    public void shouldCreateIndexWithSettings() throws Exception {

        final boolean createdIndex = geoElasticSearchClient.createIndex(new_index, indexLocationSource);

        assertThat(createdIndex, equalTo(true));
    }

    @Test
    public void shouldExistsIndex() throws Exception {

        final boolean existIndex = geoElasticSearchClient.existsIndex(atm_index);
        assertThat(existIndex, equalTo(true));
    }

    @Test
    public void shouldNotExistsIndex() throws Exception {

        final boolean existIndex = geoElasticSearchClient.existsIndex("notExistIndex");
        assertThat(existIndex, equalTo(false));
    }


    @Test
    public void shouldDeleteIndex() throws Exception {

        final boolean isDeleteIndex = geoElasticSearchClient.deleteIndex(delete_index);
        assertThat(isDeleteIndex, equalTo(true));
    }


    @Test
    public void shouldRefreshIndex() throws Exception {

        geoElasticSearchClient.refreshIndex(atm_index);

    }


    @Test
    public void shouldGetMaxIndexByPrefix() throws Exception {

        prepareCreate("openaddress-20170121-121658").get();
        prepareCreate("openaddress-20170122-121702").get();
        prepareCreate("openaddress-20170123-122147").get();

        final String recentIndexByPrefix = geoElasticSearchClient.getRecentIndexByPrefix("openaddress-*");

        assertThat(recentIndexByPrefix, equalTo("openaddress-20170123-122147"));

    }
}