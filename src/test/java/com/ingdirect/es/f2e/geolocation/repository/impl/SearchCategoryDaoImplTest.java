package com.ingdirect.es.f2e.geolocation.repository.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingdirect.es.f2e.geolocation.bean.geo.*;
import com.ingdirect.es.f2e.geolocation.client.impl.GeoElasticSearchClientImpl;
import com.ingdirect.es.f2e.geolocation.ctes.PoiConstant;
import com.ingdirect.es.f2e.geolocation.it.repository.impl.BaseRestElasticSearchTestIT;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.test.ESIntegTestCase;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;

@ESIntegTestCase.ClusterScope(scope = ESIntegTestCase.Scope.SUITE, numDataNodes = 1, numClientNodes = 0, transportClientRatio = 0, supportsDedicatedMasters = false)
public class SearchCategoryDaoImplTest extends BaseRestElasticSearchTestIT {

    private GeoElasticSearchClientImpl geoClient;

    private SearchCategoryDaoImpl repository;

    private String indexAtmJsonSource;

    private String indexTwypJsonSource;

    private String indexOsmJsonSource;

    private final String twyp_index = "geo-poi-twyp-20170417-102601";

    private final String atm_index = "geo-poi-atm-20170407-100819";

    private final String osm_index = "geo-poi-osm-20170403-162635";

    private final String mapping_geo = "geolocation";

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public void aditionalInit() throws Exception {

        geoClient = new GeoElasticSearchClientImpl(client);

        repository = new SearchCategoryDaoImpl(geoClient, mapper);

        //indexAtmJsonSource = F2EApplicationProperties.getProperty( "environment.atmgeolocation.elasticIndex" );

        indexAtmJsonSource = "{\"settings\":{\"index\":{\"number_of_shards\":\"2\",\"number_of_replicas\":\"1\"}},\"mappings\":{\"geolocation\":{\"properties\":{\"location\":{\"type\":\"geo_point\"},\"id\":{\"type\":\"string\"},\"red4B\":{\"type\":\"boolean\"},\"red6000\":{\"type\":\"boolean\"},\"servired\":{\"type\":\"boolean\"},\"entityCode\":{\"type\":\"string\"},\"subcategory\":{\"type\":\"keyword\"},\"branchCode\":{\"type\":\"string\"},\"branch\":{\"type\":\"string\"},\"atmCode\":{\"type\":\"string\"},\"ordinal\":{\"type\":\"string\"},\"province\":{\"type\":\"string\"},\"state\":{\"type\":\"string\"},\"postalCode\":{\"type\":\"string\"},\"town\":{\"type\":\"string\"},\"viaType\":{\"type\":\"string\"},\"viaName\":{\"type\":\"string\"},\"number\":{\"type\":\"string\"},\"rdo\":{\"type\":\"string\"},\"success\":{\"type\":\"integer\"},\"source\":{\"type\":\"keyword\"}}}}}";

        //indexTwypJsonSource = F2EApplicationProperties.getProperty( "environment.twyp.elasticIndex" );

        indexTwypJsonSource = "{\"settings\":{\"index\":{\"number_of_shards\":\"2\",\"number_of_replicas\":\"1\"},\"analysis\":{\"char_filter\":{\"no_commas\":{\"type\":\"mapping\",\"mappings\":[\", => \"]}},\"analyzer\":{\"no_accents\":{\"tokenizer\":\"whitespace\",\"char_filter\":[\"no_commas\"],\"filter\":[\"standard\",\"asciifolding\",\"lowercase\",\"synonym\"]}},\"filter\":{\"synonym\":{\"type\":\"synonym\",\"synonyms\":[\"ODONELL, O'DONELL, O DONELL, O DONNELL\",\"NUESTRA, NTRA, NTRA,\",\"SE�ORA, SRA, SRA.\",\"AVDA, AVENIDA, Av., AVDA., AVD., AVD\",\"URB, URB., URBANIZACION\",\"c/, CALLE, c-\",\"PZA., PZA, PLAZA\",\"DR, DOCTOR, DR.\",\"FCO, FRANCISCO\",\"GRAL, GENERAL\"],\"ignore_case\":true}}}},\"mappings\":{\"geolocation\":{\"properties\":{\"via_name\":{\"type\":\"string\",\"analyzer\":\"no_accents\"},\"postalCode\":{\"type\":\"string\"},\"town\":{\"type\":\"string\",\"analyzer\":\"no_accents\"},\"province\":{\"type\":\"string\"},\"state\":{\"type\":\"string\"},\"country\":{\"type\":\"string\"},\"subcategory\":{\"type\":\"keyword\"},\"location\":{\"type\":\"geo_point\"}},\"_all\":{\"analyzer\":\"no_accents\"}}}}";

        //indexOsmJsonSource = F2EApplicationProperties.getProperty( "environment.poi.elasticIndex" );

        indexOsmJsonSource = "{\"settings\":{\"index\":{\"number_of_shards\":\"2\",\"number_of_replicas\":\"1\"}},\"mappings\":{\"geolocation\":{\"properties\":{\"id\":{\"type\":\"string\"},\"uid\":{\"type\":\"string\"},\"version\":{\"type\":\"string\"},\"changeset\":{\"type\":\"string\"},\"timestamp\":{\"type\":\"string\"},\"category\":{\"type\":\"string\"},\"subcategory\":{\"type\":\"keyword\"},\"name\":{\"type\":\"string\"},\"location\":{\"type\":\"geo_point\"},\"other\":{\"type\":\"string\"}}}}}";

        prepareCreate( atm_index ).setSource( indexAtmJsonSource, XContentType.JSON ).get();

        prepareCreate( twyp_index ).setSource( indexTwypJsonSource, XContentType.JSON ).get();

        prepareCreate( osm_index ).setSource( indexOsmJsonSource, XContentType.JSON ).get();

        insertData( atm_index, mapping_geo, generateAtm() );

        insertData( twyp_index, mapping_geo, generateTwyp() );

        insertData( osm_index, mapping_geo, generatePoi() );


    }

    @Test
    public void shouldSearchOsmSchool() throws Exception {

        CategoryRequest request = new CategoryRequest();
        request.addCategory( "school" );

        List<PoiSubCategoryResponse> responses = repository.getPoiByCategory( request );

        assertThat( responses, is( not( nullValue() ) ) );

        PoiSubCategoryResponse poiSchool = responses.get( 0 );
        assertThat( poiSchool.getSubcategory(), equalTo( "school" ) );
        assertThat( poiSchool.getRelevantInfo(), equalTo( "Colegio Abaco" ) );
        assertThat( poiSchool.getLatitude(), equalTo( "3.23" ) );
        assertThat( poiSchool.getLongitude(), equalTo( "-5.908" ) );
        assertThat( poiSchool.getIndex(), equalTo( PoiConstant.POI_OSM ) );
    }

    @Test
    public void shouldReturnPoisByCategoriesAndBuffer() throws Exception {
        // Given
        String profile = "foot";

        List<CategoryInfo> categoriesInfo = Arrays.asList(
                new CategoryInfo( "BANKIA", "icon001", "/icons/icon01", 100, 200 ),
                new CategoryInfo( "DIA", "icon002", "/icons/icon02", 100, 200 ) );

        String buffer = "1000";
        String processType = "TILES";

        int mapWidth = 750;
        int mapHeight = 500;

        GeoPoint location = new GeoPoint( "-3.7655529", "40.3635086" );
        MapQueryRequest mapQuery = new MapQueryRequest( buffer, profile, 1, processType, categoriesInfo, mapWidth,
                mapHeight );
        mapQuery.setLocation( location );
        // When
        final List<PoiSubCategoryResponse> pois = repository.getPoiByCategoryAndBuffer( mapQuery );
        // Then
        assertThat( pois.size(), equalTo( 2 ) );
    }

    @Test
    public void shouldSearchTwypDIA() throws Exception {

        CategoryRequest request = new CategoryRequest();
        request.addCategory( "DIA" );

        List<PoiSubCategoryResponse> responses = repository.getPoiByCategory( request );

        //assertThat(responses, is(not(nullValue())));

        PoiSubCategoryResponse poiSchool = responses.get( 0 );
        assertThat( poiSchool.getSubcategory(), equalTo( "DIA" ) );
        assertThat( poiSchool.getRelevantInfo(), equalTo( "Calle del Dia 20890" ) );
        assertThat( poiSchool.getIndex(), equalTo( PoiConstant.POI_TWYP ) );
    }

    @Test
    public void shouldSearchAtmBankia() throws Exception {

        CategoryRequest request = new CategoryRequest();
        request.addCategory( "BANKIA" );

        List<PoiSubCategoryResponse> responses = repository.getPoiByCategory( request );

        assertThat( responses, is( not( nullValue() ) ) );

        PoiSubCategoryResponse poiSchool = responses.get( 0 );
        assertThat( poiSchool.getSubcategory(), equalTo( "BANKIA" ) );
        assertThat( poiSchool.getRelevantInfo(), equalTo( "viaType viaName number postalCode" ) );
        assertThat( poiSchool.getIndex(), equalTo( PoiConstant.POI_ATM ) );
    }

    @Test
    public void shouldSearchInDifferentPoiIndex() throws Exception {

        CategoryRequest request = new CategoryRequest();
        request.addCategory( "school" );
        request.addCategory( "DIA" );
        request.addCategory( "BANKIA" );

        List<PoiSubCategoryResponse> responses = repository.getPoiByCategory( request );

        assertThat( responses, is( not( nullValue() ) ) );

        assertThat( responses, hasSize( 3 ) );

        final List<String> poiIndex = responses.stream().map( response -> response.getIndex() )
                .collect( Collectors.toList() );

        assertThat( poiIndex, containsInAnyOrder( PoiConstant.POI_ATM, PoiConstant.POI_TWYP, PoiConstant.POI_OSM ) );

    }

    @Test
    public void shouldReturnIndexFromPois() throws Exception {
        // Given
        prepareCreate( "geo-poi-atm-20180407-100819" ).setSource( indexAtmJsonSource, XContentType.JSON ).get();
        prepareCreate( "geo-poi-osm-20180403-162635" ).setSource( indexTwypJsonSource, XContentType.JSON ).get();
        prepareCreate( "geo-poi-twyp-20180403-162635" ).setSource( indexOsmJsonSource, XContentType.JSON ).get();

        // When
        String[] index = repository.getPoisIndex();

        // Then
        assertThat( index[0], equalTo( "geo-poi-atm-20180407-100819" ) );
        assertThat( index[1], equalTo( "geo-poi-twyp-20180403-162635" ) );
        assertThat( index[2], equalTo( "geo-poi-osm-20180403-162635" ) );

    }

    @Test
    public void shouldReturnAllCategory() throws Exception {
        // Given

        // When
        List<CategoryResponse> categories = repository.getAllCategories();
        // Then
        assertThat( categories, hasSize( 6 ) );
    }


    private List<AtmRequest> generateAtm() {

        AtmRequest atm1 = new AtmRequest( new GeoPoint( "-3.75681111011152", "40.368287486990695" ), "0001", "red4b",
                "red6000", "servired", "entityCode",
                "BANKIA", "branchCode", "Sucursal Bankia", "atmCode", "ordinal", "province",
                "state", "postalCode", "town", "viaType", "viaName", "number", "rdo", 1, "carto" );

        AtmRequest atm2 = new AtmRequest( new GeoPoint( "-30.333", "13.4" +
                "5" ), "0002", "red4b", "red6000", "servired", "entityCode",
                "CAJA RURAL DE GIJON", "branchCode", "branch", "atmCode", "ordinal", "province",
                "state", "postalCode", "town", "viaType", "viaName", "number", "rdo", 1, "carto" );

        return Arrays.asList( atm1, atm2 );
    }

    private List<TwypRequest> generateTwyp() {

        TwypRequest twyp1 = new TwypRequest( "1", "20890", new GeoPoint( "-3.758955", "40.36834" ), "Cuenca", "Cuenca",
                "Calle del Dia", "Cuenca", "DIA" );
        TwypRequest twyp2 = new TwypRequest( "2", "20891", new GeoPoint( "-44.55", "3.67" ), "Madrid", "Madrid",
                "Avenida", "Madrid", "GALP" );
        return Arrays.asList( twyp1, twyp2 );
    }

    private List<PointInterestRequest> generatePoi() {

        PointInterestRequest poi1 = new PointInterestRequest( "01", "uuid", "v1", "00", "001", "cat1", "school",
                "Colegio Abaco", new GeoPoint( "-5.908", "3.23" ), "Colegio" );
        PointInterestRequest poi2 = new PointInterestRequest( "02", "uuid", "v2", "00", "002", "cat2", "pharmacy",
                "Farmacia de Guardia", new GeoPoint( "-6.908", "3.23" ), "Medico" );
        return Arrays.asList( poi1, poi2 );
    }

}