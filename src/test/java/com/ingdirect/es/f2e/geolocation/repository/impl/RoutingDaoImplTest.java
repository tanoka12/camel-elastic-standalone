package com.ingdirect.es.f2e.geolocation.repository.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingdirect.es.f2e.geolocation.bean.geo.ErrorESResponse;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoPoint;
import com.ingdirect.es.f2e.geolocation.bean.geo.RoutingCsv;
import com.ingdirect.es.f2e.geolocation.builder.RoutingCsvBuilder;
import com.ingdirect.es.f2e.geolocation.client.impl.GeoElasticSearchClientImpl;
import com.ingdirect.es.f2e.geolocation.it.repository.impl.BaseRestElasticSearchTestIT;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.test.ESIntegTestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;

@ESIntegTestCase.ClusterScope(scope = ESIntegTestCase.Scope.SUITE, numDataNodes = 1, numClientNodes = 0, transportClientRatio = 0, supportsDedicatedMasters = false)
public class RoutingDaoImplTest extends BaseRestElasticSearchTestIT {

    private static final String FIND_UUID_INDEX = "mi_uuid";

    private static final String HEADER = "ID,LONGITUD,LATITUD,TIPO_VIA,NOMBRE_VIA,NUMERO,COD_POSTAL,SEXO,SOURCE,ACIERTO,TIPO_VIA_GEO,NOMBRE_VIA_GEO,NUMERO_GEO,COD_POSTAL_GEO";

    private GeoElasticSearchClientImpl geoClient;

    private RoutingDaoImpl repository;

    private String indexJsonSource;

    private final String UUID_INDEX = "uuid_index";

    private final String BUFFER = "1500";

    private static final String TARGET_MAPPER = "routing_target";

    private static final String HEADER_MAPPER = "header";

    private static final String DELETE_UUID_INDEX = "delete_index";

    private final ObjectMapper mapper = new ObjectMapper();


    @Override
    public void aditionalInit() throws Exception {

        geoClient = new GeoElasticSearchClientImpl(client);

        repository = new RoutingDaoImpl(geoClient, mapper);

        // indexJsonSource = F2EApplicationProperties.getProperty("environment.routing.target.elasticIndex");
        indexJsonSource = "{\"settings\":{\"index\":{\"number_of_shards\":\"2\",\"number_of_replicas\":\"1\"}},\"mappings\":{\"routing_target\":{\"properties\":{\"id\":{\"type\":\"string\"},\"location\":{\"type\":\"geo_point\"},\"others\":{\"type\":\"string\"}}},\"header\":{\"properties\":{\"header\":{\"type\":\"string\"}}}}}";

        prepareCreate(UUID_INDEX).setSource(indexJsonSource, XContentType.JSON).get();

        prepareCreate(FIND_UUID_INDEX).setSource(indexJsonSource, XContentType.JSON).get();

        prepareCreate(DELETE_UUID_INDEX).setSource(indexJsonSource, XContentType.JSON).get();

        insertHeader(FIND_UUID_INDEX, HEADER_MAPPER);

        createDataFindBufferTest(FIND_UUID_INDEX, TARGET_MAPPER);


    }

    @Test
    public void shouldGetNearestLocation() throws Exception {

        RoutingCsv source = new
                RoutingCsvBuilder().id("1").latitude("40.3635086")
                .otherFields("Calle,Pinar de San jose,30,28054,M,OPENADDRESS,2,Calle,Pinar de San jose,30,28054")
                .longitude("-3.7655529").build();

        List<RoutingCsv> nearestLocations = repository.getNearestLocations(FIND_UUID_INDEX, source, BUFFER);

        assertThat(nearestLocations, notNullValue());
        assertThat(nearestLocations, hasSize(3));

    }


    @Test
    public void shouldDeleteIndex() throws Exception {
        boolean deleteIndex = repository.deleteIndex(DELETE_UUID_INDEX);
        assertThat(deleteIndex, equalTo(true));
    }

    @Test
    public void shouldGetHeader() throws Exception {
        String header = repository.getRoutingHeader(FIND_UUID_INDEX);
        assertThat(header, equalTo(HEADER));
    }

    @Test
    public void shouldInsertRoutingTargets() throws Exception {

        List<ErrorESResponse> errors = repository.insertRoutingTargets(UUID_INDEX, createListRoutingTarget());

        assertThat(errors, hasSize(0));

    }

    @Test
    public void shouldInsertHeader() throws Exception {

        RoutingCsv header = new RoutingCsv(
                "ID",
                new GeoPoint("LATITUDE", "LONGITUDE"),
                "TIPO_VIA,NOMBRE_VIA,NUMERO,COD_POSTAL,SEXO,SOURCE,ACIERTO,TIPO_VIA_GEO,NOMBRE_VIA_GEO,NUMERO_GEO,COD_POSTAL_GEO",
                true, false);
        ErrorESResponse errors = repository.insertRoutingHeader(UUID_INDEX, header);

        assertThat(errors, notNullValue());
        assertThat(errors.getDetail(), equalTo("CREATED"));

    }

    private List<RoutingCsv> createListRoutingTarget() {
        RoutingCsv csv = new RoutingCsv("1", new GeoPoint("12.8989", "-3.0909"),
                "calle, del pino,30, madrid, madrid, openaddress",
                false, false);
        RoutingCsv csv2 = new RoutingCsv("2", new GeoPoint("1.8989", "-8.0909"),
                "avenida, valsesangil,53, madrid, madrid, openaddress", false, false);

        List<RoutingCsv> targets = new ArrayList<RoutingCsv>();
        targets.add(csv);
        targets.add(csv2);
        return targets;
    }


    private void createDataFindBufferTest(String indexFind, String mapping) {

        RoutingCsv csv = new RoutingCsv("1", new GeoPoint("-3.7556269", "40.3685862"),
                "Av.,Carabanchel Alto,80,28044,V,CARTOCIUDAD,3,Av.,Carabanchel Alto,80,28044",
                false, false);
        RoutingCsv csv2 = new RoutingCsv("2", new GeoPoint("-3.7634929", "40.3641792"),
                ",Av.,de la Peseta,22,28054,V,CARTOCIUDAD,1,Av.,de la Peseta,22,28054", false, false);

        RoutingCsv csv3 = new RoutingCsv("3", new GeoPoint("-3.741027", "40.3627155"),
                "Calle,Real,1,28054,V,CARTOCIUDAD,2,Calle,Real,1,28054",
                false, false);
        RoutingCsv csv4 = new RoutingCsv("4", new GeoPoint("-3.7663498", "40.3728691"),
                "Calle,de Joaqu�n Turina,45,28044,M,OPENADDRESS,1,Calle,de Joaqu�n Turina,45", false, false);

        List<RoutingCsv> insertLocations = new ArrayList<RoutingCsv>();
        insertLocations.add(csv);
        insertLocations.add(csv2);
        insertLocations.add(csv3);
        insertLocations.add(csv4);

        insertData(indexFind, mapping, insertLocations);

    }


    private void insertHeader(String index, String mapper) throws Exception {
        IndexRequest indexRequest = new IndexRequest(index, mapper, "1");

        indexRequest.source(
                jsonBuilder()
                        .startObject()
                        .field("header",
                                HEADER)
                        .endObject()
        );
        client().index(indexRequest).actionGet();

        refresh(FIND_UUID_INDEX);
    }

}