package com.ingdirect.es.f2e.geolocation.it.repository.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingdirect.es.f2e.geolocation.bean.geo.ESRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.common.network.NetworkModule;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.http.HttpTransportSettings;
import org.elasticsearch.plugins.Plugin;
import org.elasticsearch.test.ESIntegTestCase;
import org.elasticsearch.transport.Netty4Plugin;
import org.junit.After;
import org.junit.Before;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public abstract class BaseRestElasticSearchTestIT extends ESIntegTestCase {

    protected final ObjectMapper mapper = new ObjectMapper();

    protected RestClient client;

    @Before
    protected void initTest() throws Exception {

        client = getRestClient();
        aditionalInit();

    }

    protected void aditionalInit() throws Exception {

    }


    protected void insertData(String indexFind, String mapping, List<? extends ESRequest> esObjects) {

        esObjects.forEach(location -> {
            IndexRequest indexRequest = new IndexRequest(indexFind, mapping,
                    UUID.randomUUID().toString());
            try {
                indexRequest.source(mapper.writeValueAsString(location), XContentType.JSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            final IndexResponse indexResponse = client().index(indexRequest).actionGet();
            System.out.println(indexResponse);
        });
        flushAndRefresh(indexFind);
    }


    protected void createSimpleDataWithId(String indexFind, String mapping, ESRequest request) throws Exception {
        IndexRequest indexRequest = new IndexRequest(indexFind, mapping, request.getIndexId());
        indexRequest.source(mapper.writeValueAsString(request), XContentType.JSON);
        client().index(indexRequest).actionGet();
        refresh(indexFind);
    }

    @Override
    protected Collection<Class<? extends Plugin>> nodePlugins() {
        return Arrays.asList(Netty4Plugin.class);
    }

    @Override
    protected Settings nodeSettings(int nodeOrdinal) {
        int randomPort = randomIntBetween(49152, 65525);

        Settings.Builder builder = Settings.builder()
                .put(super.nodeSettings(nodeOrdinal))
                .put(NetworkModule.HTTP_ENABLED.getKey(), true)
                .put(HttpTransportSettings.SETTING_HTTP_PORT.getKey(), randomPort);
        Settings settings = builder.build();
        return settings;
    }


    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        //cluster().close();
    }

}
