package com.ingdirect.es.f2e.geolocation.repository.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingdirect.es.f2e.geolocation.bean.geo.ErrorESResponse;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoPoint;
import com.ingdirect.es.f2e.geolocation.bean.geo.PointInterestRequest;
import com.ingdirect.es.f2e.geolocation.client.impl.GeoElasticSearchClientImpl;
import com.ingdirect.es.f2e.geolocation.it.repository.impl.BaseRestElasticSearchTestIT;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.test.ESIntegTestCase;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;


@ESIntegTestCase.ClusterScope(scope = ESIntegTestCase.Scope.SUITE, numDataNodes = 1, numClientNodes = 0, transportClientRatio = 0, supportsDedicatedMasters = false)
public class PointInterestTestIT extends BaseRestElasticSearchTestIT {

    private ElasticSearchRestDaoImpl repository;

    private GeoElasticSearchClientImpl geoClient;

    private String indexJsonSource;

    private final String point_create_index = "create_point_index";

    private final String point_insert_index = "point_insert_index";

    private final String mapping_geo = "geolocation";

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public void aditionalInit() throws Exception {

        geoClient = new GeoElasticSearchClientImpl(client);

        repository = new ElasticSearchRestDaoImpl(geoClient, mapper);

//        indexJsonSource = F2EApplicationProperties.getProperty( "environment.poi.elasticIndex" );

        indexJsonSource = "{\"settings\":{\"index\":{\"number_of_shards\":\"2\",\"number_of_replicas\":\"1\"}},\"mappings\":{\"geolocation\":{\"properties\":{\"id\":{\"type\":\"string\"},\"uid\":{\"type\":\"string\"},\"version\":{\"type\":\"string\"},\"changeset\":{\"type\":\"string\"},\"timestamp\":{\"type\":\"string\"},\"category\":{\"type\":\"string\"},\"subcategory\":{\"type\":\"keyword\"},\"name\":{\"type\":\"string\"},\"location\":{\"type\":\"geo_point\"},\"other\":{\"type\":\"string\"}}}}}";


        prepareCreate(point_insert_index).setSource(indexJsonSource, XContentType.JSON).get();


    }

    @Test
    public void shouldCreateIndex() throws Exception {

        boolean ret = repository.createIndex( point_create_index, indexJsonSource );

        assertThat( ret, is( true ) );

    }

    @Test
    public void shouldInsertListGeoCsvInES() throws Exception {

        List<PointInterestRequest> points = createPoints();
        List<ErrorESResponse> errors = repository.insertLocations( point_insert_index, mapping_geo, points );

        assertThat( errors, hasSize( 0 ) );

    }

    private List<PointInterestRequest> createPoints() {
        GeoPoint location = new GeoPoint( "-3.7684460", "40.363556" );
        PointInterestRequest point = new PointInterestRequest( "1", "dsds", "v1", "changeset", "timestamp", "cat1",
                "subcat1", "name1", location, "other1" );

        GeoPoint location2 = new GeoPoint( "-3.7684460", "40.363556" );
        PointInterestRequest point2 = new PointInterestRequest( "2", "sdsds", "v2", "changeset2", "timestamp2", "cat2",
                "subcat2", "nam2", location2, "other2" );

        return Arrays.asList( point, point2 );
    }

}
