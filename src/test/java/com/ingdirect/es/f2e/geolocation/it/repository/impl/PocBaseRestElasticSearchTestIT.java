package com.ingdirect.es.f2e.geolocation.it.repository.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingdirect.es.f2e.geolocation.bean.geo.AtmRequest;
import com.ingdirect.es.f2e.geolocation.bean.geo.ESRequest;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoPoint;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoRequest;
import com.ingdirect.es.f2e.geolocation.builder.GeoRequestBuilder;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.common.network.NetworkModule;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.http.HttpTransportSettings;
import org.elasticsearch.plugins.Plugin;
import org.elasticsearch.test.ESIntegTestCase;
import org.elasticsearch.transport.Netty4Plugin;
import org.junit.Before;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public abstract class PocBaseRestElasticSearchTestIT extends ESIntegTestCase {

    protected final ObjectMapper mapper = new ObjectMapper();

    protected RestClient client;

    protected String indexLocationSource;

    protected String indexAtmSource;


    protected List<GeoRequest> locations;

    protected final String openaddress_index = "geo-openaddress-20170407-100819";

    protected final String atm_index = "geo-poi-atm-20170408-100819";

    protected final String new_index = "geo-poi-new-index-20170403-162635";

    protected final String mapping_geo = "geolocation";

    protected final String find_index = "find_index";

    protected final String delete_index = "delete_index";



    @Before
    protected void initTest() throws Exception{

        client = getRestClient();

        GeoRequest location1 = new GeoRequestBuilder().latitude( "40.363556" ).longitude( "-3.7684460" )
                .province( "Madrid" )
                .number( "53" ).postalcode( "28039" ).viaName( "Valsesangil" ).viaType( "Calle" ).build();

        GeoRequest location2 = new GeoRequestBuilder().latitude( "20.363556" ).longitude( "-45.7684460" )
                .province( "Cuenca" )
                .number( "3" ).postalcode( "28054" ).viaName( "Pinar de Sanjose" ).viaType( "Calle" ).build();

        locations = Arrays.asList( location1, location2 );

        indexLocationSource = "{\"settings\":{\"index\":{\"number_of_shards\":\"2\",\"number_of_replicas\":\"1\"},\"analysis\":{\"char_filter\":{\"no_commas\":{\"type\":\"mapping\",\"mappings\":[\", => \"]}},\"analyzer\":{\"no_accents\":{\"tokenizer\":\"whitespace\",\"char_filter\":[\"no_commas\"],\"filter\":[\"standard\",\"asciifolding\",\"lowercase\",\"synonym\"]}},\"filter\":{\"synonym\":{\"type\":\"synonym\",\"synonyms\":[\"ODONELL, O'DONELL, O DONELL, O DONNELL\",\"NUESTRA, NTRA, NTRA,\",\"SENORA, SE�ORA, SRA, SRA.\",\"INGENIERO, ING\",\"AVDA, AVENIDA, Av., AVDA., AVD., AVD, AVINGUDA, ETORBIDEA, AV, AVGDA\",\"URB, URB., URBANIZACION, URBANIZA, URBANITZ\",\"c/, CALLE, CL, RUA, c-, C, CARRER, KALEA, RU, CARRE\",\"PZA., PZA, PLAZA, PLACA, PL, PZ, PRAZA, PZ, ENPARANTZA\",\"DR, DOCTOR, DR.\",\"FCO, FCO., FRANCISCO\",\"RODRIGUEZ, RO, RODRIG, RGUEZ\",\"FERNANDEZ, FDEZ\",\"ARQUITECTO, ARQTO, ARQUITECTE\",\"SANTA, STA, STA.\",\"ARCHIDUQUE, ARX, ARXIDUC\",\"MADRE, MARE\",\"DIOS, DEU\",\"PDA, PUJADA\",\"VIRGEN, VIRXEN, MARE DE DEU, MADRE DE DIOS\",\"CAPITAN, CAP\",\"JUAN, JOAN, XOAN, XOAM\",\"GRAL, GENERAL\",\"PASEO, PASSEIG, PG, PS, IBILTOKIA\",\"CAMINO, CMNO, BIDEA, CAMI, CAMIN, CM\",\"GRAN VIA, G.V., GV\",\"HERMANO, GERMA\",\"SAN, S, S., SANT\",\"PASAJE, PSAJE, PJ, PASSATGE\",\"CARRETERA, CTRA, CARRETER, CR\",\"RAMBLA, RBLA, RB\",\"FRAY, FREI\",\"TRAVESIA, TRVA\",\"POLIGONO, POLIG\",\"GLORIETA, GTA\"],\"ignore_case\":true}}}},\"mappings\":{\"geolocation\":{\"_all\":{\"enabled\":false},\"properties\":{\"postalCode\":{\"type\":\"text\",\"copy_to\":\"geo_all\"},\"via_type\":{\"type\":\"text\",\"analyzer\":\"no_accents\",\"copy_to\":\"geo_all\"},\"via_name\":{\"type\":\"text\",\"analyzer\":\"no_accents\",\"copy_to\":\"geo_all\"},\"number\":{\"type\":\"text\",\"copy_to\":\"geo_all\"},\"town\":{\"type\":\"text\",\"analyzer\":\"no_accents\",\"copy_to\":\"geo_all\"},\"province\":{\"type\":\"text\"},\"state\":{\"type\":\"text\"},\"country\":{\"type\":\"text\"},\"location\":{\"type\":\"geo_point\"},\"geo_all\":{\"type\":\"text\",\"analyzer\":\"no_accents\"}}}}}";


        prepareCreate(openaddress_index).setSource(indexLocationSource, XContentType.JSON ).get();

        prepareCreate( find_index ).setSource(indexLocationSource, XContentType.JSON ).get();

        indexAtmSource = "{\"settings\":{\"index\":{\"number_of_shards\":\"2\",\"number_of_replicas\":\"1\"}},\"mappings\":{\"geolocation\":{\"properties\":{\"location\":{\"type\":\"geo_point\"},\"id\":{\"type\":\"string\"},\"red4B\":{\"type\":\"boolean\"},\"red6000\":{\"type\":\"boolean\"},\"servired\":{\"type\":\"boolean\"},\"entityCode\":{\"type\":\"string\"},\"subcategory\":{\"type\":\"keyword\"},\"branchCode\":{\"type\":\"string\"},\"branch\":{\"type\":\"string\"},\"atmCode\":{\"type\":\"string\"},\"ordinal\":{\"type\":\"string\"},\"province\":{\"type\":\"string\"},\"state\":{\"type\":\"string\"},\"postalCode\":{\"type\":\"string\"},\"town\":{\"type\":\"string\"},\"viaType\":{\"type\":\"string\"},\"viaName\":{\"type\":\"string\"},\"number\":{\"type\":\"string\"},\"rdo\":{\"type\":\"string\"},\"success\":{\"type\":\"integer\"},\"source\":{\"type\":\"keyword\"}}}}}";

        prepareCreate( atm_index ).setSource(indexAtmSource, XContentType.JSON ).get();

        prepareCreate( delete_index ).setSource(indexAtmSource, XContentType.JSON ).get();


        insertData( atm_index, mapping_geo, generateAtm() );

        insertData( find_index, mapping_geo, locations );

        aditionalInit();

    }

    protected  void aditionalInit() throws Exception {

    }

    protected List<AtmRequest> generateAtm() {

        AtmRequest atm1 = new AtmRequest( new GeoPoint( "-3.75681111011152", "40.368287486990695" ), "0001", "red4b",
                "red6000", "servired", "entityCode",
                "BANKIA", "branchCode", "Sucursal Bankia", "atmCode", "ordinal", "province",
                "state", "postalCode", "town", "viaType", "viaName", "number", "rdo", 1, "carto" );

        AtmRequest atm2 = new AtmRequest( new GeoPoint( "-30.333", "13.4" +
                "5" ), "0002", "red4b", "red6000", "servired", "entityCode",
                "CAJA RURAL DE GIJON", "branchCode", "branch", "atmCode", "ordinal", "province",
                "state", "postalCode", "town", "viaType", "viaName", "number", "rdo", 1, "carto" );

        AtmRequest atm3 = new AtmRequest( new GeoPoint( "-31.333", "16.4" +
                "5" ), "0003", "red4b", "red6000", "servired", "entityCode",
                "CAM", "branchCode", "branch", "atmCode", "ordinal", "province",
                "state", "postalCode", "town", "viaType", "viaName", "number", "rdo", 1, "carto" );

        return Arrays.asList( atm1, atm2, atm3 );
    }

    protected void insertData(String indexFind, String mapping, List<? extends ESRequest> esObjects) {

        esObjects.forEach( location -> {
            IndexRequest indexRequest = new IndexRequest( indexFind, mapping,
                    UUID.randomUUID().toString() );
            try {
                indexRequest.source( mapper.writeValueAsString( location ), XContentType.JSON );
            } catch (Exception e) {
                e.printStackTrace();
            }
            client().index( indexRequest ).actionGet();
        } );
        flushAndRefresh( indexFind );
    }

    @Override
    protected Collection<Class<? extends Plugin>> nodePlugins() {
        return Arrays.asList(Netty4Plugin.class);
    }

    @Override
    protected Settings nodeSettings(int nodeOrdinal) {
        int randomPort = randomIntBetween(49152, 65525);

        Settings.Builder builder = Settings.builder()
                .put(super.nodeSettings(nodeOrdinal))
                .put(NetworkModule.HTTP_ENABLED.getKey(), true)
                .put(HttpTransportSettings.SETTING_HTTP_PORT.getKey(), randomPort);
        Settings settings = builder.build();
        return settings;
    }

    protected void printResponse(Response response) throws Exception {
        System.out.println(EntityUtils.toString(response.getEntity()));
    }

    protected void createSimpleData(String indexFind, String mapping, ESRequest request) throws Exception {
        IndexRequest indexRequest = new IndexRequest(indexFind, mapping, request.getIndexId());
        indexRequest.source(mapper.writeValueAsString(request), XContentType.JSON);
        client().index(indexRequest).actionGet();
        refresh(indexFind);
    }

    protected  <T> T extractLocationResponse(Response response, Class<T> clazz) throws IOException {

        JsonNode root = mapper.readTree(response.getEntity().getContent());

        final JsonNode locationNode = root.path("_source").path("location");

        return mapper.readValue(locationNode.toString(), clazz);

    }
}
