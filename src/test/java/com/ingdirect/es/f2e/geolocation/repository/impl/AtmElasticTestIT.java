package com.ingdirect.es.f2e.geolocation.repository.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingdirect.es.f2e.geolocation.bean.geo.AtmRequest;
import com.ingdirect.es.f2e.geolocation.bean.geo.AtmRequestBuilder2;
import com.ingdirect.es.f2e.geolocation.bean.geo.ErrorESResponse;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoPoint;
import com.ingdirect.es.f2e.geolocation.client.impl.GeoElasticSearchClientImpl;
import com.ingdirect.es.f2e.geolocation.it.repository.impl.BaseRestElasticSearchTestIT;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.test.ESIntegTestCase;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;


@ESIntegTestCase.ClusterScope(scope = ESIntegTestCase.Scope.SUITE, numDataNodes = 1, numClientNodes = 0, transportClientRatio = 0, supportsDedicatedMasters = false)
public class AtmElasticTestIT extends BaseRestElasticSearchTestIT {

    private ElasticSearchRestDaoImpl repository;

    private GeoElasticSearchClientImpl geoClient;

    private String indexJsonSource;

    private final String point_create_index = "create_point_index";

    private final String point_insert_index = "point_insert_index";

    private final String mapping_geo = "geolocation";

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public void aditionalInit() throws Exception {

        geoClient = new GeoElasticSearchClientImpl(client);

        repository = new ElasticSearchRestDaoImpl(geoClient, mapper);

        //indexJsonSource = F2EApplicationProperties.getProperty( "environment.atmgeolocation.elasticIndex" );

        indexJsonSource = "{\"settings\":{\"index\":{\"number_of_shards\":\"2\",\"number_of_replicas\":\"1\"},\"analysis\":{\"char_filter\":{\"no_commas\":{\"type\":\"mapping\",\"mappings\":[\", => \"]}},\"analyzer\":{\"no_accents\":{\"tokenizer\":\"whitespace\",\"char_filter\":[\"no_commas\"],\"filter\":[\"standard\",\"asciifolding\",\"lowercase\",\"synonym\"]}},\"filter\":{\"synonym\":{\"type\":\"synonym\",\"synonyms\":[\"ODONELL, O'DONELL, O DONELL, O DONNELL\",\"NUESTRA, NTRA, NTRA,\",\"SENORA, SE�ORA, SRA, SRA.\",\"INGENIERO, ING\",\"AVDA, AVENIDA, Av., AVDA., AVD., AVD, AVINGUDA, ETORBIDEA, AV, AVGDA\",\"URB, URB., URBANIZACION, URBANIZA, URBANITZ\",\"c/, CALLE, CL, RUA, c-, C, CARRER, KALEA, RU, CARRE\",\"PZA., PZA, PLAZA, PLACA, PL, PZ, PRAZA, PZ, ENPARANTZA\",\"DR, DOCTOR, DR.\",\"FCO, FCO., FRANCISCO\",\"RODRIGUEZ, RO, RODRIG, RGUEZ\",\"FERNANDEZ, FDEZ\",\"ARQUITECTO, ARQTO, ARQUITECTE\",\"SANTA, STA, STA.\",\"ARCHIDUQUE, ARX, ARXIDUC\",\"MADRE, MARE\",\"DIOS, DEU\",\"PDA, PUJADA\",\"VIRGEN, VIRXEN, MARE DE DEU, MADRE DE DIOS\",\"CAPITAN, CAP\",\"JUAN, JOAN, XOAN, XOAM\",\"GRAL, GENERAL\",\"PASEO, PASSEIG, PG, PS, IBILTOKIA\",\"CAMINO, CMNO, BIDEA, CAMI, CAMIN, CM\",\"GRAN VIA, G.V., GV\",\"HERMANO, GERMA\",\"SAN, S, S., SANT\",\"PASAJE, PSAJE, PJ, PASSATGE\",\"CARRETERA, CTRA, CARRETER, CR\",\"RAMBLA, RBLA, RB\",\"FRAY, FREI\",\"TRAVESIA, TRVA\",\"POLIGONO, POLIG\",\"GLORIETA, GTA\"],\"ignore_case\":true}}}},\"mappings\":{\"geolocation\":{\"_all\":{\"enabled\":false},\"properties\":{\"postalCode\":{\"type\":\"text\",\"copy_to\":\"geo_all\"},\"via_type\":{\"type\":\"text\",\"analyzer\":\"no_accents\",\"copy_to\":\"geo_all\"},\"via_name\":{\"type\":\"text\",\"analyzer\":\"no_accents\",\"copy_to\":\"geo_all\"},\"number\":{\"type\":\"text\",\"copy_to\":\"geo_all\"},\"town\":{\"type\":\"text\",\"analyzer\":\"no_accents\",\"copy_to\":\"geo_all\"},\"province\":{\"type\":\"text\"},\"state\":{\"type\":\"text\"},\"country\":{\"type\":\"text\"},\"location\":{\"type\":\"geo_point\"},\"geo_all\":{\"type\":\"text\",\"analyzer\":\"no_accents\"}}}}}";

        prepareCreate(point_insert_index).setSource(indexJsonSource, XContentType.JSON).get();


    }

    @Test
    public void shouldCreateIndex() throws Exception {

        boolean ret = repository.createIndex(point_create_index, indexJsonSource);

        assertThat(ret, is(true));

    }

    @Test
    public void shouldInsertListAtmRequestInES() throws Exception {

        List<AtmRequest> points = buildAtmList();
        List<ErrorESResponse> errors = repository.insertLocations(point_insert_index, mapping_geo, points);

        assertThat(errors, hasSize(0));

    }

    private List<AtmRequest> buildAtmList() {
        GeoPoint location = new GeoPoint("-3.7684460", "40.363556");
        AtmRequest atm = new AtmRequestBuilder2().setLocation(location).setId("15306").setRed4b("0").setRed6000("0").setServired("1").setEntityCode("2100").setEntity("CAIXABANK").setBranchCode("0106").setBranch("CASTELLAR DEL VALLES").setAtmCode("17169").setOrdinal("1").setProvince("BARCELONA").setState("CASTELLAR DEL VALL�S").setPostalCode("08211").setTown("CASTELLAR DEL VALL�S").setViaType("Carrer").setViaName("Major").setNumber("57").setRdo("59").setSuccess(1).setSource("carto").createAtmRequest();

        GeoPoint location2 = new GeoPoint("-3.7684460", "40.363556");
        AtmRequest atm2 = new AtmRequestBuilder2().setLocation(location2).setId("15271").setRed4b("0").setRed6000("0").setServired("1").setEntityCode("2100").setEntity("CAIXABANK").setBranchCode("1082").setBranch("ESPRONCEDA").setAtmCode("1698").setOrdinal("2").setProvince("BARCELONA").setState("SABADELL").setPostalCode("08204").setTown("SABADELL").setViaType("Carrer").setViaName("Aribau").setNumber("20").setRdo(null).setSuccess(1).setSource("carto").createAtmRequest();

        return Arrays.asList(atm, atm2);
    }


}
