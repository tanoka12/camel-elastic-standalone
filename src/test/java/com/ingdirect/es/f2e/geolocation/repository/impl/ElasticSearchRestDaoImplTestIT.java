package com.ingdirect.es.f2e.geolocation.repository.impl;

import com.ingdirect.es.f2e.geolocation.bean.geo.*;
import com.ingdirect.es.f2e.geolocation.client.impl.GeoElasticSearchClientImpl;
import com.ingdirect.es.f2e.geolocation.it.repository.impl.BaseRestElasticSearchTestIT;
import com.ingdirect.es.f2e.geolocation.it.repository.impl.PocBaseRestElasticSearchTestIT;
import com.ingdirect.es.f2e.geolocation.repository.impl.ElasticSearchRestDaoImpl;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.test.ESIntegTestCase;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;

@ESIntegTestCase.ClusterScope(scope = ESIntegTestCase.Scope.SUITE, numDataNodes = 1, numClientNodes = 0, transportClientRatio = 0, supportsDedicatedMasters = false)
public class ElasticSearchRestDaoImplTestIT extends
        BaseRestElasticSearchTestIT {

    private ElasticSearchRestDaoImpl repository;

    private GeoElasticSearchClientImpl geoClient;

    private String indexCustomerJsonSource;

    private String indexLocationSource;

    protected final String mapping_geo = "geolocation";

    private final String point_insert_index = "point_insert_index";

    protected final String openaddress_index = "geo-openaddress-20170407-100819";

    @Override
    public void aditionalInit() throws Exception {

        geoClient = new GeoElasticSearchClientImpl(client);

        repository = new ElasticSearchRestDaoImpl(geoClient, mapper);

        //indexCustomerJsonSource = F2EApplicationProperties.getProperty( "environment.customer.elasticIndex" );
        indexCustomerJsonSource = "{\"settings\":{\"index\":{\"number_of_shards\":\"2\",\"number_of_replicas\":\"1\"}},\"mappings\":{\"geolocation\":{\"properties\":{\"id\":{\"type\":\"string\"},\"name\":{\"type\":\"string\"},\"surName1\":{\"type\":\"string\"},\"surName2\":{\"type\":\"string\"},\"viaType\":{\"type\":\"string\"},\"viaName\":{\"type\":\"string\"},\"number\":{\"type\":\"string\"},\"floor\":{\"type\":\"string\"},\"additionalInfo\":{\"type\":\"string\"},\"city\":{\"type\":\"string\"},\"province\":{\"type\":\"string\"},\"country\":{\"type\":\"string\"},\"postalCode\":{\"type\":\"string\"},\"success\":{\"type\":\"integer\"},\"source\":{\"type\":\"keyword\"},\"location\":{\"type\":\"geo_point\"}}}}}";

        indexLocationSource = "{\"settings\":{\"index\":{\"number_of_shards\":\"2\",\"number_of_replicas\":\"1\"},\"analysis\":{\"char_filter\":{\"no_commas\":{\"type\":\"mapping\",\"mappings\":[\", => \"]}},\"analyzer\":{\"no_accents\":{\"tokenizer\":\"whitespace\",\"char_filter\":[\"no_commas\"],\"filter\":[\"standard\",\"asciifolding\",\"lowercase\",\"synonym\"]}},\"filter\":{\"synonym\":{\"type\":\"synonym\",\"synonyms\":[\"ODONELL, O'DONELL, O DONELL, O DONNELL\",\"NUESTRA, NTRA, NTRA,\",\"SENORA, SE�ORA, SRA, SRA.\",\"INGENIERO, ING\",\"AVDA, AVENIDA, Av., AVDA., AVD., AVD, AVINGUDA, ETORBIDEA, AV, AVGDA\",\"URB, URB., URBANIZACION, URBANIZA, URBANITZ\",\"c/, CALLE, CL, RUA, c-, C, CARRER, KALEA, RU, CARRE\",\"PZA., PZA, PLAZA, PLACA, PL, PZ, PRAZA, PZ, ENPARANTZA\",\"DR, DOCTOR, DR.\",\"FCO, FCO., FRANCISCO\",\"RODRIGUEZ, RO, RODRIG, RGUEZ\",\"FERNANDEZ, FDEZ\",\"ARQUITECTO, ARQTO, ARQUITECTE\",\"SANTA, STA, STA.\",\"ARCHIDUQUE, ARX, ARXIDUC\",\"MADRE, MARE\",\"DIOS, DEU\",\"PDA, PUJADA\",\"VIRGEN, VIRXEN, MARE DE DEU, MADRE DE DIOS\",\"CAPITAN, CAP\",\"JUAN, JOAN, XOAN, XOAM\",\"GRAL, GENERAL\",\"PASEO, PASSEIG, PG, PS, IBILTOKIA\",\"CAMINO, CMNO, BIDEA, CAMI, CAMIN, CM\",\"GRAN VIA, G.V., GV\",\"HERMANO, GERMA\",\"SAN, S, S., SANT\",\"PASAJE, PSAJE, PJ, PASSATGE\",\"CARRETERA, CTRA, CARRETER, CR\",\"RAMBLA, RBLA, RB\",\"FRAY, FREI\",\"TRAVESIA, TRVA\",\"POLIGONO, POLIG\",\"GLORIETA, GTA\"],\"ignore_case\":true}}}},\"mappings\":{\"geolocation\":{\"_all\":{\"enabled\":false},\"properties\":{\"postalCode\":{\"type\":\"text\",\"copy_to\":\"geo_all\"},\"via_type\":{\"type\":\"text\",\"analyzer\":\"no_accents\",\"copy_to\":\"geo_all\"},\"via_name\":{\"type\":\"text\",\"analyzer\":\"no_accents\",\"copy_to\":\"geo_all\"},\"number\":{\"type\":\"text\",\"copy_to\":\"geo_all\"},\"town\":{\"type\":\"text\",\"analyzer\":\"no_accents\",\"copy_to\":\"geo_all\"},\"province\":{\"type\":\"text\"},\"state\":{\"type\":\"text\"},\"country\":{\"type\":\"text\"},\"location\":{\"type\":\"geo_point\"},\"geo_all\":{\"type\":\"text\",\"analyzer\":\"no_accents\"}}}}}";



        prepareCreate( point_insert_index ).setSource(indexCustomerJsonSource, XContentType.JSON).get();

        createDataFindBufferTest(point_insert_index, mapping_geo);

        prepareCreate(openaddress_index).setSource(indexLocationSource, XContentType.JSON ).get();

    }

    @Test
    public void shouldFindObjectById() throws Exception {

        GeoPoint expectedGeoPoint = new GeoPoint("-45.7685", "20.36888");

        GeoLocationByIdRequest request = new GeoLocationByIdRequest("1234", expectedGeoPoint);

        createSimpleDataWithId(openaddress_index, mapping_geo,request);

        final GetResponse response = repository.findObjectById(openaddress_index, mapping_geo, "1234");

        final boolean exists = response.isExists();

        assertThat(exists, equalTo(true));


        Map<String, Object> sourceAsMap = response.getSourceAsMap();

        final String id = (String) sourceAsMap.get("id");

        assertThat(id, equalTo("1234"));

        final Map<String, Object> location = (Map<String, Object>) sourceAsMap.get("location");

        assertThat(location.get("lon"), equalTo("-45.7685"));
        assertThat(location.get("lat"), equalTo("20.36888"));
    }


    @Test
    public void shouldGetResultWithScroll() throws Exception {
        final List<CustomerRequest> customers = repository.getAllDocuments(point_insert_index, CustomerRequest.class);
        assertThat( customers, hasSize( 2 ) );


    }

    @Test
    public void shouldInsertWithBulk() throws Exception {

        List<AtmRequest> points = buildAtmList();

        List<ErrorESResponse> errors = repository.insertLocations( point_insert_index, mapping_geo, points );

        assertThat( errors, hasSize( 0 ) );

    }

    private List<AtmRequest> buildAtmList() {
        GeoPoint location = new GeoPoint( "-3.7684460", "40.363556" );
        AtmRequest atm = new AtmRequestBuilder2().setLocation(location).setId("15301").setRed4b("0").setRed6000("0").setServired("1").setEntityCode("2100").setEntity("CAIXABANK").setBranchCode("0106").setBranch("CASTELLAR DEL VALLES").setAtmCode("17169").setOrdinal("1").setProvince("BARCELONA").setState("CASTELLAR DEL VALL�S").setPostalCode("08211").setTown("CASTELLAR DEL VALL�S").setViaType("Carrer").setViaName("Major").setNumber("57").setRdo("59").setSuccess(1).setSource("carto").createAtmRequest();

        GeoPoint location2 = new GeoPoint( "-3.7684460", "40.363556" );
        AtmRequest atm2 = new AtmRequestBuilder2().setLocation(location2).setId("15271").setRed4b("0").setRed6000("0").setServired("1").setEntityCode("2100").setEntity("CAIXABANK").setBranchCode("1082").setBranch("ESPRONCEDA").setAtmCode("1698").setOrdinal("2").setProvince("BARCELONA").setState("SABADELL").setPostalCode("08204").setTown("SABADELL").setViaType("Carrer").setViaName("Aribau").setNumber("20").setRdo(null).setSuccess(1).setSource("carto").createAtmRequest();

        return Arrays.asList( atm, atm2 );
    }


    private void createDataFindBufferTest(String indexFind, String mapping) {

        GeoPoint location = new GeoPoint( "-3.7684460", "40.363556" );
        CustomerRequest customer1 = new CustomerRequest( "1", "Natalia", "Roales", "GOnzalez", "Calle", "Moraleja",
                "3", "1", "additionalInfo", "Alcobendas", "Madrid", "Espa�a",
                "28080", 1, "CARTO", location );

        GeoPoint location2 = new GeoPoint( "-3.7684460", "40.363556" );
        CustomerRequest customer2 = new CustomerRequest( "2", "Jorge", "Pacheco", "Mengual", "Avenida", "Peseta",
                "33", "1", "additionalInfo2", "Tarancon", "Cuenca", "Spain",
                "03013", 1, "CARTO", location2 );

        List<CustomerRequest> insertLocations = new ArrayList<>();
        insertLocations.add( customer1 );
        insertLocations.add( customer2 );

        insertData(indexFind,mapping,insertLocations);

    }


}