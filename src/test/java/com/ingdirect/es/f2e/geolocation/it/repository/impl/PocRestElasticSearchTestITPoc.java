package com.ingdirect.es.f2e.geolocation.it.repository.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.ingdirect.es.f2e.geolocation.bean.geo.*;
import com.ingdirect.es.f2e.geolocation.builder.GeoRequestBuilder;
import com.ingdirect.es.f2e.geolocation.builder.GeoResponseBuilder;
import com.ingdirect.es.f2e.geolocation.builder.PoiSubCategoryResponseBuilder;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Response;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.test.ESIntegTestCase;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

/**
 * Created by EH80OB on 17/04/2017.
 */

@ESIntegTestCase.ClusterScope(scope = ESIntegTestCase.Scope.SUITE, numDataNodes = 1, numClientNodes = 0, transportClientRatio = 0, supportsDedicatedMasters = false)
public class PocRestElasticSearchTestITPoc extends PocBaseRestElasticSearchTestIT {


    @BeforeClass
    public static void setUpClass() throws Exception {
        prepareF2EApplicationProperties();
    }

    @Test
    public void shouldGetResultWithScroll() throws Exception {
        // Given

        // When
        final List<PoiSubCategoryResponse> allDocuments = getAllDocuments(atm_index, mapping_geo, PoiSubCategoryResponse.class);
        // Then
        assertThat(allDocuments, Matchers.hasSize(3));

    }


    public <T> List<T> getAllDocuments(String index, String mapping, Class<T> clazz) throws Exception {

        List<T> result = new ArrayList<>();

        Map<String, String> paramMap = new HashMap<>();

        paramMap.put("scroll", "1m");

        HttpEntity initialBody = generateInitialBody("2");

        Response response = client.performRequest("POST", "/" + index + "/" + mapping + "/_search",
                paramMap, initialBody);


        JsonNode root = mapper.readTree(response.getEntity().getContent());

        final String scrollId = root.path("_scroll_id").asText();

        final JsonNode resultNode = root.path("hits").path("hits");

        int size;

        final List<PoiSubCategoryResponse> responses = extractResponse(resultNode);

        result.addAll((Collection<? extends T>) responses);

        do {
            final HttpEntity scrollBody = generateScrollBody(scrollId);
            Response partialResponse = client.performRequest("POST", "/_search/scroll",
                    new HashMap<>(), scrollBody);
            JsonNode resultPartialNode = mapper.readTree(partialResponse.getEntity().getContent()).path("hits").path("hits");

            size = resultPartialNode.size();

            result.addAll((Collection<? extends T>) extractResponse(resultPartialNode));

        } while (size != 0);

        return result;
    }

    private HttpEntity generateInitialBody(String size) throws Exception {
        XContentBuilder builder = jsonBuilder()
                .startObject()
                .field("size", size)
                .endObject();

        return new NStringEntity(builder.string(), ContentType.APPLICATION_JSON);

    }

    private HttpEntity generateScrollBody(String scrollId) throws Exception {
        XContentBuilder builder = jsonBuilder()
                .startObject()
                .field("scroll", "1m")
                .field("scroll_id", scrollId)
                .endObject();

        return new NStringEntity(builder.string(), ContentType.APPLICATION_JSON);

    }

    @Test
    public void findLocationByIdShouldReturnGeoPointCorrectly() throws Exception {

        String id = "1234";

        String longitude = "-45.7685";

        String latitude = "20.36888";

        GeoPoint expectedGeoPoint = new GeoPoint(longitude, latitude);

        GeoLocationByIdRequest request = new GeoLocationByIdRequest(id, expectedGeoPoint);

        createSimpleData(openaddress_index, mapping_geo, request);

        Response response = client.performRequest("GET", "/" + openaddress_index + "/" + mapping_geo + "/" + id,
                new Hashtable<>());

        final GeoPoint geoPoint = extractLocationResponse(response, GeoPoint.class);

        assertEquals(expectedGeoPoint, geoPoint);

    }

    @Test
    public void shouldInsertBulkLocations() throws Exception {

        // Given
        GeoRequest location1 = new GeoRequestBuilder().latitude("40.363556").longitude("-3.7684460")
                .province("Madrid")
                .number("53").postalcode("28039").viaName("Valsesangil").viaType("Calle").build();

        GeoRequest location2 = new GeoRequestBuilder().latitude("41.363556").longitude("-4.7684460")
                .province("Madrid")
                .number("22").postalcode("28035").viaName("Antonio Machado").viaType("Av").build();

        final List<GeoRequest> locations = Arrays.asList(location1, location2);

        final HttpEntity entity = generateBulkRequest(openaddress_index, mapping_geo, locations);

        Response response = client.performRequest("POST", "/_bulk",
                new Hashtable<>(), entity);

        final List<ErrorESResponse> errorESResponses = extractErrorResponse(response.getEntity());

        assertThat(errorESResponses, Matchers.hasSize(0));

    }

    private List<ErrorESResponse> extractErrorResponse(HttpEntity entity) throws Exception {

        JsonNode root = mapper.readTree(entity.getContent());

        final boolean hasErrors = root.path("errors").asBoolean();

        if (hasErrors) {
            final Iterator<JsonNode> iterator = root.path("items").iterator();

            final List<ErrorESResponse> errorESResponses = StreamSupport
                    .stream(((Iterable<JsonNode>) () -> iterator).spliterator(), false)
                    .map(jsonNode -> new ErrorESResponse(jsonNode.path("index").path("_index").asText(),
                            jsonNode.path("index").path("error").path("reason").asText()))
                    .collect(Collectors.toList());

            return errorESResponses;

        }
        return Collections.emptyList();

    }

    private HttpEntity generateBulkRequest(String index, String mapping, List<GeoRequest> locations) throws Exception {
        StringBuilder bulkRequestBody = new StringBuilder();

        locations.stream().forEach(geoRequest -> {
            try {
                bulkRequestBody.append(buildBulkOperation(index, mapping, geoRequest.getIndexId()));
                bulkRequestBody.append("\n");
                bulkRequestBody.append(mapper.writeValueAsString(geoRequest));
                //bulkRequestBody.append( geoRequest );
                bulkRequestBody.append("\n");
            } catch (Exception e) {
                e.printStackTrace();
            }

        });
        System.out.println(bulkRequestBody.toString());

        return new NStringEntity(bulkRequestBody.toString(), ContentType.APPLICATION_JSON);

    }

    private String buildBulkOperation(String index, String mapping, String id) throws Exception {

        XContentBuilder builder = jsonBuilder()
                .startObject()
                .startObject("index")
                .field("_index", index)
                .field("_type", mapping)
                .field("_id", id)
                .endObject()
                .endObject();
        return builder.string();
    }

    @Test
    public void shouldReturnOneLocation() throws Exception {
        // Given

        HttpEntity entity = generateQueryLocation();

        final Response response = client.performRequest("GET",
                "/" + find_index + "/geolocation/_search",
                new Hashtable<>(),
                entity);
        //System.out.println( EntityUtils.toString( response.getEntity() ) );
        List<GeoResponse> locations = extractLocationsResponse(response);

        assertThat(locations, hasSize(1));

    }



    private HttpEntity generateQueryLocation() throws Exception {
        XContentBuilder builder = jsonBuilder()
                .startObject()
                .field("size", "100")
                .startObject("query")
                .startObject("match")
                .startObject("geo_all")
                .field("query", "Calle, Pinar de Sanjose, 3, 28098")
                .field("operator", "AND")
                .field("fuzziness", "2")
                .field("prefix_length", 0)
                .field("max_expansions", 50)
                .field("fuzzy_transpositions", true)
                .field("lenient", false)
                .field("zero_terms_query", "NONE")
                .field("boost", 1.0)
                .endObject()
                .endObject()
                .endObject()
                .endObject();

        return new NStringEntity(builder.string(), ContentType.APPLICATION_JSON);
    }


    @Test
    public void shouldCreateIndexWithRestClient() throws Exception {
        // Given
        HttpEntity entity = new StringEntity(indexLocationSource, ContentType.APPLICATION_JSON);
        Response response = client.performRequest("PUT", "/" + new_index,
                new Hashtable<>(), entity);

        // When
        int statusCode = response.getStatusLine().getStatusCode();
        // Then

        assertThat(statusCode, equalTo(200));

    }

    @Test
    public void shouldGetMaxIndexByPrefix() throws Exception {

        prepareCreate("openaddress-20170121-121658").get();
        prepareCreate("openaddress-20170122-121702").get();
        prepareCreate("openaddress-20170123-122147").get();

        String preffixIndex = "openaddress-*";

        final Response response = client.performRequest("GET", "/" + preffixIndex + "/_settings",
                new HashMap<>());

        final String recentIndexByCreationTime = getRecentIndexByCreationTime(response);

        assertThat(recentIndexByCreationTime, equalTo("openaddress-20170123-122147"));

    }

    private String getRecentIndexByCreationTime(Response response) throws IOException {

        JsonNode root = mapper.readTree(EntityUtils.toString(response.getEntity()));

        final Iterator<JsonNode> iteratorRoot = root.iterator();

        final Optional<JsonNode> max = StreamSupport
                .stream(((Iterable<JsonNode>) () -> iteratorRoot).spliterator(), false)
                .max(Comparator.comparing(
                        jsonNode -> jsonNode.path("settings").path("index").path("creation_date").asLong()));

        if (max.isPresent()) {
            return max.get().path("settings").path("index").path("provided_name").asText();

        } else {
            return "";
        }
    }

    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    static void prepareF2EApplicationProperties() {
        final File configTestFile = new File(PocRestElasticSearchTestITPoc.class.getClassLoader()
                .getResource("environment.conf")
                .getPath());

    }



    private List<GeoResponse> extractLocationsResponse(Response response) throws IOException {

        JsonNode root = mapper.readTree(response.getEntity().getContent());
        final Iterator<JsonNode> iterator = root.path("hits").path("hits").iterator();

        final List<GeoResponse> collect = StreamSupport
                .stream(((Iterable<JsonNode>) () -> iterator).spliterator(), false)
                .map(hitNode -> {
                    try {
                        String index = hitNode.path("_index").asText();
                        Map<String, Object> result = mapper.convertValue(hitNode.path("_source"), Map.class);
                        return new GeoResponseBuilder(result).source(index).build();
                    } catch (Exception e) {
                        throw new RuntimeException();
                    }
                }).collect(Collectors.toList());

        return collect;
    }




    private List<PoiSubCategoryResponse> extractResponse(JsonNode result) throws IOException {

        final Iterator<JsonNode> iterator = result.iterator();

        return StreamSupport
                .stream(((Iterable<JsonNode>) () -> iterator).spliterator(), false)
                .map(hitNode -> {
                    try {

                        String index = hitNode.path("_index").asText();
                        Map<String, Object> resultMap = mapper.convertValue(hitNode.path("_source"), Map.class);
                        return new PoiSubCategoryResponseBuilder(index, resultMap)
                                .setId("ss").build();

                    } catch (Exception e) {
                        throw new RuntimeException();
                    }
                }).collect(Collectors.toList());
    }





}