package com.ingdirect.es.f2e.geolocation.it.repository.impl;

import com.ingdirect.es.f2e.geolocation.bean.geo.GeoLocationByIdRequest;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoPoint;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.ResponseException;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.test.ESIntegTestCase;
import org.junit.Test;

import java.util.Hashtable;
import java.util.Map;

import static org.hamcrest.Matchers.equalTo;

/**
 * Created by EH80OB on 17/04/2017.
 */

@ESIntegTestCase.ClusterScope(scope = ESIntegTestCase.Scope.SUITE, numDataNodes = 1, numClientNodes = 0, transportClientRatio = 0, supportsDedicatedMasters = false)
public class PocHighRestElasticSearchTestITPoc extends PocBaseRestElasticSearchTestIT {

    private RestHighLevelClient clientHigh;

    @Override
    public void aditionalInit() throws Exception {
        clientHigh = new RestHighLevelClient(client);
    }

    @Test
    public void shouldCreateIndexWithRestClient() throws Exception {

        // Given
        HttpEntity entity = new StringEntity(indexLocationSource, ContentType.APPLICATION_JSON);
        Response response = client.performRequest("PUT", "/" + new_index,
                new Hashtable<>(), entity);

        // When
        int statusCode = response.getStatusLine().getStatusCode();
        // Then

        assertThat(statusCode, equalTo(200));


    }

    @Test
    public void shouldExistsIndex() throws Exception {
        Response response = client.performRequest("GET", "/" + atm_index);
        int statusCode = response.getStatusLine().getStatusCode();
        assertThat(statusCode, equalTo(200));
    }

    @Test
    public void shouldNotExistsIndex() throws Exception {
        try {
            Response response = client.performRequest("GET", "/noExiste");
            int statusCode = response.getStatusLine().getStatusCode();
            assertThat(statusCode, equalTo(200));
        } catch (ResponseException e) {
            int statusCode = e.getResponse().getStatusLine().getStatusCode();
            assertThat(statusCode, equalTo(404));
        }
    }

    @Test
    public void shouldDeleteIndex() throws Exception {

        Response response = client.performRequest("DELETE", "/" + delete_index);
        int statusCode = response.getStatusLine().getStatusCode();
        assertThat(statusCode, equalTo(200));
    }


    @Test
    public void shouldRefreshIndex() throws Exception {

        Response response = client.performRequest("POST", "/" + atm_index + "/_refresh");
        int statusCode = response.getStatusLine().getStatusCode();
        assertThat(statusCode, equalTo(200));
    }


    @Test
    public void findLocationByIdShouldReturnGeoPointCorrectly() throws Exception {
        GeoPoint expectedGeoPoint = new GeoPoint("-45.7685", "20.36888");
        GeoLocationByIdRequest request = new GeoLocationByIdRequest("1234", expectedGeoPoint);
        createSimpleData(openaddress_index, mapping_geo, request);

        GetRequest getRequest = new GetRequest(openaddress_index, mapping_geo, "1234");

        GetResponse getResponse = clientHigh.get(getRequest);

        String index = getResponse.getIndex();
        String type = getResponse.getType();
        String id = getResponse.getId();
        if (getResponse.isExists()) {
            long version = getResponse.getVersion();
            String sourceAsString = getResponse.getSourceAsString();
            Map<String, Object> sourceAsMap = getResponse.getSourceAsMap();
            byte[] sourceAsBytes = getResponse.getSourceAsBytes();
        } else {

        }


    }
}