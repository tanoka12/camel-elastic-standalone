package com.ingdirect.es.f2e.geolocation.repository.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ingdirect.es.f2e.geolocation.bean.geo.CustomerRequest;
import com.ingdirect.es.f2e.geolocation.bean.geo.ErrorESResponse;
import com.ingdirect.es.f2e.geolocation.bean.geo.GeoPoint;
import com.ingdirect.es.f2e.geolocation.bean.geo.PointInterestRequest;
import com.ingdirect.es.f2e.geolocation.client.impl.GeoElasticSearchClientImpl;
import com.ingdirect.es.f2e.geolocation.it.repository.impl.BaseRestElasticSearchTestIT;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.test.ESIntegTestCase;
import org.junit.After;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;


@ESIntegTestCase.ClusterScope(scope = ESIntegTestCase.Scope.SUITE, numDataNodes = 1, numClientNodes = 0, transportClientRatio = 0, supportsDedicatedMasters = false)
public class CustomerTestIT extends BaseRestElasticSearchTestIT {

    private ElasticSearchRestDaoImpl repository;

    private GeoElasticSearchClientImpl geoClient;

    private String indexJsonSource;

    private final String point_create_index = "create_point_index";

    private final String point_insert_index = "point_insert_index";

    private final String mapping_geo = "geolocation";

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public void aditionalInit() throws Exception {

        geoClient = new GeoElasticSearchClientImpl(client);

        repository = new ElasticSearchRestDaoImpl(geoClient, mapper);

        //indexJsonSource = F2EApplicationProperties.getProperty( "environment.customer.elasticIndex" );

        indexJsonSource = "{\"settings\":{\"index\":{\"number_of_shards\":\"2\",\"number_of_replicas\":\"1\"}},\"mappings\":{\"geolocation\":{\"properties\":{\"id\":{\"type\":\"string\"},\"name\":{\"type\":\"string\"},\"surName1\":{\"type\":\"string\"},\"surName2\":{\"type\":\"string\"},\"viaType\":{\"type\":\"string\"},\"viaName\":{\"type\":\"string\"},\"number\":{\"type\":\"string\"},\"floor\":{\"type\":\"string\"},\"additionalInfo\":{\"type\":\"string\"},\"city\":{\"type\":\"string\"},\"province\":{\"type\":\"string\"},\"country\":{\"type\":\"string\"},\"postalCode\":{\"type\":\"string\"},\"success\":{\"type\":\"integer\"},\"source\":{\"type\":\"keyword\"},\"location\":{\"type\":\"geo_point\"}}}}}";

        prepareCreate( point_insert_index ).setSource( indexJsonSource, XContentType.JSON).get();

        createDataFindBufferTest(point_insert_index, mapping_geo);


    }


    private void createDataFindBufferTest(String indexFind, String mapping) {

        GeoPoint location = new GeoPoint( "-3.7684460", "40.363556" );
        CustomerRequest customer1 = new CustomerRequest( "1", "Natalia", "Roales", "GOnzalez", "Calle", "Moraleja",
                "3", "1", "additionalInfo", "Alcobendas City", "Madrid", "Espa�a",
                "28080", 1, "CARTO", location );

        GeoPoint location2 = new GeoPoint( "-3.7684460", "40.363556" );
        CustomerRequest customer2 = new CustomerRequest( "2", "Jorge", "Pacheco", "Mengual", "Avenida", "Peseta",
                "33", "1", "additionalInfo2", "Tarancon", "Cuenca", "Spain",
                "03013", 1, "CARTO", location2 );

        List<CustomerRequest> insertLocations = new ArrayList<>();
        insertLocations.add( customer1 );
        insertLocations.add( customer2 );

        insertData(indexFind, mapping,insertLocations);

    }


    @Test
    public void shouldReturnTwoCustomers() throws Exception {

        final List<CustomerRequest> customers = repository.getAllDocuments(point_insert_index, CustomerRequest.class);
        assertThat( customers, hasSize( 2 ) );

    }

    @Test
    public void shouldCreateIndex() throws Exception {

        boolean ret = repository.createIndex( point_create_index, indexJsonSource );

        assertThat( ret, is( true ) );

    }

    @Test
    public void shouldInsertListCustomerInES() throws Exception {

        List<CustomerRequest> points = createPoints();
        List<ErrorESResponse> errors = repository.insertLocations( point_insert_index, mapping_geo, points );

        assertThat( errors, hasSize( 0 ) );

    }

    private List<CustomerRequest> createPoints() {
        GeoPoint location = new GeoPoint( "-3.7684460", "40.363556" );
        CustomerRequest customer1 = new CustomerRequest( "id", "name", "surName1", "surName2", "viaType", "viaName",
                "number", "floor", "additionalInfo", "city", "province", "country",
                "postalCode", 1, "CARTO", location );

        GeoPoint location2 = new GeoPoint( "-3.7684460", "40.363556" );
        CustomerRequest customer2 = new CustomerRequest( "id", "name", "surName1", "surName2", "viaType", "viaName",
                "number", "floor", "additionalInfo", "city", "province", "country",
                "postalCode", 1, "CARTO", location2 );

        return Arrays.asList( customer1, customer2 );
    }



}
