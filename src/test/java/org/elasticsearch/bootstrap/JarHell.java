package org.elasticsearch.bootstrap;

import java.io.IOException;
import java.net.URISyntaxException;

// IMPORTANT: Needed since the -Dtests.jarhell.check has been disabled in ES 5.0
// DO NOT REMOVE

/**
 * Created by EH80OB on 10/07/2017.
 */
public class JarHell {

    public static void checkJarHell() throws IOException, URISyntaxException {
        //IMPORTANT: Needed since the -Dtests.jarhell.check has been disabled in ES 5.0
        //DO NOT REMOVE
    }
}