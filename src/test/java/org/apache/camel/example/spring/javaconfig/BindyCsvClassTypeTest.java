package org.apache.camel.example.spring.javaconfig;


import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.model.dataformat.BindyDataFormat;
import org.apache.camel.model.dataformat.BindyType;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 */
public class BindyCsvClassTypeTest extends CamelTestSupport {

    @Test
    public void testMarshallMessage() throws Exception {
        String expected = "-15.6957802,28.0986426,S/N,CL ADARGOMA,,,,,35480,,43a499c553ef0fb2\r\n";


        MockEndpoint resultEndpoint = getMockEndpoint("mock:in");

        resultEndpoint.expectedBodiesReceived(expected);

        template.sendBody("direct:in", generateOrder());

        resultEndpoint.assertIsSatisfied();
        //assertMockEndpointsSatisfied();
    }

    @Test
    public void testUnmarshallMessage() throws Exception {
        getMockEndpoint("mock:out").expectedMessageCount(1);
        getMockEndpoint("mock:out").message(0).body().isInstanceOf(GeoCsv.class);

        String data = "-15.6957802,28.0986426,S/N,CL ADARGOMA,,,,,35480,,43a499c553ef0fb2\r\n";
        template.sendBody("direct:out", data);

        assertMockEndpointsSatisfied();

        GeoCsv order = getMockEndpoint("mock:out").getReceivedExchanges().get(0).getIn().getBody(GeoCsv.class);
        assertEquals("35480", order.getPostalCode());
    }

    public GeoCsv generateOrder() {

        GeoPoint location = new GeoPoint();
        location.setLat("28.0986426");
        location.setLon("-15.6957802");

        GeoCsv order = new GeoCsv();
        order.setDistrict("");
        //order.setHash("43a499c553ef0fb2");
        order.setId("");
        order.setNumber("S/N");
        order.setPostalCode("35480");
        order.setProvince("");
        order.setLocation(location);
        order.setStreet("CL ADARGOMA");
        order.setUnit("");
        order.setHash("43a499c553ef0fb2");


        GeoPoint calendar = new GeoPoint();
        calendar.setLat("LAT");
        calendar.setLon("LON");

        return order;
    }

    @Override
    protected RouteBuilder createRouteBuilder() throws Exception {
        return new RouteBuilder() {
            @Override
            public void configure() throws Exception {
                BindyDataFormat bindy = new BindyDataFormat();
                bindy.setClassType(GeoCsv.class);
                bindy.setLocale("en");
                bindy.setType(BindyType.Csv);

                from("direct:in")
                        .marshal(bindy)
                        .to("mock:in");

                from("direct:out")
                        .unmarshal().bindy(BindyType.Csv, GeoCsv.class)
                        .to("mock:out");
            }
        };
    }
}
