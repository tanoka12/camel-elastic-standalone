package org.apache.camel.example.spring.javaconfig;

import org.apache.camel.*;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.model.RouteDefinition;
import org.apache.camel.test.spring.CamelSpringDelegatingTestContextLoader;
import org.apache.camel.test.spring.CamelSpringRunner;
import org.apache.camel.test.spring.CamelTestContextBootstrapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.BootstrapWith;
import org.springframework.test.context.ContextConfiguration;

import java.nio.file.Files;
import java.nio.file.Paths;

@RunWith(CamelSpringRunner.class)
@BootstrapWith(CamelTestContextBootstrapper.class)
@DirtiesContext
@ContextConfiguration(classes = {MyApplicationSpring.class}, loader = CamelSpringDelegatingTestContextLoader.class)
public class CartoCiudadRouteSpringTest {

    private static final String fileColonSlashSlash = "file://";

    @EndpointInject(uri = "mock://stream:out")
    protected MockEndpoint streamRoute;

    private String contentFile;

    private String contentFileError;

    private String directory;

    @EndpointInject(uri = "mock://file:target/reports")
    protected MockEndpoint errorRoute;

    @Produce
    protected ProducerTemplate template;

    @Autowired
    protected CamelContext context;

    @Before
    public void setUp() throws Exception {
        directory = "files_to_publish";



    }


    @Test
    public void shouldProcessorAllLines() throws Exception {
        contentFile = new String( Files.readAllBytes( Paths.get( CartoCiudadRouteTest.class.getClassLoader()
                .getResource( "open-street-data.csv" ).toURI() ) ) );
        RouteDefinition route = context.getRouteDefinition( CartoCiudadRouteSpring.SETTINGS_ROUTE_ID_KK );
        route.adviceWith( context, new AdviceWithRouteBuilder() {

            @Override
            public void configure() throws Exception {
                mockEndpointsAndSkip( "stream:out" );
            }
        } );


        streamRoute.setAssertPeriod( 3000 );
        streamRoute.expectedMessageCount( 11 );



        template.sendBodyAndHeader( fileColonSlashSlash + directory, contentFile,
                Exchange.FILE_NAME, "open-street-data.csv" );

        streamRoute.assertIsSatisfied();

    }


    //@Test
    public void shouldGetOneErrorLine() throws Exception {

        contentFileError = new String( Files.readAllBytes( Paths.get( CartoCiudadRouteTest.class.getClassLoader()
                .getResource( "open-street-data-error.csv" ).toURI() ) ) );

        RouteDefinition route2 = context.getRouteDefinition( CartoCiudadRouteSpring.SETTINGS_ERROR_ROUTE_ID );
        route2.adviceWith( context, new AdviceWithRouteBuilder() {

            @Override
            public void configure() throws Exception {
                mockEndpointsAndSkip( "file:target/reports" );
            }
        } );



        errorRoute.setAssertPeriod(3000);
        errorRoute.expectedMessageCount( 1 );

        template.sendBodyAndHeader( fileColonSlashSlash + directory, contentFileError,
                Exchange.FILE_NAME, "open-street-data-error.csv" );

        errorRoute.assertIsSatisfied();
    }
}
// end::example[]