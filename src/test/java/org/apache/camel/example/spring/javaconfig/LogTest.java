package org.apache.camel.example.spring.javaconfig;

import org.apache.camel.*;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.util.ExchangeHelper;
import org.junit.Test;

/**
 * Created by jorgepacheco on 13/1/17.
 */

// tag::example[]
public class LogTest extends CamelTestSupport {

    @EndpointInject(uri = "mock:result")
    protected MockEndpoint resultEndpoint;

    @Produce(uri = "direct:start")
    protected ProducerTemplate template;

    @Override
    public boolean isDumpRouteCoverage() {
        return true;
    }

    @Test
    public void testSendMatchingMessage() throws Exception {
        String expectedBody = "<matched/>";

       // resultEndpoint.expectedBodiesReceived(expectedBody);

        GeoCsv geo = new GeoCsv();
        geo.setStreet("PINAR");


        GeoCsv geo2 = new GeoCsv();
        geo2.setStreet("SAN");



        DefaultExchange kk = new DefaultExchange(context());
        Message in = kk.getIn();
        in.setBody(geo);
        in.setHeader("foo","bar");
        kk.setProperty(Exchange.SPLIT_COMPLETE,true);

        template.send(kk);

        //exchange.setIn("");
       // template.getCamelContext().get

        //template.sendBodyAndHeader(geo, "foo", "bar");

        //template.sendBodyAndHeader(geo2, "foo", "bar");

        //resultEndpoint.assertIsSatisfied();
    }



    @Override
    protected RouteBuilder createRouteBuilder() {
        return new RouteBuilder() {
            public void configure() {
                from("direct:start").filter(header("foo").isEqualTo("bar")).
                        log(LoggingLevel.INFO, "Vamos primo ${body}").to("direct:second").end();

                from("direct:second").log(LoggingLevel.INFO, "Vamos cousin ${body.street}").
                        aggregate(constant(false), new OpenStreetMapAggregationStrategy()).completionSize(2).log("Lista ${body}").
                        end();

            }
        };
    }
}
