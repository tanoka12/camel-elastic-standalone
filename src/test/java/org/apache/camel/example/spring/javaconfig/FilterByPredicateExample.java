package org.apache.camel.example.spring.javaconfig;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultProducerTemplate;

public class FilterByPredicateExample {
    public static void main(String[] args) throws Exception {
        CamelContext camelContext = new DefaultCamelContext();
        try {
            camelContext.addRoutes(new RouteBuilder() {
                public void configure() {
/*
                    from("direct:start").setProperty("kk",constant(true)).choice()
                            .when(property("kk").isEqualTo(true)).log("TARALARI")
                            .otherwise()
                            .log("TARALARO")
                            .end();*/
                    from("direct:start")
                                 .filter(new Predicate() {

                                public boolean matches(Exchange exchange) {
                                    final String body = exchange.getIn().getBody(String.class);
                                    return ((body != null) && body.startsWith("Camel"));
                                }})
                                    .to("direct:siFilter").end()
                            .choice().when(exchangeProperty(Exchange.FILTER_MATCHED).isEqualTo(false))
                            .to("direct:notFilter").end();

                    from("direct:notFilter").log(":::: NOT MATCHED: ${body}").end();

                    from("direct:siFilter").log("::::: MATCHED ${body}").end();
                }});
            camelContext.start();
            //choice().when(simple("${property.CamelFilterMatched == false}")).to("direct:notFilter").endChoice()
            ProducerTemplate template = new DefaultProducerTemplate(camelContext);
            template.start();
            template.sendBody("direct:start", "Camel Multicast");
            template.sendBody("direct:start", "Camel Components");
            template.sendBody("direct:start", "Spring Integration");
            Thread.sleep(5000);
        } finally {
            camelContext.stop();
        }
    }

}