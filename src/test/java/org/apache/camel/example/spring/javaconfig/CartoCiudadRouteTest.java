package org.apache.camel.example.spring.javaconfig;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.model.RouteDefinition;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.BeforeClass;
import org.junit.Test;


public class CartoCiudadRouteTest extends CamelTestSupport {
	
	private static final String fileColonSlashSlash = "file://";
	
	@EndpointInject(uri = "mock://stream:out")
	protected MockEndpoint streamRoute;
	
	private String contentFile;
	
	private String directory;
	
	@BeforeClass
	public static void setUpClass() throws Exception {

	}
	
	@Override
	public void setUp() throws Exception {
		directory = "files_to_publish";
		contentFile = new String( Files.readAllBytes( Paths.get( CartoCiudadRouteTest.class.getClassLoader()
				.getResource( "open-street-data.csv" ).toURI() ) ) );
		super.setUp();
		
	}
	
	@Override
	protected RouteBuilder createRouteBuilder() throws Exception {
		return new CartoCiudadRoute();
	}
	
	@Test
	public void shouldProcessorAllLines() throws Exception {
		RouteDefinition route = context.getRouteDefinition( CartoCiudadRoute.SETTINGS_ROUTE_ID );
		route.adviceWith( context, new AdviceWithRouteBuilder() {
			
			@Override
			public void configure() throws Exception {
				mockEndpointsAndSkip( "stream:out" );
			}
		} );
		
		streamRoute.setAssertPeriod( 3000 );
		streamRoute.expectedMessageCount( 4 );
		
		template.sendBodyAndHeader( fileColonSlashSlash + directory, contentFile,
				Exchange.FILE_NAME, "open-street-data.csv" );
		
		streamRoute.assertIsSatisfied();
	}
	

}
