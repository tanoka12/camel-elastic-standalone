package org.apache.camel.example.spring.javaconfig;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.main.Main;
import org.apache.camel.spi.DataFormat;

import java.util.Date;

public class MainExample {

    private Main main;

    public static void main(String[] args) throws Exception {
        MainExample example = new MainExample();
        example.boot();
    }

    public void boot() throws Exception {
        // create a Main instance
        main = new Main();
        // bind MyBean into the registry
        main.bind("foo", new MyBean());
        // add routes
        main.addRouteBuilder(new MyRouteBuilder());
         // run until you terminate the JVM
        System.out.println("Starting Camel. Use ctrl + c to terminate the JVM.\n");
        main.run();
    }

    private static class MyRouteBuilder extends RouteBuilder {
        DataFormat bindy = new BindyCsvDataFormat( GeoCsv.class );
        @Override
        public void configure() throws Exception {
            from("file:MyFolder?move=Procesed").onCompletion()
                    // this route is only invoked when the original route is complete as a kind
                    // of completion callback
                    .log(":::: COMPLETE ::::").end()
                    .process(new Processor() {
                        public void process(Exchange exchange) throws Exception {
                            System.out.println("Invoked timer at " + new Date());
                        }
                    }).split( body().tokenize( "\n", 1, false ) )
                    .streaming().unmarshal( bindy )
                        .to("direct:step1");

            from("direct:step1").log(" ::: Busqueda 1 ${property.CamelSplitIndex}").process(new Processor() {
                @Override
                public void process(Exchange exchange) throws Exception {
                    //System.out.printf("Exchane: " + exchange);
                }
            }).to("direct:step2");

            from("direct:step2").to("direct:elastic");

            from("direct:elastic").choice().when(simple("${body.number} == 33"))
                    .log(":::: EL ����:::").process(new Processor() {
                @Override
                public void process(Exchange exchange) throws Exception {
                    GeoCsv geo = exchange.getIn().getBody(GeoCsv.class);
                    geo.setNumber("44");
                }
            }).to("direct:step1").endChoice();
        }
    }

    public static class MyBean {
        public void callMe() {
            System.out.println("MyBean.callMe method has been called");
        }
    }


}